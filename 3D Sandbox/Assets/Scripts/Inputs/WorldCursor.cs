﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WorldCursor : MonoBehaviour {

    // This script is used to select objects through raycasting.
    [Header("Raycast 3D")]
    public static WorldCursor worldCursor;
    private Ray _ray;
    private RaycastHit _rayHit;

    [Header("Selection")]
    public GameObject selectedObject;
    public GameObject constructionObject;
    public Button buildButton;
    public bool allowButton;

	// Use this for initialization
	void Start () {
        worldCursor = this;
    }
	
	// Update is called once per frame
	void Update () {
        // Raycast from the camera.
        _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // Checks if the raycast has hit anything.
        if(Physics.Raycast(_ray, out _rayHit, Camera.main.farClipPlane))
        {
            if (selectedObject == null)
            {
                UnSelected();
            }
            else if(selectedObject != null)
            {
                Selected();
            }

            // TEMP: Checks if the player is allowed to build. --------------------
            if (allowButton)
            {
                if(Input.GetButtonUp("Fire2"))
                {
                    Instantiate(constructionObject, _rayHit.point, Quaternion.identity);
                    allowButton = false;
                }
            }
            // Checks the wood in the resource manager.
            if(ResourceManager.resourceManager.woodQuantity > 20)
            {
                buildButton.interactable = true;
            }
            else
            {
                buildButton.interactable = false;
            }
        }
    }

    // This function is used to handle events when no object is selected.
    void UnSelected()
    {
        // Checks if the player requested a click.
        if (Input.GetButtonUp("Fire1"))
        {
            // Checks if the selected object is a human.
            if (_rayHit.collider.gameObject.CompareTag("Human"))
            {
                selectedObject = _rayHit.collider.gameObject;
                selectedObject.GetComponent<NavMeshAgent>().Stop();
                selectedObject.GetComponent<HumanBehaviour>().isSelected = true;
            }
        }
    }

    // This function is used to handle events when a object is selected.
    void Selected()
    {
        // Does a different tree of action depending on the object.
        if(selectedObject != null)
        {
            // Checks if the player has hit the click button
            if(Input.GetButton("Fire1"))
            {
                // Checks if the selected object is a resource.
                if(_rayHit.collider.gameObject.CompareTag("Resource"))
                {
                    // Sets the action object as the resource to mine, in the Human Behaviour object.
                    HumanBehaviour humanBehaviour = selectedObject.GetComponent<HumanBehaviour>();
                    // Creates a new action to give to the human characther.
                    Action newAction = new Action(_rayHit.collider.gameObject, "MineResource");
                    humanBehaviour.actionList.Add(newAction);
                    // Unselects the especific human.
                    humanBehaviour.isSelected = false;
                    Unselect();
                }
            }
        }
    }

    // This function is used to clear selection and similiar. 
    void Unselect()
    {
        // Resets the selection.
        selectedObject = null;
    }

    // TEMP: Anouces the player is allowed to build. --------------------
    public void AllowBuild ()
    {
        allowButton = true;
        ResourceManager.resourceManager.woodQuantity -= 20;
    }
}
