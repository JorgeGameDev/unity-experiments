﻿using UnityEngine;
using System.Collections;

public class CameraControls : MonoBehaviour {

    // This class is used to control the camera, so there's a better prespective of the plane.
    [Header("Camera Controls")]
    public float cameraSpeed;
	
	// Update is called once per frame
	void Update () {

        // Gets values from axis inputed by the player.
        float horAxis = Input.GetAxis("Horizontal");
        float verAxis = Input.GetAxis("Vertical");
        Vector3 cameraPos = transform.position;

        // Checks if any of them are diferent than zero.
        if(horAxis != 0)
        {
             cameraPos.x += horAxis * cameraSpeed;
        }

        if (verAxis != 0)
        {
            cameraPos.z += verAxis * cameraSpeed;
        }

        // Sets the camera.
        transform.position = cameraPos;
    }
}
