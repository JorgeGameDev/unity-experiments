﻿using UnityEngine;
using System.Collections;

public class Resource : MonoBehaviour {

    // This script is used to indicate a resource that can be mined by humans.
    [Header("Resource Mining")]
    public ResourceTypes resourceType;
    public byte resourceQuantity;
    public byte maxResource;
    public float resourceTimer;
    private bool _isRestocking;
	
	// Update is called once per frame
	void Update () {
	    // Changes the sprite depending on the resource quantity available.
            if(resourceQuantity > 0)
            {
                transform.GetChild(0).gameObject.SetActive(true);
                transform.GetChild(1).gameObject.SetActive(false);
                GetComponent<BoxCollider>().enabled = true;
                GetComponent<NavMeshObstacle>().enabled = true;
            }
            else
            {
                transform.GetChild(0).gameObject.SetActive(false);
                transform.GetChild(1).gameObject.SetActive(true);
                GetComponent<BoxCollider>().enabled = false;
                GetComponent<NavMeshObstacle>().enabled = false;
            }
        // Restocks the resources.
        if(resourceQuantity < maxResource && !_isRestocking)
        {
           StartCoroutine(RegenerateResource());
        }
    }

    // Public function that can be called for this resource to be mined.
    public void MineResource ()
    {
        // Checks if the quantity of the resource is 0 as a fail safe.
        if(resourceQuantity == 0)
        {
            return;
        }

        // If not, continues mining the resource.
        resourceQuantity--;

        // Checks the resource type and adds to the resource manager based on the type.
        switch(resourceType)
        {
            case ResourceTypes.Wood:
                ResourceManager.resourceManager.woodQuantity++;
                break;
            case ResourceTypes.Food:
                ResourceManager.resourceManager.foodQuantity++;
                break;
        }
    }

    // This function is used to regenerate resources as time goes on.
    IEnumerator RegenerateResource ()
    {
        // Annouces the object is restocking.
        _isRestocking = true;
        // Every defined timer, adds a new resources.
        yield return new WaitForSeconds(resourceTimer);
        // Returns the resources.
        if(resourceQuantity < maxResource)
        {
            resourceQuantity++;
        }
        _isRestocking = false;
    }
}
