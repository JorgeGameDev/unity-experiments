﻿using UnityEngine;
using System.Collections;

public class BuildingTimer : MonoBehaviour {

    // This scripts make cause a specific building to be destroyed after a specific time.
    [Header("Timer")]
    public float destroyTime;

	// Use this for initialization
	void Start () {
        // Starts the destroy timer.
        Destroy(gameObject, destroyTime);
	}
}
