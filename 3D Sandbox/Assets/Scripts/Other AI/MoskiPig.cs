﻿using UnityEngine;
using System.Collections;

public class MoskiPig : MonoBehaviour {

    [Header("Moski's Pig of Awesome Artificial Inteligence!")]
    public GameObject stalkHuman;

	// Use this for stalking!
	void Update () {
        GetComponent<NavMeshAgent>().SetDestination(stalkHuman.transform.position);
	}
}
