﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    // The Game Manager manages all the other managers and controls various variables that
    // do not fit or don't require a bigger manager to hold.
    [Header("Other Managers")]
    public static GameManager gameManager;
    public static ResourceManager resourceManager;

    [Header("Time Managament")]
    public GameObject sunLight;
    public float timeHour;
    private byte timeScale = 6;
    private float _xRotation;

    // Use this for initialization
    void Start () {
        // Gets the other managers.
        gameManager = this;
        resourceManager = ResourceManager.resourceManager;
        // Defines a starting time.
        _xRotation = 50;
    }
	
	// Update is called once per frame
	void Update () {
        ManageTime();
	}

    // This function is used to manage time and calculate everything related to it.
    void ManageTime()
    {
        // Adds to z based on time.
        _xRotation += timeScale * Time.deltaTime;
        // Clamps rotation to 0 to 360.
        if(_xRotation > 360)
        {
            _xRotation = 0;
        }
        // Calculates the time of the day. TODO: Get a better way to calculate time.
        if (_xRotation < 270)
            timeHour = Mathf.Round(((_xRotation + 90) / 15));
        else
            timeHour = Mathf.Round((_xRotation / 15) - 18);
        sunLight.transform.rotation = Quaternion.Euler(_xRotation, 230, 180);
    }
}
