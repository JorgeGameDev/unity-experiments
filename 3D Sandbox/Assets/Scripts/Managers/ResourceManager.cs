﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResourceManager : MonoBehaviour {

    // Holds all the info regarding resources and it's amount.
    [Header("Resource Manager")]
    public static ResourceManager resourceManager;

    [Header("Resource Quantity")]
    public int woodQuantity;
    public int foodQuantity;

    [Header("Resource UI")]
    public Text woodDisplay;
    public Text foodDisplay;

    // Use this for initialization
    void Start () {
        resourceManager = this;
	}
	
	// Update is called once per frame
	void Update () {
        // Updates the Wood Quantity.
        woodDisplay.text = "Wood: " + woodQuantity;
        foodDisplay.text = "Food: " + foodQuantity;
    }
}
