﻿using UnityEngine;
using System.Collections;

public class HumanNeeds : MonoBehaviour {

    // Human Needs define the necessity of each individual human. 
    // You know how that goes. (0 to 100 where 0 is necesity).
    [Header("General Resources")]
    private HumanBehaviour _humanBehaviour;

    [Header("Food Needs")]
    private bool _isHungerWait;
    public float hungerTimer;
    public byte hungerCurrent;
    public byte hungerBar;

	// Use this for initialization
	void Start () {
        _humanBehaviour = GetComponent<HumanBehaviour>();
	}
	
	// Update is called once per frame
	void Update () {
	    // Reduces hunger every specific set of time.
        if(!_isHungerWait)
        {
            StartCoroutine(NeedTimer());
        }
        // Calls functions.
        RecoverNeeds();
        CauseAcidents();
    }

    // This function is used to recover needs.
    void RecoverNeeds()
    {
        if(hungerCurrent < hungerBar)
        {
            // Recovers food based on the Resource Manager food quantity.
            if(ResourceManager.resourceManager.foodQuantity != 0)
            {
                // Causes hunger.
                ResourceManager.resourceManager.foodQuantity--;
                // Recovers hunger.
                hungerCurrent += 20;
            }
        }
    }

    // This function... is used to kill. Poor thing.
    void CauseAcidents()
    {
        // If hunger is max (0), kills the human.
        if(hungerCurrent == 0)
        { 
           Destroy(gameObject, 1);
        }
    }

    // This function is used as a timer to cause hunger.
    IEnumerator NeedTimer ()
    {
        // Announces that the characther is waiting for the next hunger loss.
        _isHungerWait = true;
        // Waits for seconds.
        yield return new WaitForSeconds(hungerTimer);
        // Causes hunger.
        hungerCurrent--;
        _isHungerWait = false;
    }
}
