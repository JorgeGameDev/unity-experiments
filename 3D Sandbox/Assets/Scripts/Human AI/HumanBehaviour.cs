﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HumanBehaviour : MonoBehaviour {

    // The Human Behaviour allows the human to have a simple thinking AI.
    // As well as to listen to the user requests.
    [Header("Selection")]
    public bool isSelected;
    public bool isAction;
    public bool isWaiting;

    [Header("Random AI")]
    private NavMeshAgent _navMeshAgent;

    [Header("Actions")]
    public List<Action> actionList;

    // Use this for initialization
    void Start()
    {
        // Gets the Navmesh Agent component.
        _navMeshAgent = GetComponent<NavMeshAgent>();
        // Gives the first random position to the player.
        StartCoroutine(NewPosition(0));
    }

    // Update is called once per frame
    void Update () {
	    // If it's not selected, the AI does what's it's been assigned.
        if(!isSelected)
        {
            // Checks if there are any tasks given to the selected NPC.
            if(actionList.Count > 0)
            {
                isAction = true;
                ExecuteTasks();
            }
            else
            {
                FinishedMoving();
            }
        }
        // Stops for a while so it can be given orders.
        else
        {
            _navMeshAgent.Stop();
        }
	}

    // This function is used to manage tasks given to humans.
    void ExecuteTasks ()
    {
        // Gets each of the individual actions and perfoms it, in another function.
        if (actionList.Count > 0)
        {
            // Gets the task globaly.
            Action executeAction = actionList[0];

            // Depending on the task message, does a diferent thing.
            if (executeAction.actionMessage == "MineResource")
            {
                if (Vector3.Distance(gameObject.transform.position, executeAction.actionObject.transform.position) > 1f)
                {
                    // Sets the navmesh decision.
                    _navMeshAgent.SetDestination(executeAction.actionObject.transform.position);
                    _navMeshAgent.Resume();
                }
                else
                {
                    // Checks if the resource is empty.
                    if (executeAction.actionObject.GetComponent<Resource>().resourceQuantity == 0)
                    {
                        // Removes the task.
                        actionList.Remove(executeAction);
                    }
                    else
                    {
                        // Sends the message to the action Object.
                        executeAction.actionObject.SendMessage(executeAction.actionMessage);
                    }
                }
            }
            else if (executeAction.actionMessage == "MoveHere")
            {
                // Checks if there are more tasks, in which case the action is destroyed.
                // Also checks if the object has reached its destination.
                if (actionList.Count > 1 || Vector3.Distance(gameObject.transform.position, executeAction.actionObject.transform.position) < 0.5f)
                {
                    // Destorys the used waypoint.
                    Destroy(executeAction.actionObject);
                    // Finishes the movement.
                    actionList.Remove(executeAction);
                }
                else
                {
                    // Sets the navmesh decision.
                    _navMeshAgent.SetDestination(executeAction.actionObject.transform.position);
                    _navMeshAgent.Resume();
                }

            }
        }
    }

    // This function is used as a timer to select a new position.
    IEnumerator NewPosition (float timeWait)
    {
        // Waits the time that has been requested.
        yield return new WaitForSeconds(timeWait);
        // Gets a random position based on a detection sphere.
        Vector3 randomSphere = Random.insideUnitSphere * 16;
        // Gets a Hit point in the navmesh.
        NavMeshHit navHit;
        NavMesh.SamplePosition(randomSphere, out navHit, 16, 1);
        // Defines the final position.
        Vector3 finalPosition = navHit.position;
        // Creates a new gameobject based on the navmesh position that was picked.
        GameObject wayPointGO = new GameObject("wayPoint");
        wayPointGO.transform.position = finalPosition;
        // Gives that object as an action.
        Action newAction = new Action(wayPointGO, "MoveHere");
        actionList.Add(newAction);
        // Confirms the waiting is over.
        isWaiting = false;
    }

    // This function is used to check if the human has finished moving.
    void FinishedMoving ()
    {
        // Checks if the human is currently waiting for the position.
        if (!isWaiting)
        {
            // Obtains a random value that determinates the time to wait between this and the next action.
            float randomTime = Random.Range(6, 12);
            // Stars courotine with the time.
            StartCoroutine(NewPosition(randomTime));
            // Makes the script know a new position is being rolled.
            isWaiting = true;
        }
    }
}
