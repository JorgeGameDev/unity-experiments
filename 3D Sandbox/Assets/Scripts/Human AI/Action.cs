﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Action {

    // This class defines an Action, which is a task that was given to an AI.
    // They can be multiple staked.

    [Header("Task Info")]
    public GameObject actionObject;
    public string actionMessage;

    public Action(GameObject newObject , string newMessage)
    {
        // Gives the information to the task.
        actionObject = newObject;
        actionMessage = newMessage;
    }
}
