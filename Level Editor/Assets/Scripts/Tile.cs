﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class Tile : ScriptableObject {

    // This scriptable object defines a tile. A tile is based on a prefab, and has a name and other constraints.
    [Header("Tile Information")]
    public string tileName;
    public GameObject tilePrefab;
}
