﻿using UnityEngine;
using System.Collections.Generic;

public class TileBrush : MonoBehaviour {

    /* This script managed the tileset brush that is used to place the tiles. 
    It's a global script that can always be acessed */

    [HideInInspector] public static TileBrush tileBrush;

    [Header("Brush Cursor")]
    public Vector3 currentMousePos; // NOT MOUSE POSITION. Only X and Z are taken from the mouse.
    public Vector3 currentMouseAngle;
    private Ray _ray;
    private RaycastHit _rayHit;

    [Header("Visible Cursor Preferences")]
    public GameObject triggerPlane;
    public GameObject cursorObject;
    public Material cursorMaterial;
    private GameObject _cursorPlane;
    private GameObject _hoveredObject;
    private Material _hoveredMaterial;

    [Header("Tiles")]
    public Tile tileSelected;
    private int _tileIndex = 0;
    public List<Tile> tileList;

    // Used for early initialization.
    void Awake ()
    {
        // Creates a self reference.
        tileBrush = this;
    }

    // Use this for initialization.
    void Start()
    {
        // Automatically selects the current tile.
        tileSelected = tileList[_tileIndex];
        cursorObject.GetComponent<MeshFilter>().mesh = tileList[_tileIndex].tilePrefab.GetComponent<MeshFilter>().sharedMesh;
        TileUI.tileUI.tileText.text = tileList[_tileIndex].tileName;
    }

    // Update is called once per frame.
    void Update()
    {
        // Calls the raycast function that will check for raycasts.
        DoRaycast();
        CheckInput();
    }

    // Checks if the player has requested any of the editor input.
    void CheckInput()
    {
        // Changes the Trigger Y if the user requests it.
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            currentMousePos.y += 1;
            triggerPlane.transform.position = new Vector3(0, currentMousePos.y + 0.01f, 0);
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            currentMousePos.y -= 1;
            triggerPlane.transform.position = new Vector3(0, currentMousePos.y + 0.01f, 0);
        }

        // Changes the item rotation if the user requests it.
        if (Input.GetKeyUp(KeyCode.R))
        {
            currentMouseAngle.y += 90;
            if (currentMouseAngle.y >= 360)
            {
                currentMouseAngle.y = 0;
            }
        }

        if (Input.GetKeyUp(KeyCode.T))
        {
            currentMouseAngle.y -= 90;
            if (currentMouseAngle.y <= 0)
            {
                currentMouseAngle.y = 360;
            }
        }

        // Changes the Tile if the users requests it.
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            // Updates the tile index.
            _tileIndex--;
            // Avoids exceptions.
            if (_tileIndex < 0)
            {
                _tileIndex = tileList.Count - 1;
            }
            // Updates the selected tile.
            tileSelected = tileList[_tileIndex];
            TileUI.tileUI.tileText.text = tileList[_tileIndex].tileName;
            cursorObject.GetComponent<MeshFilter>().mesh = tileList[_tileIndex].tilePrefab.GetComponent<MeshFilter>().sharedMesh;
        }

        else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            // Updates the tile index.
            _tileIndex++;
            // Avoids exceptions.
            if (_tileIndex > tileList.Count - 1)
            {
                _tileIndex = 0;
            }
            // Updates the selected tile.
            tileSelected = tileList[_tileIndex];
            TileUI.tileUI.tileText.text = tileList[_tileIndex].tileName;
            cursorObject.GetComponent<MeshFilter>().mesh = tileList[_tileIndex].tilePrefab.GetComponent<MeshFilter>().sharedMesh;
        }
    }

    // Does the starting raycast which checks for movement.
    void DoRaycast()
    {
        // Creates a ray from the camera to the mouse position.
        _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // Calculates if the raycast has hit anything.
        if (Physics.Raycast(_ray, out _rayHit))
        {
            // Destroys previous cursor.
            if (_cursorPlane != null)
                Destroy(_cursorPlane);

            // If something was raycast, ask for the hit position.
            if (_rayHit.collider.name == "TriggerPlane")
            {
                // Removes the hover material from the old hovered object.
                if (_hoveredObject != null)
                {
                    _hoveredObject.GetComponent<Renderer>().material = _hoveredMaterial;
                }

                // Updates the mouse position and the cursor plane.
                currentMousePos = new Vector3(Mathf.Round(_rayHit.point.x), currentMousePos.y, Mathf.Round(_rayHit.point.z));
                _cursorPlane = (GameObject)Instantiate(cursorObject, currentMousePos, Quaternion.Euler(currentMouseAngle));

                // Checks for player input.
                if (Input.GetButtonDown("Fire1"))
                {
                    SpawnObject(tileSelected.tilePrefab);
                }
            }
            else
            {
                // Checks if the old object exists.
                if (_hoveredObject != null)
                {
                    _hoveredObject.GetComponent<Renderer>().material = _hoveredMaterial;
                }
                // Updates the new hovered object and stores it's material.
                _hoveredObject = _rayHit.collider.gameObject;
                _hoveredMaterial = _hoveredObject.GetComponent<Renderer>().material;
                _hoveredObject.GetComponent<Renderer>().material = cursorMaterial;

                // Checks for player input.
                if (Input.GetButtonUp("Fire2"))
                {
                    TileSave.tileSave.RemoveTile(_hoveredObject);
                    Destroy(_hoveredObject);
                }
            }
        }
    }

    // This function is used to spawn objects.
    void SpawnObject(GameObject tileObject)
    {
        // Instanciates the object.
        GameObject newTile = (GameObject)Instantiate(tileObject, currentMousePos, Quaternion.Euler(currentMouseAngle));
        TileSave.tileSave.AddTile(newTile);
        newTile.transform.SetParent(TileSave.tileSave.parentPlaced);
    }

    // This function is called from the outside to desselect objects.
    public void DeSelectObjects()
    {
        // Destroys the cursor plane if it exists.  
        if(_cursorPlane != null)
        {
            Destroy(_cursorPlane);
        }

        if(_hoveredObject != null)
        {
            _hoveredObject.GetComponent<Renderer>().material = _hoveredMaterial;
        }
    }
}
