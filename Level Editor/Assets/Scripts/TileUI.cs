﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TileUI : MonoBehaviour {

    /* This script is used to manage UI on both the global UI and the editor UI */

    [HideInInspector]
    public static TileUI tileUI;

    [Header("Text Elements")]
    public Text tileText;

    // Used for early initialization.
    void Awake()
    {
        // Creates a self reference.
        tileUI = this;
    }
}
