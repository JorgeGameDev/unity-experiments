﻿using UnityEngine;
using System.Collections;

public class EditorCamera : MonoBehaviour {

    /* This script is used to control the camera in the editor. */
    [HideInInspector]
    public static EditorCamera editorCamera;

    [Header("Editor Camera")]
    public Camera editCamera;
    public float cameraSpeed;
    public float rotationSpeed;

    // Used for early initialization.
    void Awake()
    {
        // Creates a self reference.
        editorCamera = this;
    }

    // Use this for initialization
    void Start()
    {
        editCamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update () {
        ControlCamera();
    }

    // This function is used to control the camera.
    void ControlCamera ()
    {
        // Moves the main editor camera depending on horizontal or vertical input.
        float verMovement = Input.GetAxis("Vertical");
        float horMovement = Input.GetAxis("Horizontal");
        float rotMovement = Input.GetAxis("Rotation");

        // Changes the camera position.
        Vector3 cameraPosition = editCamera.transform.position;
        cameraPosition += transform.up * (verMovement * Time.deltaTime * cameraSpeed);
        cameraPosition += transform.right * (horMovement * Time.deltaTime * cameraSpeed);
        editCamera.transform.position = cameraPosition;

        // Chanegs the camera rotation.
        Vector3 cameraRotation = editCamera.transform.rotation.eulerAngles;
        cameraRotation += Vector3.up * (rotMovement * Time.deltaTime * rotationSpeed);
        editCamera.transform.eulerAngles = cameraRotation;
    }
}
