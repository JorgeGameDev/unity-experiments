﻿using UnityEngine;
using System.Collections.Generic;

public class TileSave : MonoBehaviour {

    /* This script is used to serialize stuff to XML */

    [HideInInspector]
    public static TileSave tileSave;

    [Header("Placed Tiles List")]
    public Transform parentPlaced;
    public List<GameObject> placedTiles;

    // Used for early initialization.
    void Awake()
    {
        // Creates a self reference.
        tileSave = this;
    }

    // Use this to add a tile to the placed tiles.
    public void AddTile (GameObject addedTile) {
        placedTiles.Add(addedTile);
	}
	
	// Use this to remove a tile to the removed tiles.
	public void RemoveTile (GameObject removedTile) {
        placedTiles.Remove(removedTile);
	}
}
