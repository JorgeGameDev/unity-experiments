﻿using UnityEngine;
using System.Collections;

public class GlobalEditor : MonoBehaviour {

    /* This script is used for global editing and changing from Editor Mode
    to Play Mode. */

    [HideInInspector]
    public static GlobalEditor globalEditor;

    [Header("Edit/Play Settings")]
    public bool isEditing = true;
    public GameObject editorManager;
    public GameObject gameManager;
    public GameObject playerObject;

    // Used for early initialization.
    void Awake()
    {
        // Creates a self reference.
        globalEditor = this;
    }

    // Use this for initialization
    void Start () {
        // Forces editing mode on start.
        isEditing = true;
    }
	
	// Update is called once per frame
	void Update () {
        ChangeMode();
	}

    // This function is used to switch between play mode and editor mode.
    void ChangeMode ()
    {
        // Changes between modes at user request.
        if (Input.GetKeyUp(KeyCode.P))
        {
            if (isEditing)
            {
                GoToPlay();
            }
            else
            {
                GoToEdit();
            }
        }
    }

    // This function is used to start the play mode and exit edit mode.
    void GoToPlay ()
    {
        // Resets the player transform position.
        TileBrush.tileBrush.DeSelectObjects();

        // Switches Editors and changes cursor states.
        editorManager.SetActive(false);
        gameManager.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        // Finds the player object and starts the play session.
        playerObject = GameObject.FindGameObjectWithTag("Player");
        isEditing = false;
    }

    // This function is used to start the edit mode at end of play session.
    void GoToEdit ()
    {
        // Resets the player position.
        playerObject.transform.position = Vector3.zero;

        // Switches Editors and changes cursor states.
        editorManager.SetActive(true);
        gameManager.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        // Starts the editor again.
        isEditing = true;
    }
}
