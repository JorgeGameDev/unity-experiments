﻿using UnityEngine;
using System.Collections;

// Pushes bodies away from each other.
public class SteeringSeparation : SteeringGroupBehaviour {

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        // Well, there's no one to go away from might as well give it a rest.
        if (neighbours.Count == 0)
        {
            return new Vector3();
        }

        // Gets the distace of each body
        Vector3 force = new Vector3();
        foreach (SteeringVehicle neighbour in neighbours)
        {
            Vector3 awayVector = vehicle.transform.position - neighbour.transform.position;
            float distance = awayVector.magnitude;
            awayVector /= distance;
            force += awayVector / distance;
        }

        return force;
    }

}
