﻿using UnityEngine;
using System.Collections;

// Creates and Manages Boids, You know the drill.
public class BoidManager : MonoBehaviour
{

    [Header("Boid Limits")]
    public Bounds bounds;

    [Header("Boid Spawning")]
    public GameObject boid;
    public float ammountOfBoids;

    // Use this for initialization
    void Start()
    {
        SpawnBoids();
    }

    // Spawns the ammount of boids wanted.
    void SpawnBoids()
    {
        for (int i = 0; i < ammountOfBoids; i++)
        {
            // Generates a new random position.
            Vector3 minBound = bounds.min;
            Vector3 maxBound = bounds.max;
            Vector3 position = Vector3.zero;
            position.x = Random.Range(minBound.x, maxBound.x);
            position.y = Random.Range(minBound.y, maxBound.y);
            position.z = Random.Range(minBound.z, maxBound.z);

            // Spawns the boid.
            GameObject newBoid = (GameObject)Instantiate(boid, position, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Draws the bounds on the scene.
    public void OnDrawGizmos()
    {
        // Draws the bounding box of the cube.
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }
  }
