﻿using UnityEngine;
using System.Collections;

// Aligns the behaviours together.
public class SteeringAlignment : SteeringGroupBehaviour {

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        // Well, there's nothing to align with so let's throw a party, I guess.
        if (neighbours.Count == 0)
        {
            return new Vector3();
        }

        // Gets the average velocity of every body.
        Vector3 averageVelocity = new Vector3 ();
        foreach (SteeringVehicle neighbour in neighbours)
        {
            averageVelocity += vehicle.velocity;
        }

        averageVelocity /= neighbours.Count;
        return averageVelocity - vehicle.velocity;
    }
}
