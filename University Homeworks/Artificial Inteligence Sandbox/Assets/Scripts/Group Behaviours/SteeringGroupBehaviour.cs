﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Used for Steering Behaviours that are applied in group.
[RequireComponent(typeof(SphereCollider))]
public abstract class SteeringGroupBehaviour : SteeringBehaviour
{

    // List of neighbours (Vehicles shared with this behaviours).
    protected List<SteeringVehicle> neighbours = new List<SteeringVehicle>();

    // Registers the neighbours when they get into the trigger.
    public void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.CompareTag(gameObject.tag))
        {
            SteeringVehicle vehicle = collider.GetComponent<SteeringVehicle>();
            neighbours.Add (vehicle);
        }
    }

    // Registers the neighbours when they exit into the trigger.
    public void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.CompareTag(gameObject.tag))
        {
            SteeringVehicle vehicle = collider.GetComponent<SteeringVehicle>();
            neighbours.Remove (vehicle);
        }
    }
}
