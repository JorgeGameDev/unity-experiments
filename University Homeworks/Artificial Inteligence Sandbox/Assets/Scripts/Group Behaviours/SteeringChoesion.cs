﻿using UnityEngine;
using System.Collections;

// Choeses the vehicles together. Yes. I don't know what that means.
public class SteeringChoesion : SteeringGroupBehaviour {

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        // Well, there's no one to follow might as well commit seppuku.
        if (neighbours.Count == 0)
        {
            return new Vector3();
        }

        // Gets the center of mass of all the neighbours and seeks there.
        Vector3 centerOfMass = new Vector3();
        foreach (SteeringVehicle neighbour in neighbours)
        {
            centerOfMass += neighbour.transform.position;
        }

        // Approximates the Center of Mass to the center of bodies and seeks there.
        centerOfMass /= neighbours.Count;
        return helper.Seek(vehicle, centerOfMass);
    }
}
