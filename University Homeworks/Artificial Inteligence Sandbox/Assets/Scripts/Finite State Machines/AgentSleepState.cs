﻿using UnityEngine;
using System.Collections;

// M-Makes the agent sleep?
public class AgentSleepState : State {

    private float currentHue;

    // What is this state going to do during the main loop?
    public override void UpdateState()
    {
        // Creates a color to print the text in.
        currentHue += 0.5f;
        if(currentHue > 360)
        {
            currentHue = 0;
        }
        Color newColor = Color.HSVToRGB(currentHue / 360, 1, 1);
        string finalColor = ColorToHex(newColor);

        // Prints ZzzzZzz.
        Debug.Log("<size=23><color=#" + finalColor + ">ZzzzZzzzZzzzZzzzZzz...</color></size>");
    }

    // Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
    string ColorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }
}
