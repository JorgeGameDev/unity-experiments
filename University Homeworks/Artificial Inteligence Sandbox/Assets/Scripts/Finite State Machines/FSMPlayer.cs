﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float speed;
	public float rotSpeed;

	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}

	void FixedUpdate(){
		float v = Input.GetAxis ("Vertical");

		Vector3 targetVel = transform.forward * v * speed;
		Vector3 currVel = rb.velocity;
		Vector3 force = (targetVel - currVel) * 0.3f;
		rb.AddForce (force, ForceMode.Impulse);
	}

	void Update(){
		float h = Input.GetAxis ("Horizontal");
		transform.Rotate (0, h * rotSpeed * Time.deltaTime, 0);
	}


}
