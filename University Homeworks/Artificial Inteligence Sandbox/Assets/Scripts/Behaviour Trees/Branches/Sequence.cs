﻿using UnityEngine;
using System.Collections;

public class Sequence : SingleRunningBranchTask {

	public override void TaskStart ()
	{
		base.TaskStart ();
		currTaskIndex = 0;
	}

	public override void ChildSucceeded (Task child)
	{
		if (currTaskIndex + 1 >= numChildren) {
			MarkSuccess ();
		} else {
			currTaskIndex++;
			currentTask.MarkRunning ();
			currentTask.TaskStart ();
			MarkRunning ();
		}
	}

	public override void ChildFailed (Task child)
	{
		MarkFailed ();
	}

	public override void ChildRunning (Task child)
	{
		MarkRunning ();
	}

	public override void ChildCancelled (Task child)
	{
		MarkCancelled ();
	}
}
