﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Used by the cube to change colors.
/// </summary>
public class CubeBehaviour : MonoBehaviour {

    [Header("Color")]
    public Color[] shiftColor;

    [Header("Timer")]
    public float time;

    // Internal Components
    private BehaviourTree behaviourTree;
    private Sequence sequence;

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Start()
    {
        // Creates a new behaviour tree.
        behaviourTree = new BehaviourTree(gameObject);
        sequence = new Sequence();
        
        // Adds the tasks to the sequence.
        foreach(Color color in shiftColor)
        {
            sequence.AddChild(new SetColor(color));
            sequence.AddChild(new WaitColor());
        }

        // Parses the sequence to the behaviour tree.
        behaviourTree.root = sequence;
    }

    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    void Update ()
    {
        // Updates the status of the behaviour tree.
        behaviourTree.UpdateStatus();
	}
}
