﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Task used for changing the colors of the cube.
/// </summary>
public class SetColor : LeafTask
{
    // Internal Properties of the Task.
    private Color _currentColor;

    /// <summary>
    /// Constructs the class, with a given color.
    /// </summary>
    public SetColor(Color color)
    {
        _currentColor = color;
    }

    /// <summary>
    /// Sets up the task when it starts.
    /// </summary>
    public override void OnTaskStart()
    {
        // Changes the color to the given color.
        agent.GetComponent<MeshRenderer>().material.color = _currentColor;
        agent.GetComponentInChildren<Light>().color = _currentColor;
    }

    /// <summary>
    /// Called when the task is executed.
    /// </summary>
    public override Status ExecuteState()
    {
        // Returns that the status has been sucessfull.
        return Status.Succeeded;
    }
}
