﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Used for waiting between tasks.
/// </summary>
public class WaitColor : LeafTask
{
    // Internal Properties of the Task. 
    private float _time;
    private bool _taskComplete;

    /// <summary>
    /// Sets up the task when it starts.
    /// </summary>
    public override void OnTaskStart()
    {
        // Gets the time and behaviour from the agent.
        CubeBehaviour cubeBehaviour = agent.GetComponent<CubeBehaviour>();
        _time = cubeBehaviour.time;
        _taskComplete = false;

        // Starts coroutine on the agent's monobehaviour.
        cubeBehaviour.StartCoroutine(WaitTimer());
    }

    /// <summary>
    /// Called when the task is executed.
    /// </summary>
    public override Status ExecuteState()
    {
        if(_taskComplete)
        {
            // Returns that the status has been sucessfull.
            return Status.Succeeded;
        }
        else
        {
            return Status.Running;
        }
    }

    /// <summary>
    /// Courtoine used for waiting! How new!
    /// </summary>
    IEnumerator WaitTimer()
    {
        yield return new WaitForSeconds(_time);
        _taskComplete = true;
    }
}
