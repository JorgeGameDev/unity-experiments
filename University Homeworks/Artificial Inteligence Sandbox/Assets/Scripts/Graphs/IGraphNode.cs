﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface used for Nodes.
/// </summary>
public interface IGraphNode {

	float totalNodeCost{
		get;
		set;
	}

    float realNodeCost
    {
        get;
        set;
    }
}
