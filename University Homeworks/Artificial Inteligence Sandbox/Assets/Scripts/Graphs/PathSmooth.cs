﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PathSmooth{

	public List<Transform> Smooth(List<Transform> path, LayerMask mask){
		List<Transform> result = new List<Transform> ();
		result.Add (path [0]);
		int pivot = 0;
		for (int i = 1; i < path.Count-1; i++) {
			Transform t1 = path [pivot];
			Transform t2 = path [i + 1];
			if (Physics.Linecast (t1.position, t2.position, mask)) {
				result.Add (path [i]);
				pivot = i;
			}
		}
		result.Add (path [path.Count - 1]);
		return result;
	}



}
















/*
public class PathSmooth{

	public List<Transform> Smooth(List<Transform> path, LayerMask mask){
		List<Transform> result = new List<Transform> ();


		result.Add (path [0]);
		int pivot = 0;
		for (int i = 1; i < path.Count - 1; i++) {
			Transform p = path [pivot];
			Transform next = path [i + 1];
			if (IsBlocked (p, next, mask)) {
				result.Add(path[i]);
					pivot = i;
			}
		}
		result.Add (path [path.Count - 1]);

		return result;
	}

	private bool IsBlocked(Transform p1, Transform p2, LayerMask mask){
		return Physics.Linecast (p1.position, p2.position, mask);
	}
}*/
