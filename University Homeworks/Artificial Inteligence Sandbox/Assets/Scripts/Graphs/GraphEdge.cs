﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Edge between Nodes. Keeps track of the cost of traveling between a node.
/// </summary>
public class GraphEdge {

    // Nodes which belong to this edge, and the cost of traveling from one to another.
	public IGraphNode from, to;
	public float cost;

    /// <summary>
    /// Sets the Edge between nodes up.
    /// </summary>
	public GraphEdge(IGraphNode from, IGraphNode to, float cost){
		this.from = from;
		this.to = to;
		this.cost = cost;
	}
		
}
