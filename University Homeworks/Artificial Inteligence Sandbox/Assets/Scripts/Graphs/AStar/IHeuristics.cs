﻿using UnityEngine;
using System.Collections;

public interface IHeuristics
{
    float Calculate(IGraphNode neighbour, IGraphNode target);
}
