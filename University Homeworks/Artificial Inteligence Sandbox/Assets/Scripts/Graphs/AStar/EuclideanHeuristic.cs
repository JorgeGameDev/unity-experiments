﻿using UnityEngine;
using System.Collections;
using System;

public class EuclideanHeuristic : IHeuristics {

    public float Calculate(IGraphNode neighbour, IGraphNode target)
    {
        PositionNode neighbourNode = (PositionNode)neighbour;
        PositionNode targetNode = (PositionNode)target;
        Vector3 difference = neighbourNode.transform.position - targetNode.transform.position;
        float result = Mathf.Abs(difference.x) + Mathf.Abs(difference.y) + Mathf.Abs(difference.z);
        return result;
    }
}
