﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Executes the Dijkstra algorithm in order to find the shortest path to node.
/// </summary>
public class GraphSearchDijkstra {

    // Keeps track of the shortest path so far.
	public Dictionary<IGraphNode, GraphEdge> shortestPathTree;

    // Keeps track of the frontier that's being searched at the moment.
	public Dictionary<IGraphNode, GraphEdge> searchFrontier;

    // Keeps a list of the resulting path.
    public List<IGraphNode> resultPath;

    // Priority Queue object which sorts a list and queues it up.
    PriorityQueue pq;

    /// <summary>
    /// Searches the graph with the algorithm. Functionality explained along.
    /// </summary>
	public bool Search(Graph graph, IGraphNode origin, IGraphNode target)
    {
        // Creates new lists that will be used.
		shortestPathTree = new Dictionary<IGraphNode, GraphEdge> ();
		searchFrontier = new Dictionary<IGraphNode, GraphEdge> ();
		pq = new PriorityQueue ();

        // Priority queues the origin node and goes from there.
		pq.Add (origin);

        // While the queue is still not empty, execute the code.
		while (pq.Count () > 0) {

            // Checks the closest node after sorting the node cost.
			IGraphNode closestNode = pq.PopFirst ();
			if (searchFrontier.ContainsKey (closestNode))
            {
                // Stores the closest node.
				GraphEdge cheapestEdge = searchFrontier [closestNode];
				if (shortestPathTree.ContainsKey (closestNode))
                {
                    // Since there's already a reference to this node. Updates with the closest edge.
					shortestPathTree [closestNode] = cheapestEdge;
				}
                else
                {
                    // This node is being added for the first time. Associates it with the closest node.
					shortestPathTree.Add (closestNode, cheapestEdge);
				}
			}

            // Checks if the currently searched node is the target node.
			if (closestNode == target) {

                // Construct the final path.
                ConstructPath(origin, target);
                return true;
			}

            // Gets the frontier from the closet node.
			List<GraphEdge> frontier = graph.GetFrontier (closestNode);
			for (int i = 0; i < frontier.Count; i++)
            {
                // Compares the edge cost for each node present in the fontier.
				GraphEdge edge = frontier [i];
				IGraphNode neighbour = edge.to;

                // Given the cost, checks the total cost of the nodes.
				float totalCost = closestNode.totalNodeCost + edge.cost;

                // Checks if the node has a neighbour.
				if (!searchFrontier.ContainsKey (neighbour))
                {
					neighbour.totalNodeCost = totalCost;
					searchFrontier.Add (neighbour, edge);
					pq.Add (neighbour);
				}
                // Checks if the Total Cost is smaller than the neighbour cost.
                else if(totalCost < neighbour.totalNodeCost)
                {
                    // Adds the new searched edge.
					searchFrontier [neighbour] = edge;
					neighbour.totalNodeCost = totalCost;
					pq.MarkToSort ();
				}
			}
		}

        return false;
	}

    /// <summary>
    /// Constructs the path that is used by the node system.
    /// </summary>
    private void ConstructPath(IGraphNode origin, IGraphNode target)
    {
        // Creates the new resulting grid.
        resultPath = new List<IGraphNode>();

        // Adds the new edges to the path.
        resultPath.Add(target);
        GraphEdge edge = shortestPathTree[target];
        resultPath.Add(edge.from);
        while (edge.from != origin)
        {
            edge = shortestPathTree[edge.from];
            resultPath.Add(edge.from);
        }

        // Reverses the path so it's on the order of start to finish.
        resultPath.Reverse();
    }
}
