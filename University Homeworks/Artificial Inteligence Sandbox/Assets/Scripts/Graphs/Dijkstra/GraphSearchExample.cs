﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphSearchExample : MonoBehaviour {

	Graph graph;
	GraphSearchDijkstra dijkstra;

	PositionNode[,] grid;

	[System.Serializable]
	public struct NodePosition{
		public int row, col;
	}

    [Header("Dijkstra Properties")]
    public SearchType searchType;
	public NodePosition origin;
	public NodePosition target;
	public bool performSearch;

	[Header("Graph Grid Properties")]
	public PositionNode nodePrefab;
	public Transform obstaclePrefab;
    public LayerMask obstaclesMask;
    public float obstacleProbability = 0.1f;
	public int numRows, numCols;
	public float nodeSpacing;
	public bool remakeGraph;

	[Header("Player Properties")]
	public GraphPlayer player;
	public SteeringVehicle vehicle;

    // Internal Instances.
    private List<Transform> obstacles;

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Awake () {
		ConstructGraphGrid();
	}

    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    void Update () {

        // Checks if a search is pending.
		if (performSearch)
        {
			SearchGraph ();
			performSearch = false;
		}

        // Checks if a remake of the graph is pending.
		if (remakeGraph)
        {
			ConstructGraphGrid ();
			remakeGraph = false;
		}
	
	}

    /// <summary>
    /// Searchs the graph depending on the different 
    /// </summary>
    void SearchGraph()
    {
        PositionNode originNode = grid[origin.row, origin.col];
        PositionNode targetNode = grid[target.row, target.col];

        dijkstra = new GraphSearchDijkstra();
        bool didFind = dijkstra.Search(graph, originNode, targetNode);

        if (didFind)
        {
            SteeringPathFollow fp = player.GetComponent<SteeringPathFollow>();
            fp.pathPoints.Clear();

            PathSmooth smooth = new PathSmooth();
            List<Transform> originalPath = new List<Transform>();
            foreach (PositionNode n in dijkstra.resultPath)
            {
                originalPath.Add(n.transform);
            }

            List<Transform> smoothedPath = smooth.Smooth(originalPath, obstaclesMask);
            fp.pathPoints = smoothedPath;
            player.transform.position = fp.pathPoints[0].position;
            fp.index = 0;
        }
    }

    void ConstructGraphGrid()
    {
        if (obstacles.Count > 0)
        {
            foreach (Transform obs in obstacles)
            {
                Destroy(obs.gameObject);
            }
            obstacles.Clear();
        }
        graph = new Graph(false);
        grid = new PositionNode[numRows, numCols];
        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numCols; j++)
            {
                Vector3 pos = new Vector3(i * nodeSpacing, 0, j * nodeSpacing);
                PositionNode n = (PositionNode)Instantiate(nodePrefab, pos, Quaternion.identity);
                if (Random.value <= obstacleProbability)
                {
                    Transform obs = (Transform)Instantiate(obstaclePrefab, n.transform.position, Quaternion.identity);
                    obstacles.Add(obs);
                    n.hasObstacle = true;
                }
                else
                {
                    graph.AddNode(n);
                }
                grid[i, j] = n;
            }
        }

        for (int i = 0; i < numRows - 1; i++)
        {
            for (int j = 0; j < numCols; j++)
            {
                PositionNode n1 = grid[i, j];
                PositionNode n2 = grid[i + 1, j];
                if (n1.hasObstacle == false && n2.hasObstacle == false)
                {
                    GraphEdge edge = new GraphEdge(n1, n2, 1);
                    graph.AddEdge(edge);
                }
            }
        }

        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numCols - 1; j++)
            {
                PositionNode n1 = grid[i, j];
                PositionNode n2 = grid[i, j + 1];
                if (n1.hasObstacle == false && n2.hasObstacle == false)
                {
                    GraphEdge edge = new GraphEdge(n1, n2, 1);
                    graph.AddEdge(edge);
                }
            }
        }
    }

    void OnDrawGizmos()
    {
        DrawGraphGizmos();
        DrawSearchGizmos();
    }

    private void DrawGraphGizmos()
    {
        if (graph == null)
        {
            return;
        }
        Gizmos.color = Color.red;
        foreach (PositionNode n in graph.nodes)
        {
            Vector3 pos = n.transform.position;
            Gizmos.DrawWireSphere(pos, 0.5f);
        }

        foreach (GraphEdge e in graph.edges)
        {
            Vector3 pos1 = ((PositionNode)e.from).transform.position;
            Vector3 pos2 = ((PositionNode)e.to).transform.position;
            Gizmos.DrawLine(pos1, pos2);
        }
    }

    private void DrawSearchGizmos()
    {
        if (grid == null)
        {
            return;
        }

        //draw origin, target
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(originNode.transform.position, 0.6f);
        Gizmos.DrawWireSphere(targetNode.transform.position, 0.6f);

        //draw searched nodes/edges
        if (dijkstra != null)
        {
            foreach (IGraphNode key in dijkstra.searchFrontier.Keys)
            {
                GraphEdge edge = dijkstra.searchFrontier[key];
                PositionNode from = (PositionNode)edge.from;
                PositionNode to = (PositionNode)edge.to;
                Gizmos.DrawLine(from.position, to.position);
            }
            if (dijkstra.resultPath != null)
            {
                Gizmos.color = Color.white;
                for (int i = 0; i < dijkstra.resultPath.Count - 1; i++)
                {
                    PositionNode n1 = (PositionNode)dijkstra.resultPath[i];
                    PositionNode n2 = (PositionNode)dijkstra.resultPath[i + 1];
                    Gizmos.DrawLine(n1.transform.position, n2.transform.position);
                }
            }
            SteeringPathFollow fp = player.GetComponent<SteeringPathFollow>();
            if (fp.pathPoints.Count > 0)
            {
                Gizmos.color = Color.cyan;
                for (int i = 0; i < fp.pathPoints.Count - 1; i++)
                {
                    Transform t1 = fp.pathPoints[i];
                    Transform t2 = fp.pathPoints[i + 1];
                    Gizmos.DrawLine(t1.position, t2.position);
                }
            }
        }
    }

    private PositionNode originNode
    {
        get
        {
            return grid[origin.row, origin.col];
        }
    }

    private PositionNode targetNode
    {
        get
        {
            return grid[target.row, target.col];
        }
    }
}

/// <summary>
/// Enumerator used for deciding the Search Type.
/// </summary>
public enum SearchType
{
    Dijkstra,
    AStar,
}
