﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Queues the nodes based on a priority and their edge value.
/// </summary>
public class PriorityQueue{

    // List of Nodes. 
	List<IGraphNode> nodes;

    // Keeps track if the node still needs sorting.
	bool needsSorting;

    // Constructor. Creates the new list of nodes that will be used.
	public PriorityQueue()
    {
		nodes = new List<IGraphNode> ();
	}

    /// <summary>
    /// Adds the node to be sorted.
    /// </summary>
	public void Add(IGraphNode node)
    {
		nodes.Add(node);
		needsSorting = true;
	}

    /// <summary>
    /// Returns the first node at the top of hte list.
    /// </summary>
	public IGraphNode PopFirst()
    {
        // Checks if the list still needs sorting.
		if (needsSorting)
        {
			Sort ();
		}

        // Removes the node from the list and returns it.
		IGraphNode first = nodes [0];
		nodes.RemoveAt (0);
		return first;
	}

    /// <summary>
    /// Checks how many nodes are still on the queue.
    /// </summary>
	public int Count()
    {
		return nodes.Count;
	}

    /// <summary>
    /// Marks the list as needing to be sorted. Again.
    /// </summary>
	public void MarkToSort()
    {
		needsSorting = true;
	}

    /// <summary>
    /// Sorts the list based on the node cost.
    /// </summary>
	void Sort()
    {
		nodes.Sort ( (a, b) => a.totalNodeCost.CompareTo(b.totalNodeCost) );
		needsSorting = false;
	}
}
