﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphPlayer : MonoBehaviour {

	public List<Transform> pathToFollow{
		get
        {
			return followPath.pathPoints;
		}
	}

	private SteeringVehicle vehicle;
	private SteeringPathFollow followPath;

	// Use this for initialization
	void Start ()
    {
		vehicle = GetComponent<SteeringVehicle> ();
		followPath = GetComponent<SteeringPathFollow> ();
	}

	void Update()
    {
		float rotSpeed = 10f * Time.deltaTime;
		Quaternion targetRot = Quaternion.LookRotation (vehicle.direction);
		transform.rotation = Quaternion.Slerp (transform.rotation, targetRot, rotSpeed);
	}

	public void FollowPath(List<Transform> path)
    {
		transform.position = path [0].position;

		if (vehicle.steeringBehaviours.Contains (followPath) == false)
        {
			vehicle.steeringBehaviours.Add (followPath);
		}

		followPath.pathPoints = path;
	}
		
}
