﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionNode : MonoBehaviour, IGraphNode {

	public float totalNodeCost {
		get;
		set;
	}

    public float realNodeCost
    {
        get;
        set;
    }

    public Vector3 position{
		get{
			return transform.position;
		}
	}

    public bool hasObstacle;
}
