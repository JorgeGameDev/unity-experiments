﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Graph composed of Nodes and the Edges that interconect each other. Used by the AI.
/// </summary>
public class Graph {

    // List of Nodes, Edges and Frontiers.
	public List<IGraphNode> nodes = new List<IGraphNode>();
	public List<GraphEdge> edges = new List<GraphEdge> ();
	Dictionary<IGraphNode, List<GraphEdge>> frontiers = new Dictionary<IGraphNode, List<GraphEdge>>();

	bool directed;

    /// <summary>
    /// Graph Constructor. Creates the graph.
    /// </summary>
	public Graph(bool directed){
		this.directed = directed;
	}

    /// <summary>
    /// Adds a Node to the Graph.
    /// </summary>
	public void AddNode(IGraphNode node){
		if (nodes.Contains (node)) {
			return;
		}
		nodes.Add (node);
		frontiers.Add (node, new List<GraphEdge> ());
	}

    /// <summary>
    /// Removes a Node from the Graph.
    /// </summary>
	public void RemoveNode(IGraphNode node){
		nodes.Remove (node);
		frontiers.Remove (node);
        
        // Removes each of the edges connected to this node.
		for (int i = 0; i < edges.Count; i++) {
			GraphEdge edge = edges [i];
			if (edge.from == node || edge.to == node) {
				RemoveEdge (edge);
			}
		}
	}

    /// <summary>
    /// Creates a new edge between nodes.
    /// </summary>
	public void AddEdge(GraphEdge edge){
		if (edges.Contains (edge)) {
			return;
		}
		edges.Add (edge);
		List<GraphEdge> frontier = frontiers [edge.from];
		frontier.Add (edge);
		if (directed == false) {
			GraphEdge inverted = new GraphEdge (edge.to, edge.from, edge.cost);
			edges.Add (inverted);
			frontier = frontiers [inverted.from];
			frontier.Add (inverted);
		}
	}

    /// <summary>
    /// Removes an edge between nodes.
    /// </summary>
	public void RemoveEdge(GraphEdge edge){
		if (edges.Remove (edge)) {
			List<GraphEdge> frontier = frontiers [edge.from];
			frontier.Remove (edge);
			if (directed == false) {
				frontier = frontiers [edge.to];
				for (int i = 0; i < frontier.Count; i++) {
					GraphEdge inverted = frontier [i];
					if (inverted.to == edge.from && inverted.from == edge.to) {
						edges.Remove (inverted);
						frontier.Remove (inverted);
						break;
					}
				}
			}
		}
	}

    /// <summary>
    /// Gets a frontier between nodes.
    /// </summary>
	public List<GraphEdge> GetFrontier(IGraphNode node){
		return frontiers [node];
	}
}
