﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Node used for the algorithm.
/// </summary>
public class GraphNode : IGraphNode
{
    public float totalNodeCost
    {
        get
        {
            return totalNodeCost;
        }

        set
        {
            totalNodeCost = value;
        }
    }

    public float realNodeCost
    {
        get
        {
            return realNodeCost;
        }

        set
        {
            realNodeCost = value;
        }
    }
}
