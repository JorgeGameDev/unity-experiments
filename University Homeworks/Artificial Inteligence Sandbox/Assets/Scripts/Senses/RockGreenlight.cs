﻿using UnityEngine;
using System.Collections;

// Plays a sound when the rock fells.
public class RockGreenlight : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // Plays a sound when the rock hits the floor.
    public void OnCollisionEnter(Collision collision)
    {
        float volume = 100;
        HearingManager.hearingManager.OnSound(gameObject, transform.position, volume);
    }
}
