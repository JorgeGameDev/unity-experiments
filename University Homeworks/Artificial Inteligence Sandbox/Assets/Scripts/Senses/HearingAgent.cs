﻿using UnityEngine;
using System.Collections;
using System;

public class HearingAgent : MonoBehaviour, IHearingListener
{
    [Header("Properties")]
    public float minimumVolume;

    // Internal
    private SteeringVehicle _vehicle;

    // Minimum Volume Necessary to consider this as hearing.
    public float minVolume
    {
        get
        {
            return minimumVolume;
        }
    }

    // The current position of the agent when the sound is heared.
    public Vector3 position
    {
        get
        {
            return transform.position;
        }
    }

    // Use this for initialization
    void Start()
    {
        HearingManager.hearingManager.Register(this);
        _vehicle = GetComponent<SteeringVehicle>();
    }

    // Update is called every frame.
    public void Update()
    {
        transform.rotation = Quaternion.LookRotation(_vehicle.direction);
    }

    // Called when a sound is heard.
    public void OnHearSound(GameObject sender, Vector3 position, float volume)
    {
        // Announces that a sound was heard.
        Debug.Log("PAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAW!");

        // Removes the previous seek state.
        SteeringSeek[] seekers = GetComponents<SteeringSeek>();
        foreach (SteeringSeek previousSeek in seekers)
        {
            _vehicle.steeringBehaviours.Remove(previousSeek);
            Destroy(previousSeek);
        }

        // Makes the agent seek for the sound source.
        SteeringSeek seek = gameObject.AddComponent<SteeringSeek>();
        seek.target = sender.transform;

        // Adds the behaviour to the list.
        _vehicle.steeringBehaviours.Insert(0, seek);
    }

    // Called when the object is destroyed
    public void OnDestroy()
    {
        HearingManager.hearingManager.UnRegister(this);
    }
}
