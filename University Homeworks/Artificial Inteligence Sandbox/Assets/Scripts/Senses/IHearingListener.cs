﻿using UnityEngine;
using System.Collections;

// Helps the agent detect if they are hearing sounds.
public interface IHearingListener {

    /// <summary>
    /// Minimum Volume Necessary to consider this as hearing.
    /// </summary>
	float minVolume
    {
        get;
    }

    /// <summary>
    /// The current position of the agent when the sound is heared.
    /// </summary>
    Vector3 position
    {
        get;
    }

    /// <summary>
    /// Called when the agent hears the sound.
    /// </summary>
    /// <param name="sender">Object that emited the sound.</param>
    /// <param name="position">Position of the Agent when the sound is heard.</param>
    /// <param name="volume">Volume of the sound when it's heard.</param>
    void OnHearSound(GameObject sender, Vector3 position, float volume);
}
