﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HearingManager : MonoBehaviour
{

    public static HearingManager hearingManager
    {
        get;
        private set;
    }

    [Header("Hearing Properties")]
    public float soundFallof;

    // Keeps track of all the listeners.
    List<IHearingListener> listeners = new List<IHearingListener>();

    // Use this for initialization
    public void Awake()
    {
        // Assigns this as a singleton, unless there's already one.
        if(hearingManager != null)
        {
            Destroy(gameObject);
        }

        hearingManager = this;
    }

    // Called when a sound is heard.
    public void OnSound(GameObject sender, Vector3 position, float volume)
    {
        foreach (IHearingListener listener in listeners)
        {
            Vector3 toListener = listener.position - position;
            float distance = toListener.magnitude;
            float heardVolume = volume - distance * soundFallof;

            // Checks if, given the distance, the listener heard the sound.
            if (heardVolume >= listener.minVolume)
            {
                listener.OnHearSound(sender, position, heardVolume);
            }
        }
    }

    // Adds a new listner to the list of listeners.
    public void Register(IHearingListener listener)
    {
        listeners.Add(listener);
    }

    // Does the complete opposite of Register, duh.
    public void UnRegister(IHearingListener listener)
    {
        listeners.Remove(listener);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
