﻿using UnityEngine;
using System.Collections;

public class VisionSensor : MonoBehaviour {

    [Header("Vision Properties")]
    public LayerMask obstacleLayer;
    public Transform visorOrigin;
    public float startFOV;
    public float endFOV;
    public float range;

    [Header("Sensor Target")]
    public Transform target;

    // Internal
    public bool targetInSight
    {
        get;
        private set;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        targetInSight = TargetInCone() && TargetInOpenSight();
	}

    // Checks if the Traget is in the Cone.
    private bool TargetInCone()
    {
        // Checks if the target is in range.
        Vector3 toTarget = transform.position - visorOrigin.position;
        if(toTarget.sqrMagnitude > range * range)
        {
            return false;
        }

        // Checks if the playe fits the angle.
        float angle = Vector3.Angle(visorOrigin.forward, toTarget);
        return angle < GetFOV(toTarget.magnitude) * 0.5f;
    }

    // Checks if the target is in the open sight.
    private bool TargetInOpenSight()
    {
        return Physics.Linecast (visorOrigin.position, target.position, obstacleLayer);
    }

    // Gets the FOV from the sight.
    private float GetFOV(float distance)
    {
        // Gets the normal and calculates the FOV based on a distance.
        float normal = distance / range;
        return Mathf.Lerp(startFOV, endFOV, normal);
    }
}
