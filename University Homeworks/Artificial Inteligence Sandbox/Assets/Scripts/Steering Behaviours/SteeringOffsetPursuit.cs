﻿using UnityEngine;
using System.Collections;

// Does a pursuit but with a position offset.
public class SteeringOffsetPursuit : SteeringBehaviour {
    
    [Header("Pursuit Properties")]
    public SteeringVehicle leaderVehicle;
    public Vector3 localOffset;

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        // Calculates the difference in offsets 
        Vector3 worldOffset = leaderVehicle.LocalToWorld(localOffset);
        Vector3 toOffset = worldOffset - vehicle.transform.position;
        float lookAhead = toOffset.magnitude / (leaderVehicle.velocity.magnitude + vehicle.maxSpeed);
        Vector3 target = worldOffset + leaderVehicle.direction * lookAhead;
        return helper.Seek(vehicle, target);
    }
}
