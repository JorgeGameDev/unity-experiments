﻿using UnityEngine;
using System.Collections;

// When the vehicle approaches the target, starts slowing down.
public class SteeringArrive : SteeringBehaviour
{
    [Header("Seeking Properties")]
    public Transform target;

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        // Calculates the distance to vector.
        Vector3 toTarget = target.position - vehicle.transform.position;
        float distance = toTarget.magnitude;

        // Avoids the distance becoming zero, in order to avoid NaN errors.
        if(distance < Mathf.Epsilon)
        {
            return Vector3.zero;
        }
        else
        {
            // Calculates the speed necessary.
            float speed = Mathf.Min(vehicle.maxSpeed, distance / vehicle.decelaration);

            // Calculates the needed force.
            Vector3 neededForce = (toTarget / distance) * speed;
            return neededForce - vehicle.velocity;
        }
    }
}
