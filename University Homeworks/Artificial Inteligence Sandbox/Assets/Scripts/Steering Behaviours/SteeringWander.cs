﻿using UnityEngine;
using System.Collections;

public class SteeringWander : SteeringBehaviour
{

    [Header("Wander Properties")]
    public float radius;
    public float jitter;
    public float distance;
    private SteeringVehicle debugVehicle;

    // Internal Variables
    private Vector3 _localTarget;

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        // Calculates the displacement where the circle will be placed.
        debugVehicle = vehicle;
        _localTarget.x += Random.Range(-jitter, jitter);
        _localTarget.y += Random.Range(-jitter, jitter);
        _localTarget.z += Random.Range(-jitter, jitter);

        // Generates the circle at the displacement position.
        _localTarget.Normalize();
        _localTarget *= radius;

        // Transforms the local circle into world coordinates.
        Vector3 worldTarget = vehicle.LocalToWorld(_localTarget);
        worldTarget += vehicle.direction * distance;
        return worldTarget - vehicle.transform.position;
    }

    // Draws debug gizmos for helping.
    public void OnDrawGizmosSelected()
    {
        // Converts to World Coordinates.
        if(debugVehicle == null)
        {
            return;
        }

        // Simulates the world position calculates.
        Vector3 worldTarget = debugVehicle.LocalToWorld(_localTarget);
        worldTarget += debugVehicle.direction * distance;

        // Draws Spheres for aiding on spheres used for calculations.
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(debugVehicle.transform.position + debugVehicle.direction * distance, radius);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(worldTarget, 1f);
    }
}
