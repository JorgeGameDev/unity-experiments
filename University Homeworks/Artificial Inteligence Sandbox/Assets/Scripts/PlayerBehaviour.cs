﻿using UnityEngine;
using System.Collections;

// Manages the Player's behaviour, such as switching in an out behaviours in case of need.
public class PlayerBehaviour : MonoBehaviour {

    // Internal Variables
    private bool _collectedTool;
    private SteeringVehicle _steeringVehichle;
    private bool _isCooldown;
    private WaitForSeconds _waitForSeconds;

	// Use this for initialization
	void Start ()
    {
        _steeringVehichle = GetComponent<SteeringVehicle>();
        _waitForSeconds = new WaitForSeconds(0.65f);
    }
	
	// Update is called once per frame
	void Update ()
    {
	    // Checks if the player is near the tool in order to collect it.
        if(!GameManager.gameManager.coreCollector)
        {
            // Checks if the player is near the tool in order to collect the tool.
            if(Vector3.Distance(transform.position, GameManager.gameManager.coreObject.transform.position) < 1f)
            {
                // Collects the Tool.
                _collectedTool = true;

                // Announces that the object was collected.
                GameManager.gameManager.ToolCollected(gameObject);
            }
        }
        else
        {
            // Checks if the player is near the other player to steal him the tool.
            if (!_collectedTool && Vector3.Distance(transform.position, GameManager.gameManager.coreHolder.transform.position) < 0.8f && !_isCooldown)
            {
                // Collects the Tool.
                _collectedTool = true;

                // Announces that the object was collected.
                GameManager.gameManager.ToolCollected(gameObject);
            }
        }

        // Clamps the player movement to the arena limits.
        Vector3 position = transform.position;
        position.x = Mathf.Clamp(position.x, GameManager.gameManager.minLimits.x, GameManager.gameManager.maxLimits.x);
        position.z = Mathf.Clamp(position.z, GameManager.gameManager.minLimits.y, GameManager.gameManager.maxLimits.y);
        transform.position = position;
	}

    // Restarts the player to its seek state.
    public void ClearBehaviours()
    {
        // Clears the list of already existing behaviours and removes the existing one.
        if (!gameObject.GetComponent<SteeringSeek>())
        {
            _steeringVehichle.RemoveBehaviours();
            RemoveComponentBehaviours();

            // Readds the seeking behaviour.
            SteeringSeek seeker = gameObject.AddComponent<SteeringSeek>();
            seeker.target = GameManager.gameManager.coreObject.transform;
            _steeringVehichle.steeringBehaviours.Add(seeker);

            // Changes the speed of the vehicle.
            _steeringVehichle.ResetVelocity();
            _steeringVehichle.maxForces = 12;
            _steeringVehichle.maxSpeed = 12;
        }
    }

    // Adds the functionality necessary to the steering vehicle if they fetched the tool.
    public void RunTool(GameObject playerEvade)
    {
        // Clears the list of already existing behaviours.
        _steeringVehichle.RemoveBehaviours();
        RemoveComponentBehaviours();

        // Adds the evade behaviour to the object.
        SteeringEvade evade = gameObject.AddComponent<SteeringEvade>();
        evade.evadeVehicle = playerEvade.GetComponent<SteeringVehicle>();
        evade.panicDistance = 10f;
        _steeringVehichle.steeringBehaviours.Add(evade);

        // Adds the wander behaviour to the object.
        SteeringWander wander = gameObject.AddComponent<SteeringWander>();
        wander.radius = 2;
        wander.jitter = 0.5f;
        wander.distance = 6f;
        _steeringVehichle.steeringBehaviours.Add(wander);

        // Changes the speed of the vehicle.
        _steeringVehichle.maxForces = 15;
        _steeringVehichle.maxSpeed = 15;
        
        // Starts Timer to avoid conflicts on switching behaviours.
        StartCoroutine(TouchTimer());
    }

    // Adds the functionality necessary to the steering vehicle if they have not collected the tool.
    public void ChaseTool(GameObject playerPursuit)
    {
        // Clears the already existing behaviours.
        _steeringVehichle.RemoveBehaviours();
        RemoveComponentBehaviours();

        // Adds the pursuit behaviour to the object.
        _collectedTool = false;
        SteeringPursuit pursuit = gameObject.AddComponent<SteeringPursuit>();
        pursuit.pursuitVehicle = playerPursuit.GetComponent<SteeringVehicle>();
        _steeringVehichle.steeringBehaviours.Add(pursuit);

        // Changes the speed of the vehicle.
        _steeringVehichle.maxForces = 12;
        _steeringVehichle.maxSpeed = 12;

        // Starts Timer to avoid conflicts on switching behaviours.
        StartCoroutine(TouchTimer());
    }

    // Does a cooldown to avoid that both players switch 
    IEnumerator TouchTimer()
    {
        // Starts the cooldown.
        _isCooldown = true;
        yield return _waitForSeconds;
        _isCooldown = false;
    }

    // Removes all the component behaviours from the player.
    private void RemoveComponentBehaviours()
    {
        if (gameObject.GetComponent<SteeringSeek>())
        {
            Destroy(gameObject.GetComponent<SteeringSeek>());
        }
        if (gameObject.GetComponent<SteeringPursuit>())
        {
            Destroy(gameObject.GetComponent<SteeringPursuit>());
        }
        if (gameObject.GetComponent<SteeringEvade>())
        {
            Destroy(gameObject.GetComponent<SteeringEvade>());
        }
        if (gameObject.GetComponent<SteeringWander>())
        {
            Destroy(gameObject.GetComponent<SteeringWander>());
        }
    }
}
