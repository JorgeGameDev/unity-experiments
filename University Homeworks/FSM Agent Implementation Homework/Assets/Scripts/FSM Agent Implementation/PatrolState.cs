﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Patrol State from the NPC Tank. Uses the Wander Steering Behaviour to navigate the map.
/// </summary>
public class PatrolState : State
{
    // Internal Components.
    private SteeringVehicle _vehicle;
    private NPCBehaviour _npcBehaviour;

    /// <summary>
    /// Events that happen when the State is started.
    /// </summary>
    public override void OnEnterState()
    {
        // Gets the necessary components.
        _vehicle = agent.GetComponent<SteeringVehicle>();
        _npcBehaviour = agent.GetComponent<NPCBehaviour>();

        // Adds the wandering steering behaviour to the agent.
        SteeringWander wander = agent.AddComponent<SteeringWander>();

        // Sets up the variables on the wander behaviour.
        wander.distance = 14.5f;
        wander.radius = 2f;
        wander.jitter = 0.3f;

        // Adds the behaviour to the list.
        _vehicle.steeringBehaviours.Insert(0, wander);

        // Changes the status text on the screen.
        TankGameManager.tankGameManager.ChangeStatusText(DetectionState.Undetected);
    }

    /// <summary>
    /// Events that happen when the State is running and being called on Update().
    /// </summary>
    public override void UpdateState()
    {
        // Updates the rotation of the agent.
        _npcBehaviour.gameObject.transform.rotation = Quaternion.LookRotation(_vehicle.direction);

        // Checks if the player is near this NPC tank.
        if (Vector3.Distance(_npcBehaviour.player.transform.position, agent.transform.position) < _npcBehaviour.chaseDistance)
        {
            _npcBehaviour.PlayerNear();
        }
    }

    /// <summary>
    /// Events that happen when the State is stopped.
    /// </summary>
    public override void OnExitState()
    {
        // Removes the wandering component.
        _vehicle.steeringBehaviours.RemoveAt(0);
        GameObject.Destroy(agent.GetComponent<SteeringWander>());
    }
}
