﻿using UnityEngine;
using System.Collections;

/// <summary>
/// State that defines an action or state of a FSM.
/// </summary>
public abstract class State {

    // Agent Object which owns a FSM with this state.
    public GameObject agent;
    public FiniteStateMachine parentFSM;

    // Defines the multiple functions that the FSM-State can use.
    public virtual void OnLoad() { }
    public virtual void UpdateState() { }
    public virtual void OnEnterState() { }
    public virtual void OnExitState() { }
}
