﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Attack State which makes the tank fire bullets against the player tank.
/// </summary>
public class AttackState : State {

    // Internal Components.
    private SteeringVehicle _vehicle;
    private NPCBehaviour _npcBehaviour;

    /// <summary>
    /// Events that happen when the State is started.
    /// </summary>
    public override void OnEnterState()
    {
        // Gets the necessary components.
        _vehicle = agent.GetComponent<SteeringVehicle>();
        _npcBehaviour = agent.GetComponent<NPCBehaviour>();

        // Changes the status text on the screen.
        TankGameManager.tankGameManager.ChangeStatusText(DetectionState.Attacking);
    }

    /// <summary>
    /// Events that happen when the State is running and being called on Update().
    /// </summary>
    public override void UpdateState()
    {
        // Gets the direction between the two tanks and updates the rotation.
        Vector3 direction = (_npcBehaviour.player.transform.position - _npcBehaviour.gameObject.transform.position);
        _npcBehaviour.gameObject.transform.rotation = Quaternion.LookRotation(direction);
        _vehicle.direction = direction;

        // Checks if the tank is no longer close to the player to attack.
        if (Vector3.Distance(_npcBehaviour.player.transform.position, agent.transform.position) > _npcBehaviour.attackDistance)
        {
            // Goes back to changing the player.
            _npcBehaviour.PlayerNear();
        }

        // Fires a bullet.
        if(!_npcBehaviour.hasFired)
        {
            ShootFire();
        }
    }

    /// <summary>
    /// Called when firing a bullet.
    /// </summary>
    void ShootFire()
    {
        // Creates an instance of the bullet being fired.
        GameObject firedBullet = (GameObject)GameObject.Instantiate(_npcBehaviour.bulletGameObject, _npcBehaviour.shootTransform.position, _npcBehaviour.shootTransform.rotation);
        Rigidbody bulletRigid = firedBullet.GetComponent<Rigidbody>();

        // Applies the velocity to the bullet being fired.
        bulletRigid.velocity = _npcBehaviour.launchForce * _npcBehaviour.shootTransform.forward;

        // Plays the Audio Clip on the shoot transform.
        _npcBehaviour.fireSource.Play();

        // Starts a coroutine for timing between shooting.
        _npcBehaviour.StartCoroutine(_npcBehaviour.TimeBetweenFire());
    }
}
