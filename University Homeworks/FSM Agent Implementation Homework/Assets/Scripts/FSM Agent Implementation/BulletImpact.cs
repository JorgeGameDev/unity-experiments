﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Manages the bullets that are fired from the tank, and calls the Game Manager whenever necessary.
/// </summary>
public class BulletImpact : MonoBehaviour {

    [Header("Bullet Properties")]
    public LayerMask tanksLayer;
    public float bulletLifeTime;

    [Header("Explosion Properties")]
    public float explosionRadius;
    public ParticleSystem explosionParticles;
    public ParticleSystem tankExplosion;
    public AudioSource explosionAudio;

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Start ()
    {
        // Destroys the object after a certain period of time.
        Destroy(gameObject, bulletLifeTime);
	}

    /// <summary>
    /// Checks if something has touched the bullet trigger.
    /// </summary>
    void OnTriggerEnter(Collider other)
    {
        // Does an overlap sphere to check for any tank that the bullet might have hit.
        Collider[] tankHit = Physics.OverlapSphere(transform.position, explosionRadius, tanksLayer);

        // Checks which tanks the bullet has hit.
        foreach(Collider tank in tankHit)
        {
            // Creates a tank explosion on the spot.
            ParticleSystem tankExplode = (ParticleSystem)Instantiate(tankExplosion, tank.transform.position, Quaternion.identity);
            tankExplode.Play();
            tankExplode.GetComponent<AudioSource>().Play();

            // Destroys the tank explosion after it's duration is over. 
            Destroy(tankExplode.gameObject, tankExplode.duration);

            if (tank.CompareTag("Player"))
            {
                TankGameManager.tankGameManager.RespawnPlayer();
            }

            if(tank.CompareTag("Enemy"))
            {
                TankGameManager.tankGameManager.RespawnEnemy();
            }
        }

        // Setups the Particle System and Audio Sources of the explosion.
        explosionParticles.transform.parent = null;
        explosionParticles.Play();
        explosionAudio.Play();

        // Destroys the Particle System after their duration is over. Also destroys the bullet.
        Destroy(explosionParticles.gameObject, explosionParticles.duration);
        Destroy(gameObject);
    }
}
