﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Chase State from the NPC Tank. Uses Pursuit in order to attempt to get near the player's tank.
/// </summary>
public class ChaseState : State
{
    // Internal Components.
    private SteeringVehicle _vehicle;
    private NPCBehaviour _npcBehaviour;

    /// <summary>
    /// Events that happen when the State is started.
    /// </summary>
    public override void OnEnterState()
    {
        // Gets the necessary components.
        _vehicle = agent.GetComponent<SteeringVehicle>();
        _npcBehaviour = agent.GetComponent<NPCBehaviour>();

        // Adds the pursuit steering behaviour.
        SteeringPursuit pursuit = agent.AddComponent<SteeringPursuit>();
        _vehicle.steeringBehaviours.Insert(0, pursuit);

        // Pursuits the player's tank.
        pursuit.pursuitVehicle = _npcBehaviour.player.GetComponent<SteeringVehicle>();

        // Changes the status text on the screen.
        TankGameManager.tankGameManager.ChangeStatusText(DetectionState.Chased);
    }

    /// <summary>
    /// Events that happen when the State is running and being called on Update().
    /// </summary>
    public override void UpdateState()
    {
        // Updates the rotation of the agent.
        _npcBehaviour.gameObject.transform.rotation = Quaternion.LookRotation(_vehicle.direction);

        // Checks if the tank is close to the player.
        if (Vector3.Distance(_npcBehaviour.player.transform.position, agent.transform.position) < _npcBehaviour.attackDistance)
        {
            // Starts the attack sequence of the tank.
            _npcBehaviour.AttackPlayer();
        }

        // Checks if the player has evaded the tank.
        if (Vector3.Distance(_npcBehaviour.player.transform.position, agent.transform.position) > _npcBehaviour.chaseDistance * 2)
        {
            // Starts patrolling again, searching for the player tank.
            _npcBehaviour.PlayerEscaped();
        }
    }

    /// <summary>
    /// Events that happen when the State is stopped.
    /// </summary>
    public override void OnExitState()
    {
        // Removes the Pursuit Behaviour.
        _vehicle.steeringBehaviours.RemoveAt(0);
        GameObject.Destroy(agent.GetComponent<SteeringPursuit>());
    }
}
