﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Follows the player tank at a certain distance.
/// </summary>
public class TanksCamera : MonoBehaviour {

    [Header("Camera Following")]
    public Transform tankCameraTransform;

    // Component Reference
    private Vector3 _cameraOffset;

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Awake ()
    {
        // Calculates the offset between the camera and reference point of the tank.
        _cameraOffset = transform.position - tankCameraTransform.transform.position;
	}

    /// <summary>
    /// Late Update is called at the end of a frame.
    /// </summary>
    void LateUpdate ()
    {
        // Calculates the position for the camera to follow and rotate around the player.
        float cameraAngle = tankCameraTransform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, cameraAngle, 0);
        transform.position = tankCameraTransform.position + (rotation * _cameraOffset);
        transform.LookAt(tankCameraTransform);
    }
}
