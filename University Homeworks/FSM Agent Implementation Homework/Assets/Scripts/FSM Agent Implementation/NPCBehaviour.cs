﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Used for controlling the NPC tank and it's behaviour states.
/// </summary>
public class NPCBehaviour : MonoBehaviour {

    [Header("Tank Properties")]
    public float chaseDistance;
    public float attackDistance;

    [Header("Audio Clips")]
    public AudioClip tankIdle;
    public AudioClip tankActive;

    [Header("Shooting Properties")]
    public float timeBetweenFire;
    public AudioSource fireSource;
    public GameObject bulletGameObject;
    public Transform shootTransform;
    public float launchForce;

    [Header("Reference Components")]
    [ReadOnly]
    public GameObject player;

    // Internal Variables
    [HideInInspector]
    public bool hasFired;

    // Finite State Machine
    private FiniteStateMachine fsm;

    // Internal Components.
    private WaitForSeconds _waitForSeconds;
    private AudioSource _audioSource;
    private Rigidbody _rigidbody;

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Start ()
    {
        // Gets the FSM and other necessary components.
        fsm = GetComponent<FiniteStateMachine>();
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();

        // Get's a player reference.
        player = TankGameManager.tankGameManager.playerTank;

        // Loads all the states used by the Tank.
        fsm.LoadState<PatrolState>();
        fsm.LoadState<ChaseState>();
        fsm.LoadState<AttackState>();

        // Activates the patrol state, which will make the tank wander around the map.
        fsm.ActivateState<PatrolState>();

        // Creates a Wait For Seconds to avoid a Coroutine Memory Leak.
        _waitForSeconds = new WaitForSeconds(timeBetweenFire);
    }

    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    void Update()
    {
        // Checks the tank's movement and changes the audio clip if necessary.
        if (_rigidbody.velocity.magnitude != 0)
        {
            if(_audioSource.clip == tankIdle)
            {
                _audioSource.clip = tankActive;
                _audioSource.Play();
            }
        }
        else
        {
            if (_audioSource.clip == tankActive)
            {
                _audioSource.clip = tankIdle;
                _audioSource.Play();
            }
        }
    }

    /// <summary>
    /// Called when the player is found. Activates the Chase State.
    /// </summary>
    public void PlayerNear()
    {
        // Activates the Chase State.
        fsm.ActivateState<ChaseState>();
    }

    /// <summary>
    /// Called when the player escapes the enemy tank. Activates the Patrol State again.
    /// </summary>
    public void PlayerEscaped()
    {
        // Activates the patrol state again.
        fsm.ActivateState<PatrolState>();
    }

    /// <summary>
    /// Called when the enemy tank should attack the player.
    /// </summary>
    public void AttackPlayer()
    {
        // Activates the attack state.
        fsm.ActivateState<AttackState>();
    }

    /// <summary>
    /// Enumerator for waiting time before firing new bullets on the Attack State.
    /// </summary>
    public IEnumerator TimeBetweenFire()
    {
        hasFired = true;
        yield return _waitForSeconds;
        hasFired = false;
    }

    /// <summary>
    /// Draws Gizmos on screen to define the tank detection areas.
    /// </summary>
    void OnDrawGizmosSelected()
    {
        // Blue for Chasing Radius.
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, chaseDistance);

        // Red for Attack Radius.
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackDistance);

        // Green for Safe Radius.
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, chaseDistance * 1.45f);
    }
}
