﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Manages the current status of the tank game.
/// </summary>
public class TankGameManager : MonoBehaviour {

    // Static Reference
    public static TankGameManager tankGameManager;

    [Header("Game Elements")]
    public Transform spawnTransform;
    [ReadOnly]
    public List<Transform> spawnPoints = new List<Transform>();
    [ReadOnly]
    public GameObject playerTank;
    [ReadOnly]
    public GameObject enemyTank;
    [ReadOnly]
    public int destroyStreak;

    [Header("UI Elements")]
    [ReadOnly]
    public Text streakText;
    [ReadOnly]
    public Text statusText;
    [ReadOnly]
    public Color undetectedColor;
    [ReadOnly]
    public Color chasedColor;
    [ReadOnly]
    public Color attackedColor;

    /// <summary>
    /// Use this for early initialization.
    /// </summary>
    void Awake()
    {
        // Makes this Game Manager instance the singleton.
        if (tankGameManager == null)
        {
            tankGameManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        // Gets the player and enemy tanks.
        playerTank = GameObject.FindGameObjectWithTag("Player");
        enemyTank = GameObject.FindGameObjectWithTag("Enemy");
    }

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Start()
    {
        // Gets the spawn points from the Spawn Manager.
        foreach(Transform spawn in spawnTransform)
        {
            spawnPoints.Add(spawn);
        }

        // Gets a random spawn point for each of the tanks and places them there.
        int randomSpawn = Random.Range(0, spawnPoints.Count);
        playerTank.transform.position = spawnPoints[randomSpawn].position;
        randomSpawn = Random.Range(0, spawnPoints.Count);
        enemyTank.transform.position = spawnPoints[randomSpawn].position;
    }

    /// <summary>
    /// Called when changing the status text to a different text.
    /// </summary>
    public void ChangeStatusText(DetectionState state)
    {
        // Gets the outline of the text that's going to be changed.
        Outline statusOutline = statusText.GetComponent<Outline>();

        // Switch Statement with the different states.
        switch(state)
        {
            case DetectionState.Undetected:
                statusText.text = "Undetected";
                statusOutline.effectColor = undetectedColor;
                break;
            case DetectionState.Chased:
                statusText.text = "Being Chased!";
                statusOutline.effectColor = chasedColor;
                break;
            case DetectionState.Attacking:
                statusText.text = "Being Attacked!";
                statusOutline.effectColor = attackedColor;
                break;
        }
    }

    /// <summary>
    /// Used for respawning the player's tank when it's destroyed.
    /// </summary>
    public void RespawnPlayer()
    {
        // Respawns the player.
        int randomSpawn = Random.Range(0, spawnPoints.Count);
        playerTank.transform.position = spawnPoints[randomSpawn].position;

        // Resets the Destroy Streak.
        destroyStreak = 0;
        streakText.text = destroyStreak.ToString();
    }

    /// <summary>
    /// Used for respawning the enemy tank when it's destroyed.
    /// </summary>
    public void RespawnEnemy()
    {
        // Respawns the enemy.
        int randomSpawn = Random.Range(0, spawnPoints.Count);
        enemyTank.transform.position = spawnPoints[randomSpawn].position;

        // Adds to the Destroy Streak
        ++destroyStreak;
        streakText.text = destroyStreak.ToString();
    }
}

/// <summary>
/// Enumerator for defining the Detection State.
/// </summary>
public enum DetectionState
{
    Undetected,
    Chased,
    Attacking
}
