﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Used for controlling the player - the green tank - around the map, as well as it's shots.
/// </summary>
public class PlayerTank : MonoBehaviour {

    [Header("Movement Properties")]
	public float moveSpeed;
	public float rotationSpeed;

    [Header("Shooting Properties")]
    public float timeBetweenFire;
    public GameObject bulletGameObject;
    public Transform shootTransform;
    public float launchForce;

    [Header("Audio Clips & Sources")]
    public AudioSource fireSource;
    public AudioClip tankIdle;
    public AudioClip tankActive;

    // Internal Variables
    private bool _hasFired;

    // Internal Components
	private Rigidbody _rigidbody;
    private AudioSource _audioSource;
    private WaitForSeconds _waitForSeconds;

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Start ()
    {
        // Gets the components necessary.
		_rigidbody = GetComponent<Rigidbody> ();
        _audioSource = GetComponent<AudioSource>();

        // Creates a Wait For Seconds to avoid a Coroutine Memory Leak.
        _waitForSeconds = new WaitForSeconds(timeBetweenFire);
    }

    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    void Update()
    {
        // Rotates the tank in a given rotation.
        float horAxis = Input.GetAxis("Horizontal");
        transform.Rotate(0, horAxis * rotationSpeed * Time.deltaTime, 0);

        // Checks if the player has asked for a bullet to be fire.
        if(!_hasFired && Input.GetButtonDown("TankFire"))
        {
            ShootFire();
        }
    }

    /// <summary>
    /// Fixed update is called once per physical frame.
    /// </summary>
    void FixedUpdate()
    {
        // Applies the velocity to the tank.
		float verAxis = Input.GetAxis ("Vertical");
		_rigidbody.velocity = transform.forward * verAxis * moveSpeed;

        // Checks the tank's movement and changes the audio clip if necessary.
        if (verAxis != 0)
        {
            if (_audioSource.clip == tankIdle)
            {
                _audioSource.clip = tankActive;
                _audioSource.Play();
            }
        }
        else
        {
            if (_audioSource.clip == tankActive)
            {
                _audioSource.clip = tankIdle;
                _audioSource.Play();
            }
        }
    }

    /// <summary>
    /// Called when the player requests a bullet to be fired.
    /// </summary>
    void ShootFire()
    {
        // Creates an instance of the bullet being fired.
        GameObject firedBullet = (GameObject)Instantiate(bulletGameObject, shootTransform.position, shootTransform.rotation);
        Rigidbody bulletRigid = firedBullet.GetComponent<Rigidbody>();

        // Applies the velocity to the bullet being fired.
        bulletRigid.velocity = launchForce * shootTransform.forward;

        // Plays the Audio Clip on the shoot transform.
        fireSource.Play();

        // Starts a coroutine for timing between shooting.
        StartCoroutine(TimeBetweenFire());
    }

    /// <summary>
    /// Enumerator for waiting time before firing new bullets on the Attack State.
    /// </summary>
    public IEnumerator TimeBetweenFire()
    {
        _hasFired = true;
        yield return _waitForSeconds;
        _hasFired = false;
    }
}
