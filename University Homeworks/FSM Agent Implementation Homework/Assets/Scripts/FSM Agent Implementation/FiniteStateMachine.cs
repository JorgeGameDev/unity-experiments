﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

// Finite State Machine that tracks the behaviours applied in the object.
public class FiniteStateMachine : MonoBehaviour {

    // Finite State Machine States
    private Dictionary<Type, State> states;
    private State activeState;

	// Use this for initialization
	void Awake () {
        states = new Dictionary<Type, State>();
	}
	
	// Update is called once per frame
	void Update () {
        // Runs the current state.
	    if(activeState != null)
        {
            activeState.UpdateState();
        }
	}

    // Loads a specific state.  // Constraints
    public T LoadState<T>() where T : State, new()
    {
        // Creates a new state of a certain type.
        T state = new T ();
        Type type = typeof(T);

        // Initializes the state and assign it's variables.
        states.Add(type, state);
        state.agent = gameObject;
        state.parentFSM = this;
        state.OnLoad();
        return state;
    }

    // Activates and checks for the existence of a specific state.
    public void ActivateState<T>() where T: State
    {
        // Gets the State from the Dictionary.
        Type type = typeof(T);
        State state = states[type];

        // Checks if there's an already active state.
        if(activeState != null)
        {
            activeState.OnExitState();
        }

        // Enters and Activates the state.
        activeState = state;
        state.OnEnterState();
    }
}
