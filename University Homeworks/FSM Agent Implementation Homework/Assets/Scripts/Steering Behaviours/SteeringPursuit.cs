﻿using UnityEngine;
using System.Collections;
using System;

// Chases an object that is moving trying to find their destination and trajectory
public class SteeringPursuit : SteeringBehaviour {

    [Header("Pursuit Properties")]
    public SteeringVehicle pursuitVehicle;

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        // Calculates the difference between the vehicles.
        Vector3 toPursuit = vehicle.transform.position - pursuitVehicle.transform.position;

        // Calculates the relative direction between the two vehicles, and checks if they are facing each other.
        float relativeDirection = Vector3.Dot(vehicle.direction, pursuitVehicle.direction);
        if (relativeDirection < -0.95f)
        {
            return helper.Seek(vehicle, pursuitVehicle.transform.position);
        }
        else
        {
            // Predicts the destination of the other vehicle in order to pursuit it.
            float lookAhead = toPursuit.magnitude / (pursuitVehicle.velocity.magnitude + vehicle.maxSpeed);
            Vector3 target = pursuitVehicle.transform.position + (pursuitVehicle.direction * lookAhead);
            return helper.Seek(vehicle, target);
        }
    }
}
