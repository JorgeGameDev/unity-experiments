﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionSmoother {

	Vector3[] history;
	int currSample;

	public DirectionSmoother(int numSamples, Vector3 startDirection){
		history = new Vector3[numSamples];
		for (int i = 0; i < history.Length; i++) {
			history [i] = startDirection;
		}
	}

	public Vector3 UpdateSmooth(Vector3 direction){
		history [currSample] = direction;
		currSample++;
		if (currSample >= history.Length) {
			currSample = 0;
		}
		Vector3 average = new Vector3 ();
		for (int i = 0; i < history.Length; i++) {
			average += history [i];
		}
		average /= (float)history.Length;
		return average;
	}

}
