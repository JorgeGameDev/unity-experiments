﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Base class for a Steering Vehicle.
public class SteeringVehicle : MonoBehaviour
{
    [Header("Behaviors")]
    public bool useSmoothing;
    public CombineMode combineMode;
    public List<SteeringBehaviour> steeringBehaviours;

    [Header("Steering Properties")]
    public Vector3 direction;
    public float maxForces;
    public float maxSpeed;
    public float decelaration;
    public float mass
    {
        get // Returns the Mass of the Rigidbody.
        {
            return _rigidbody.mass;
        }
        set // Sets the mass of the rigidbody to be the value parsed.
        {
            _rigidbody.mass = value;
        }
    }
    public Vector3 velocity
    {
        get
        {
            return _rigidbody.velocity;
        }
    }

    // Components
    private Rigidbody _rigidbody;

    // Internal Helper Functions.
    private DirectionSmoother _directionSmoother;

    // Called before scene render.
    public void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    public void Start()
    {
        direction = transform.forward;
        SortBehaviours();
        _directionSmoother = new DirectionSmoother(20, direction);
    }

    // Update is called once per frame.
    void FixedUpdate()
    {
        // Calculates and accumulates all the forces that exist on our behaviors.
        Vector3 forces = new Vector3();

        // Checks the type of force combining we're using.
        switch(combineMode)
        {
            case CombineMode.Unclamped:
                forces = CalculateUnclamped();
                break;
            case CombineMode.Weight:
                forces = CalculateWithWeight();
                break;
            case CombineMode.Priority:
                forces = CalculateWithPriority();
                break;

        }

        // Adds every single force to the rigidbody.
        _rigidbody.AddForce(forces);

        // Stores variables related to rigidbody information.
        float minimumDistance = 0.1f;
        float currentSpeed = _rigidbody.velocity.magnitude;

        // Stores the velocity destination given a minimum threshold.
        if (currentSpeed > minimumDistance)
        {
            direction = _rigidbody.velocity / currentSpeed;
            if(useSmoothing)
            {
                direction = _directionSmoother.UpdateSmooth(direction);
            }
        }

        // Same thing to the velocity.
        _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, maxSpeed);
    }

    // Calculates forces, unclamped.
    private Vector3 CalculateUnclamped()
    {
        Vector3 forces = new Vector3();
        foreach (SteeringBehaviour behaviour in steeringBehaviours)
        {
            forces += behaviour.Calculate(this);
        }

        // Clamps the forces to their limit.
        forces = Vector3.ClampMagnitude(forces, maxForces);

        // Returns the Forces.
        return forces;
    }

    // Calculates and Limits forces based on weight.
    private Vector3 CalculateWithWeight()
    {
        Vector3 forces = new Vector3();
        foreach (SteeringBehaviour behaviour in steeringBehaviours)
        {
            forces += behaviour.Calculate(this) * behaviour.weight;
        }

        // Clamps the forces to their limit.
        forces = Vector3.ClampMagnitude(forces, maxForces);

        // Returns the Forces.
        return forces;
    }

    // Calculates forces with priority. 
    private Vector3 CalculateWithPriority()
    {
        // Initializes variables.
        Vector3 totalForces = new Vector3();
        float forcesAllowed = maxForces;
        Vector3 forces;

        // Does calculations for every steering behavior.
        foreach (SteeringBehaviour behaviour in steeringBehaviours)
        {
            // Checks if forces are still allowed.
            if (forcesAllowed <= 0)
            {
                break;
            }

            // Calculates if our forces haven't gone over the limit yet.
            Vector3 force = behaviour.Calculate(this) * behaviour.weight;
            float forceAmount = force.magnitude;
            if (forceAmount > forcesAllowed)
            {
                force = force.normalized * forcesAllowed;
                forcesAllowed = 0;
            }
            else
            {
                forcesAllowed -= forceAmount;
            }

            // Adds the total forces.
            totalForces += force;
        }

        // States the forces.
        forces = totalForces;
        return forces;
    }

    // Sorts the behaviours in the list based on their priority.
    public void SortBehaviours()
    {
        steeringBehaviours.Sort((a, b) => -a.priority.CompareTo(b.priority));
    }

    // Used for clearing the Steering Behaviours from the list.
    public void RemoveBehaviours()
    {
        steeringBehaviours.Clear();
    }

    // Used for reseting the velocity of the rigidbody.
    public void ResetVelocity()
    {
        _rigidbody.velocity = Vector3.zero;
    }

    // Converts a local position to the world position.
    public Vector3 LocalToWorld(Vector3 local)
    {
        // Constructs a matrix with the transformations of the vehicle.
        Vector3 position = transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        Vector3 scale = Vector3.one;
        Matrix4x4 matrix = Matrix4x4.TRS(position, rotation, scale);

        // Returns the multiplied matrix.
        return matrix.MultiplyPoint(local);
    }
}

public enum CombineMode
{
    Unclamped,
    Weight,
    Priority,
}
