﻿using UnityEngine;
using System.Collections;

public abstract class SteeringBehaviour : MonoBehaviour {

    [Header("Weight & Prioritisation")]
    public float weight = 1f;
    public float priority;

    // Creates an Helper Object to help with 
    protected SteeringHelper helper = new SteeringHelper();
    
    // Base skeleton for calculate 
    public abstract Vector3 Calculate(SteeringVehicle vehicle);
}
