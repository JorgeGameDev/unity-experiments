﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

// Checks if the player has fallen under a certain cordinate which resets the scene.
public class ResetFloor : MonoBehaviour {

    [Header("Kill Cordinate")]
    public float killY;
	
	// Update is called once per frame
	void Update () {
        // Checsk and reloads the scene if necessary.
	    if(transform.position.y < killY)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}
}
