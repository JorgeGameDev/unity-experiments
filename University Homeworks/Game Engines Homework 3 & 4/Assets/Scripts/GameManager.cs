﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

// Manages the Game State and Canvas.
public class GameManager : MonoBehaviour {

    public static GameManager gameManager;

    [Header("Game Status")]
    public bool playerAlive;
    public GameObject gameOverObject;

    [Header("Enemy Spawning")]
    public GameObject enemyObject;
    public float enemyCooldown;
    public AudioClip playerDamageClip;

    [Header("Player Health")]
    public float playerHealth;
    public RectTransform healthRectTransform;

    [Header("UI Score")]
    public Text scoreText;
    public AudioClip scoreClip;
    private int _score;

    // Internal
    private AudioSource _audioSource;
    private Transform[] _enemySpawns;
    private WaitForSeconds _enemyTimer;
    private bool _isCoolDown;

    // Use this for initialization
    void Start () {
        // Sets up the necessary references and gameobjects for the enemy spawning.
        gameManager = this;
        _audioSource = GetComponent<AudioSource>();
        _enemyTimer = new WaitForSeconds(enemyCooldown);
        _enemySpawns = new Transform[transform.childCount];
        for(int i = 0; i < transform.childCount; i++)
        {
            _enemySpawns[i] = transform.GetChild(i);
        }

        // Sets player as alive.
        playerAlive = true;
	}
	
	// Update is called once per frame
	void Update () {
	    if(playerAlive && !_isCoolDown)
        {
            StartCoroutine(EnemySpawn());
        }

        // If the player is not alive, reset the scene once the player hit Enter.
        if(!playerAlive)
        {
            if(Input.GetButtonDown("Submit"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
	}

    // Used to spawn enemies after a certain cooldown.
    IEnumerator EnemySpawn()
    {
        _isCoolDown = true;
        // Spawns the enemy at a random transform point.
        Transform randomSpawn = _enemySpawns[Random.Range(0, _enemySpawns.Length)];
        Instantiate(enemyObject, randomSpawn.transform.position, Quaternion.identity);
        yield return _enemyTimer;
        _isCoolDown = false;
        
    }

    // Adds a point to the score counter.
    public void AddPoint()
    {
        _score++;
        scoreText.text = _score.ToString();
        _audioSource.PlayOneShot(scoreClip);
    }

    // Applies damage to the player.
    public void PlayerTakeDamage(int damage)
    {
        // Applies damage.
        _audioSource.PlayOneShot(playerDamageClip);
        playerHealth -= damage;

        // Checks if the enemy has gone under the limit health.
        if (playerHealth <= 0)
        {
            playerAlive = false;
            gameOverObject.GetComponent<Animator>().SetTrigger("GameOverShow");
        }

        // Gets the rect transform of the healthbar and updates it to reflect the enemy status.
        Vector2 sizeDelta = healthRectTransform.sizeDelta;
        sizeDelta.x = (playerHealth * 152) / 100;
        healthRectTransform.sizeDelta = sizeDelta;
    }
}
