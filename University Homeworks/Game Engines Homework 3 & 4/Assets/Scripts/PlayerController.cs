﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Manages Player and Camera Movement.
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {

    [Header("Player Properties")]
    public float speed;
    public float rotSpeed;
    public Transform groundPoint;
    public Transform lookPoint;

    [Header("Camera Control")]
    public float cameraSensetivity;
    public Vector2 limitCameraX;

    [Header("Bullet Spawn")]
    public GameObject bulletGO;
    public Transform bulletSpawn;

    // Internal Components
    public float bulletCooldown;
    private WaitForSeconds _bulletWait;
    private bool _isBulletCool;
    private AudioSource _audioSource;
    private bool _isJumping;
    private Rigidbody _rigidbody;
    private Camera _camera;

	// Use this for initialization
	void Start () {
        // Gets the necessary components.
        _camera = Camera.main;
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
        Cursor.lockState = CursorLockMode.Locked;

        // Creates the timer for the bullet cooldown.
        _bulletWait = new WaitForSeconds(bulletCooldown);
    }
	
	// Update is called once every physics second.
	void FixedUpdate ()
    {
        // Checks if the mouse is locked, if not, locks it.
        if(Cursor.lockState == CursorLockMode.None)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        if(GameManager.gameManager.playerAlive)
        {
            PlayerMovement();
            MouseMovement();
            FireBullet();
        }
    }

    // Used to update the player position.
    void PlayerMovement()
    {
        // Updates the player velocity and camera's position depending on the player's input.
        float verInput = Input.GetAxis("Vertical");
        float horInput = Input.GetAxis("Horizontal");

        // Rotates the player with the new velocity.
        Vector3 newMovement = Vector3.zero;
        bool isGrounded = Physics.CheckSphere(groundPoint.transform.position, 0.001f);

        // Checks if the player has pressed the jump key.
        if (isGrounded)
        {
            _isJumping = false;
            if (Input.GetButtonDown("Jump") && !_isJumping)
            {
                _isJumping = true;
                _rigidbody.AddForce(Vector3.up * 200);
            }
        }
        else if (!isGrounded)
        {
            _isJumping = true;
        }

        // Calculates the movement change in the player.
        newMovement.z = verInput * speed * Time.deltaTime;
        newMovement.x = horInput * speed * Time.deltaTime;
        transform.Translate(newMovement);
    }

    // Used to change the camera position based on the mouse position.
    void MouseMovement()
    {
        // Gets the mouse variables.
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        // Updates the camera rotation based on the axis movement.
        Vector3 rotation = _camera.transform.rotation.eulerAngles;

        // Adds to the rotation.
        rotation.x += mouseY;
        rotation.y += mouseX * cameraSensetivity;

        // Clamps the rotation in the X axis and applies it to the camera.
        _camera.transform.localRotation = Quaternion.AngleAxis(rotation.x, Vector3.right);

        // Updates the player rotation acording to the camera.
        Vector3 playerRotation = new Vector3(0, rotation.y, 0);
        transform.rotation = Quaternion.Euler(playerRotation);
    }

    // Fires a bullet whenever the player asks for an input.
    void FireBullet()
    {
        if(Input.GetButtonDown("Fire1") && !_isBulletCool)
        {
            // Creates a new bullet and sets it's velocity.
            GameObject newBullet = (GameObject)Instantiate(bulletGO, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
            newBullet.GetComponent<Rigidbody>().velocity = newBullet.transform.forward * 5;
            Destroy(newBullet, 1.5f);

            // Plays Gun Sound and starts timer.
            _audioSource.Play();
            StartCoroutine(BulletCooldown());
        }
    }

    // Times the bullet cool down.
    IEnumerator BulletCooldown()
    {
        _isBulletCool = true;
        yield return _bulletWait;
        _isBulletCool = false;
    }
}
