﻿using UnityEngine;
using System.Collections;

// Destroys the bullet on collision with an object. If the object is a specific one, interacts with it.
public class BulletDestroy : MonoBehaviour {

    // Checks for collisions.
    public void OnCollisionEnter(Collision collision)
    {
        // Checks if the bullet has hit an Enemy.
        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<EnemyBehaviour>().TakeDamage(25);
        }

        Destroy(gameObject);
    }
}
