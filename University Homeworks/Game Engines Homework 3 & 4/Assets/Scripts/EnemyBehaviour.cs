﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Manages all the enemy behaviour, health and collisions.
public class EnemyBehaviour : MonoBehaviour
{
    [Header("Movement AI")]
    public GameObject playerObject;

    [Header("Enemy Status")]
    public float health;
    public RectTransform healthRectTransform;

    [Header("Player Damage")]
    public int damageValue;
    public float timerCooldown;

    // Internal
    private WaitForSeconds _waitTimer;
    private bool _isCoolDown;
    private NavMeshAgent _navmeshAgent;
    private AudioSource _audioSource;

    // Use this for initialization
    void Start()
    {
        _navmeshAgent = GetComponent<NavMeshAgent>();
        _audioSource = GetComponent<AudioSource>();
        playerObject = GameObject.FindGameObjectWithTag("Player");
        _waitTimer = new WaitForSeconds(timerCooldown);
    }

    // Update is called everyframe.
    void Update()
    {
        // Checks if the player is alive.
        if(GameManager.gameManager.playerAlive)
        {
            // Checks the distance between the player and this enemy to see which action to do.
            if (Vector3.Distance(gameObject.transform.position, playerObject.transform.position) > 2)
            {
                // Resumes the agent if this one is stopped and set's the destination for the player.
                _navmeshAgent.Resume();
                _navmeshAgent.SetDestination(playerObject.transform.position);
            }
            else
            {
                // Stops the Agent.
                _navmeshAgent.Stop();

                // Checks if the player is still alive and that the damage isn't in cooldown.
                if (GameManager.gameManager.playerAlive && !_isCoolDown)
                {
                    StartCoroutine(PlayerDamage());
                }
            }
        }
        else
        {
            _navmeshAgent.Stop();
        }
    }

    // Used for the enemy to take damage.
    public void TakeDamage(int damage)
    {
        // Applies damage.
        _audioSource.Play();
        health -= damage;

        // Checks if the enemy has gone under the limit health.
        if(health <= 0)
        {
            Destroy(gameObject);
            GameManager.gameManager.AddPoint();
        }

        // Gets the rect transform of the healthbar and updates it to reflect the enemy status.
        Vector2 sizeDelta = healthRectTransform.sizeDelta;
        sizeDelta.x = (health * 1.5f) / 100;
        healthRectTransform.sizeDelta = sizeDelta;
    }

    // Used for the player to take damage.
    IEnumerator PlayerDamage()
    {
        // Announces that the damage is in cooldown.
        _isCoolDown = true;

        // Applies the damage and starts timer.
        GameManager.gameManager.PlayerTakeDamage(damageValue);
        yield return _waitTimer;

        // Stops the cooldown.
        _isCoolDown = false;
    }
}
