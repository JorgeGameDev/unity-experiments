﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Manages all the enemy behaviour, health and collisions.
public class EnemyBehaviour : MonoBehaviour
{
    [Header("Enemy Status")]
    public float health;
    public float detectRadius;

    [Header("Bullet Spawn")]
    public GameObject bulletGO;
    public Transform bulletSpawn;
    public int damageValue;
    public float bulletCooldown;

    [Header("Collectibles")]
    public GameObject collectibleGameObject;

    // Internal Components
    private GameObject _playerObject;
    private WaitForSeconds _bulletWait;
    private bool _isCoolDown;
    private AudioSource _audioSource;

    // Use this for initialization
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _bulletWait = new WaitForSeconds(bulletCooldown);
        _playerObject = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called everyframe.
    void Update()
    {
        // Checks if the the player is inside the check radius.
        if(_playerObject != null)
        {
            if (Vector3.Distance(transform.position, _playerObject.transform.position) < detectRadius)
            {
                // Rotates the enemy towards the player.
                Vector3 targetDirection = _playerObject.transform.position - transform.position;
                Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 3 * Time.deltaTime, 0f);
                newDirection.y = 0;
                transform.rotation = Quaternion.LookRotation(newDirection);

                // Fires a bullet if the enemy is not in cooldown.
                if (!_isCoolDown)
                {
                    StartCoroutine(FireBullet());
                }
            }
        }
    }

    // Used for the enemy to take damage.
    public void TakeDamage(int damage)
    {
        // Applies damage.
        GameManager.gameManager.EnemyDamageClip();
        health -= damage;

        // Checks if the enemy has gone under the limit health.
        if(health <= 0)
        {
            // Creates an extra collectible game object.
            Vector3 newCollectiblePos = transform.position;
            newCollectiblePos.y += 1.0f;
            Instantiate(collectibleGameObject, newCollectiblePos, Quaternion.identity);

            // Destroys this enemy.
            Destroy(gameObject);
        }
    }

    // Used for the player to take damage.
    IEnumerator FireBullet()
    {
        // Announces that the damage is in cooldown.
        _isCoolDown = true;

        // Applies the damage and starts timer.
        // Applies the damage and velocity to the bullet.
        GameObject newBullet = (GameObject)Instantiate(bulletGO, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
        newBullet.GetComponent<Rigidbody>().velocity = newBullet.transform.forward * 5;
        Destroy(newBullet, 1.5f);

        // Plays Gun Sound and starts timer.
        _audioSource.Play();
        yield return _bulletWait;

        // Stops the cooldown.
        _isCoolDown = false;
    }

    // For debugging, shows gizmos with the radius of enemy detection.
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectRadius);
    }
}
