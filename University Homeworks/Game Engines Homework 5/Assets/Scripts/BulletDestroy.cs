﻿using UnityEngine;
using System.Collections;

// Destroys the bullet on collision with an object. If the object is a specific one, interacts with it.
public class BulletDestroy : MonoBehaviour {

    public bool isPlayerBullet;

    // Checks for collisions.
    public void OnCollisionEnter(Collision collision)
    {
        // Checks if the bullet has hit an Enemy.
        if(isPlayerBullet)
        {
            if (collision.gameObject.CompareTag("Enemy"))
            {
                collision.gameObject.GetComponent<EnemyBehaviour>().TakeDamage(1);
            }
        }
        else
        {
            if(collision.gameObject.CompareTag("Player"))
            {
                GameManager.gameManager.PlayerTakeDamage(1);
            }
        }

        Destroy(gameObject);
    }
}
