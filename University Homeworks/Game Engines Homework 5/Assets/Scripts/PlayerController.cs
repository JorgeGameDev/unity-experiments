﻿using UnityEngine;
using System.Collections;

// Manages Player and Camera Movement.
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {

    [Header("Player Properties")]
    public float speed;
    public float rotSpeed;
    public Transform groundPoint;
    public Transform lookPoint;

    // Internal Components
    private bool _isJumping;
    private Camera _mainCamera;
    private Rigidbody _rigidbody;
    private Vector3 _cameraDiference;

    [Header("Bullet Spawn")]
    public GameObject bulletGO;
    public Transform bulletSpawn;

    // Internal Components
    public float bulletCooldown;
    private WaitForSeconds _bulletWait;
    private bool _isCoolDown;
    private AudioSource _audioSource;

    // Use this for initialization
    void Start ()
    {
        // Gets the necessary components.
        _mainCamera = Camera.main;
        _audioSource = GetComponent<AudioSource>();
        _rigidbody = GetComponent<Rigidbody>();
        _cameraDiference = _mainCamera.transform.position - transform.position;

        // Creates the timer for the bullet cooldown.
        _bulletWait = new WaitForSeconds(bulletCooldown);
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if(!GameManager.gameManager.isComplete)
        {
            PlayerMovement();
            PlayerFireInput();
        }
        else
        {
            _rigidbody.velocity = Vector3.zero;
        }
    }

    // Used to update the player position.
    void PlayerMovement()
    {
        // Updates the player velocity and camera's position depending on the player's input.
        float horInput = Input.GetAxis("Horizontal");
        float verInput = Input.GetAxis("Vertical");

        // Rotates the player with the new velocity.
        bool isGrounded = Physics.Raycast(groundPoint.position, -transform.up, 0.45f);

        // Checks if the player has pressed the jump key.
        if (isGrounded)
        {
            _isJumping = false;
            if (Input.GetButtonDown("Jump") && !_isJumping)
            {
                _isJumping = true;
                _rigidbody.AddForce(Vector3.up * 200);
            }
        }
        else if(!isGrounded)
        {
            _isJumping = true;
        }

        // Adds the movement.
        transform.Rotate(new Vector3(0, horInput * rotSpeed, 0));
        Vector3 velocity = transform.forward * verInput * speed;
        velocity.y = _rigidbody.velocity.y;
        _rigidbody.velocity = velocity; 

        // Updates the camera position.
        _cameraDiference = Quaternion.AngleAxis(horInput * rotSpeed, Vector3.up) * _cameraDiference;
        UpdateCameraPosition();
    }

    // Used to update camera position.
    void UpdateCameraPosition()
    {
        Vector3 newCameraPos = transform.position + _cameraDiference;
        _mainCamera.transform.position = Vector3.MoveTowards(_mainCamera.transform.position, newCameraPos, speed * 2 * Time.deltaTime);

        // And Rotation!
        _mainCamera.transform.LookAt(lookPoint);
    }

    // Fires a bullet whenever the player asks for an input.
    void PlayerFireInput()
    {
        if (Input.GetButtonDown("Fire1") && !_isCoolDown)
        {
            StartCoroutine(FireBullet());
        }
    }

    // Times the bullet cool down.
    IEnumerator FireBullet()
    {
        // Announces that the damage is in cooldown.
        _isCoolDown = true;

        // Applies the damage and velocity to the bullet.
        GameObject newBullet = (GameObject)Instantiate(bulletGO, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
        newBullet.GetComponent<Rigidbody>().velocity = newBullet.transform.forward * 5;
        newBullet.GetComponent<BulletDestroy>().isPlayerBullet = true;
        Destroy(newBullet, 1.5f);

        // Plays Gun Sound and starts timer.
        _audioSource.Play();
        yield return _bulletWait;

        // Stops the cooldown.
        _isCoolDown = false;
    }
}
