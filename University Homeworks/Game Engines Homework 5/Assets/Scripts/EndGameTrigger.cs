﻿using UnityEngine;
using System.Collections;

// Used for triggering the game end case the player has all the resources needed.
public class EndGameTrigger : MonoBehaviour
{
    // Checks if the player has entered the collision.
    public void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.CompareTag("Player"))
        {
            // Checks the number of cubits.
            if(GameManager.gameManager.cubitsCollected >= 40 && GameManager.gameManager.cubitsCollected < 50)
            {
                GameManager.gameManager.ShowEndGameCanvas("You have collected all the necessary Cubits!\n But can you do even better and collect them all?");
            }
            else if(GameManager.gameManager.cubitsCollected == 50)
            {
                GameManager.gameManager.ShowEndGameCanvas("You have collected all the Cubits in this world!\n Thanks for playing!");
            }
        }
    }
}
