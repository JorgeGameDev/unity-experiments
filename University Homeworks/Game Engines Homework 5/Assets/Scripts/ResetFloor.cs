﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

// Checks if the player has fallen under a certain cordinate which resets the scene.
public class ResetFloor : MonoBehaviour
{

    [Header("Kill Cordinate")]
    public float killY;

    [Header("Checkpoint")]
    public GameObject lastCheckPoint;

    // Update is called once per frame
    void Update()
    {
        // Checsk and reloads the scene if necessary.
        if (transform.position.y < killY)
        {
            GameManager.gameManager.PlayerTakeDamage(1);
            if (GameManager.gameManager.playerHealth > 0)
            {
                transform.position = lastCheckPoint.transform.position;
            }
        }
    }

    // Checks if there's any collision with a checkpoint.
    public void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Checkpoint") && collider.gameObject != lastCheckPoint)
        {
            lastCheckPoint = collider.gameObject;
        }
    }
}
