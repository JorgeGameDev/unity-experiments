﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

// Used for managing the game state and game variables.
public class GameManager : MonoBehaviour {

    public static GameManager gameManager;

    [Header("Damage Control")]
    public GameObject enemyObject;
    public AudioClip playerDamageClip;
    public AudioClip enemyDamageClip;

    [Header("Player Health")]
    public int playerHealth;
    public GameObject healthImage;
    public RectTransform healthTransform;
    public Animator damageAnimator;

    [Header("UI Score")]
    public Text collectedText;
    public AudioClip collectedClip;
    public int cubitsCollected;

    [Header("End Game")]
    public GameObject completeCanvas;
    public Text endingMessage;
    public bool isComplete;

    // Internal
    private AudioSource _audioSource;
    private Transform[] _enemySpawns;
    private WaitForSeconds _enemyTimer;
    private GameObject[] _healthImages;
    private bool _isCoolDown;

    // Use this for initialization
    void Start()
    {
        // Sets up the necessary references and gameobjects for the enemy spawning.
        gameManager = this;
        _audioSource = GetComponent<AudioSource>();

        // Creates the health UI.
        _healthImages = new GameObject[playerHealth];
        for(int i = 0; i < _healthImages.Length; i++)
        {
            GameObject newHealthUI = Instantiate(healthImage);
            newHealthUI.transform.SetParent(healthTransform);
            newHealthUI.GetComponent<RectTransform>().anchoredPosition = new Vector2(i * 22 + 11.5f, -20);
            newHealthUI.transform.localScale = Vector3.one;
            _healthImages[i] = newHealthUI;
        }
    }

    // Update is called once per frame
    public void Update()
    {
        // Checks if the game is complete and if the player can restart.
        if(isComplete)
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    // Applies damage to the player.
    public void PlayerTakeDamage(int damage)
    {
        damageAnimator.SetTrigger("TriggerDamage");
        // Applies damage.
        _audioSource.PlayOneShot(playerDamageClip);
        playerHealth -= damage;
        Destroy(_healthImages[playerHealth]);

        // Checks if the enemy has gone under the limit health.
        if (playerHealth <= 0)
        {
            TriggerGameOver();
            return;
        }
    }

    // Triggers Game Over
    public void TriggerGameOver()
    {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        Destroy(playerObject);
        ShowEndGameCanvas("You were unable to keep going. Will you try again and attempt to collect the cubits you need?");
    }

    // Triggers the end game canvas to appear.
    public void ShowEndGameCanvas(string canvasText)
    {
        isComplete = true;
        completeCanvas.SetActive(true);
        endingMessage.text = canvasText;
    }

    // Plays the enemy kill clip.
    public void EnemyDamageClip()
    {
        _audioSource.PlayOneShot(enemyDamageClip);
    }

    // Adds a point to the collectibles counter.
    public void AddCollectible()
    {
        cubitsCollected++;
        collectedText.text = "x" + cubitsCollected;
        _audioSource.PlayOneShot(collectedClip);
    }
}
