﻿using UnityEngine;
using System.Collections;

// Used for collecting items. In this project in specific: Cubits which are needed to finish the level.
public class Collectible : MonoBehaviour
{

    [Header("Collision LayerMask")]
    public float checkRadius;
    private GameObject _playerObject;

    // Use this for initialization
    void Start()
    {
        // Gets the necessary external components.
        _playerObject = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        CollectibleMoveTowards();
    }

    // Makes the collectible move towards the player if the player is inside a radius.
    void CollectibleMoveTowards()
    {
        // Checks if the player in a near distance of the collectible.
        if(_playerObject != null)
        {
            if (Vector3.Distance(transform.position, _playerObject.transform.position) < checkRadius)
            {
                transform.position = Vector3.MoveTowards(transform.position, _playerObject.transform.position, 2 * Time.deltaTime);
            }
        }
    }

    // Checks if the player has gone into the cubit trigger.
    public void OnTriggerEnter(Collider collider)
    {
        if(collider.CompareTag("Player"))
        {
            // Tells the Game Manager to add a point.
            GameManager.gameManager.AddCollectible();
            Destroy(gameObject);
        }
    }

    // For debugging, draw gizmos on selection.
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, checkRadius);
    }
}
