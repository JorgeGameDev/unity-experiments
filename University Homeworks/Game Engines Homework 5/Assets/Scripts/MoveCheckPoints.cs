﻿using UnityEngine;
using System.Collections.Generic;

// Moves an object between multiple check points.
public class MoveCheckPoints : MonoBehaviour {

    [Header("Checkpoints")]
    public List<Transform> transformPositions = new List<Transform>();

    [Header("Platform Speeds")]
    public float speed;

    // Internal Components
    private int _currentTransform;
    private Vector3 _currentTransformPosition;

	// Use this for initialization
	void Start () {
        // Assigns the current position.
        _currentTransformPosition = transformPositions[_currentTransform].position;

    }
	
	// Update is called once per frame
	void Update () {
        // Moves the object towards the next point.
        transform.position = Vector3.MoveTowards(transform.position, _currentTransformPosition, speed * Time.deltaTime);

        // Checks if it's already there.
        if(transform.position == _currentTransformPosition)
        {
            if(_currentTransform < transformPositions.Count - 1)
            {
                _currentTransform++;
            }
            else
            {
                _currentTransform = 0;
            }

            _currentTransformPosition = transformPositions[_currentTransform].position;
        }
	}
}
