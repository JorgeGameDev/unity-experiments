﻿using UnityEngine;
using System.Collections;

// Seeks and calculates the velocity to a given target.
public class SteeringSeek : SteeringBehaviour {

    [Header("Seeking Properties")]
    public Transform target;

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        return helper.Seek(vehicle, target.position);
    }
}
