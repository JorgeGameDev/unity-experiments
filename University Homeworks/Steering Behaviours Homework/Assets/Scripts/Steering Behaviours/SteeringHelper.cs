﻿using UnityEngine;
using System.Collections;

// Helper class with functions shared across multiple behavior.
public class SteeringHelper
{
    // Seek behavior that calculates the speed to a given target.
    public Vector3 Seek(SteeringVehicle vehicle, Vector3 target)
    {
        // Calculates the desired force to the target.
        Vector3 toTarget = target - vehicle.transform.position;
        Vector3 neededForce = toTarget.normalized * vehicle.maxSpeed;
        return neededForce - vehicle.velocity;
    }

    // Flee behavior that calculates a speed to avoid an enemy.
    public Vector3 Flee(SteeringVehicle vehicle, Vector3 target, float panicDistance)
    {
        // Checks if the vehicle is a safe distance away.
        if (Vector3.Distance(vehicle.transform.position, target) < panicDistance)
        {
            // Calculates the desired force to the target.
            Vector3 fleeFrom = vehicle.transform.position - target;
            Vector3 neededForce = fleeFrom.normalized * vehicle.maxSpeed;
            return neededForce - vehicle.velocity;
        }
        else
        {
            return Vector3.zero;
        }
    }
}
