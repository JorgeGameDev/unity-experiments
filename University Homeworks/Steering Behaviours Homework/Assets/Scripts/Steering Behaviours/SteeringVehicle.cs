﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Base class for a Steering Vehicle.
public class SteeringVehicle : MonoBehaviour
{
    [Header("Behaviors")]
    public List<SteeringBehaviour> steeringBehaviours;

    [Header("Steering Properties")]
    public Vector3 direction;
    public float maxForces;
    public float maxSpeed;
    public float decelaration;
    public float mass
    {
        get // Returns the Mass of the Rigidbody.
        {
            return _rigidbody.mass;
        }
        set // Sets the mass of the rigidbody to be the value parsed.
        {
            _rigidbody.mass = value;
        }
    }
    public Vector3 velocity
    {
        get
        {
            return _rigidbody.velocity;
        }
    }

    // Components
    private Rigidbody _rigidbody;

    // Called before scene render.
    public void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    public void Start()
    {
        direction = transform.forward;
    }

    // Update is called once per frame.
    void FixedUpdate()
    {
        // Calculates and accumulates all the forces that exist on our behaviors.
        Vector3 forces = new Vector3();

        // Does calculations for every steering behavior.
        foreach (SteeringBehaviour behaviour in steeringBehaviours)
        {
            forces += behaviour.Calculate(this);
        }

        // Clamps the forces to their limit.
        forces = Vector3.ClampMagnitude(forces, maxForces);

        // Adds every single force to the rigidbody.
        _rigidbody.AddForce(forces);

        // Stores variables related to rigidbody information.
        float minimumDistance = 0.1f;
        float currentSpeed = _rigidbody.velocity.magnitude;

        // Stores the velocity destination given a minimum threshold.
        if (currentSpeed > minimumDistance)
        {
            direction = _rigidbody.velocity.normalized;
        }

        // Same thing to the velocity.
        _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, maxSpeed);

        // Clamps the position of the car to the bounds of the map.
        Vector3 transformPos = transform.position;
        transformPos.x = Mathf.Clamp(transformPos.x, -25, 25);
        transformPos.z = Mathf.Clamp(transformPos.z, -25, 25);
        transform.position = transformPos;
    }

    // Used for clearing the Steering Behaviours from the list.
    public void RemoveBehaviours()
    {
        steeringBehaviours.Clear();
    }

    // Used for resseting the velocity of the rigidbody.
    public void ResetVelocity()
    {
        _rigidbody.velocity = Vector3.zero;
    }

    // Converts a local position to the world position.
    public Vector3 LocalToWorld(Vector3 local)
    {
        // Constructs a matrix with the transformations of the vehicle.
        Vector3 position = transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        Vector3 scale = Vector3.one;
        Matrix4x4 matrix = Matrix4x4.TRS(position, rotation, scale);

        // Returns the multiplied matrix.
        return matrix.MultiplyPoint(local);
    }
}
