﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Manages the games situation, as well as resets the game state every time the player wants.
public class GameManager : MonoBehaviour {

    // Singleton.
    public static GameManager gameManager;

    [Header("Game Status")]
    public bool coreCollector;

    [Header("Player Objects")]
    public GameObject coreHolder;
    public GameObject coreObject;
    public GameObject alienPlayer;
    public GameObject workerPlayer;

    [Header("Arena Limits")]
    public Vector2 minLimits;
    public Vector2 maxLimits;

    [Header("User Interface")]
    public Text holderText;
    public Text workerClaims;
    public Text alienClaims;

    // Internal
    private PlayerBehaviour _alienBehaviour;
    private PlayerBehaviour _workerBehaviour;
    private int _workerClaims;
    private int _alienClaims;

    // Use this for early initialization
    void Awake()
    {
        // Makes this the single existing Game Manager.
        if(gameManager == null)
        {
            gameManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameManager);
        }
    }

    // Use this for initialization
    void Start ()
    {
        // Gets the behaviours from the objects.
        _alienBehaviour = alienPlayer.GetComponent<PlayerBehaviour>();
        _workerBehaviour = workerPlayer.GetComponent<PlayerBehaviour>();

        // Re[Starts] the game.
        RestartGame();
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Check if the player wants a restart.
	    if(Input.GetButtonDown("Restart"))
        {
            // Reloads the scenes and restarts the game.
            RestartGame();
        }
	}

    void LateUpdate()
    {
        // Checks if someone is holding the tool.
        if (coreHolder != null)
        {
            Vector3 toolPosition = coreHolder.transform.position;
            toolPosition.y = 2f;
            coreObject.transform.position = toolPosition;
        }
    }

    // Generates random positions for the player characters.
    void RestartGame ()
    {
        // Generates a random position for the alien.
        Vector3 randomPosition = new Vector3(Random.Range(minLimits.x, maxLimits.x), 0.35f, Random.Range(minLimits.y, maxLimits.y));
        alienPlayer.transform.position = randomPosition;

        // Gives a random position for the worker.
        randomPosition = new Vector3(Random.Range(minLimits.x, maxLimits.x), 0.35f, Random.Range(minLimits.y, maxLimits.y));
        workerPlayer.transform.position = randomPosition;

        // Gives a random position for the tool.
        coreHolder = null;
        randomPosition = new Vector3(Random.Range(minLimits.x, maxLimits.x), 0.35f, Random.Range(minLimits.y, maxLimits.y));
        coreObject.transform.position = randomPosition;

        // Sets everything as the default.
        coreCollector = false;
        holderText.text = "The Core hasn't been claimed yet!";
        _alienBehaviour.ClearBehaviours();
        _workerBehaviour.ClearBehaviours();
    }

    // Used for registing that the tool has been collected.
    public void ToolCollected(GameObject collector)
    {
        // Gets the behaviours from the objects.
        coreCollector = true;

        // Adds the behaviours to the respective objects.
        if (collector == alienPlayer)
        {
            _alienClaims++;
            coreHolder = alienPlayer;
            _alienBehaviour.RunTool(workerPlayer);
            _workerBehaviour.ChaseTool(alienPlayer);

            // Updates the UI text to reflect who caught the Core.
            holderText.text = "The <color=cyan>Alien</color> is holding the Core!";
            alienClaims.text = _alienClaims.ToString();
        }
        else
        {
            _workerClaims++;
            coreHolder = workerPlayer;
            _workerBehaviour.RunTool(alienPlayer);
            _alienBehaviour.ChaseTool(workerPlayer);

            // Updates the UI text to reflect who caught the Core.
            holderText.text = "The <color=yellow>Worker</color> is holding the Core!";
            workerClaims.text = _workerClaims.ToString();
        }
    }
}
