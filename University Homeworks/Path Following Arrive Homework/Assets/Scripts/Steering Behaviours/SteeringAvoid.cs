﻿using UnityEngine;
using System.Collections;

// Detects for obstacles to avoid through raycast and applies forces to move away from it.
public class SteeringAvoid : SteeringBehaviour
{
    [Header("Obstacle Definition")]
    public LayerMask obstaclesLayer;
    public float avoidDistance;
    public float avoidForce;

    // Internal
    private SteeringVehicle _thisVehicle;

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        // Gets the vehicle information.
        _thisVehicle = vehicle;
        Vector3 position = vehicle.transform.position;
        Vector3 direction = vehicle.direction;

        // Does the Raycast in order to check for obstacles.
        RaycastHit rayHit;
        if (Physics.Raycast(position, direction, out rayHit, avoidDistance, obstaclesLayer))
        {
            // Calculates our distance from the hit point, too see when to start applying the force to avoid the obstacle.
            Vector3 hitPoint = rayHit.point;
            Vector3 toHit = hitPoint - position;
            float normalize = toHit.magnitude / avoidDistance;
            float lerpAmmount = 1f - normalize;
            float forceAmmount = Mathf.Lerp(0, avoidForce, lerpAmmount);

            // Calculates the final repulsion force.
            Vector3 repulsion = hitPoint.normalized * forceAmmount;
            return repulsion - vehicle.velocity;
        }

        // Returns nada.
        return Vector3.zero;
    }

    // Draws debug gizmos for helping.
    public void OnDrawGizmosSelected()
    {
        if(_thisVehicle == null)
        {
            return;
        }

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + _thisVehicle.direction * avoidDistance);
        Gizmos.DrawSphere(transform.position + _thisVehicle.direction * avoidDistance, 0.5f);
    }
}
