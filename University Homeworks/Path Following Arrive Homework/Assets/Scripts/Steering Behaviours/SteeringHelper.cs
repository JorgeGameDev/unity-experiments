﻿using UnityEngine;
using System.Collections;

// Helper class with functions shared across multiple behavior.
public class SteeringHelper
{
    // Seek behavior that calculates the speed to a given target.
    public Vector3 Seek(SteeringVehicle vehicle, Vector3 target)
    {
        // Calculates the desired force to the target.
        Vector3 toTarget = target - vehicle.transform.position;
        Vector3 neededForce = toTarget.normalized * vehicle.maxSpeed;
        return neededForce - vehicle.velocity;
    }

    // Flee behavior that calculates a speed to avoid an enemy.
    public Vector3 Flee(SteeringVehicle vehicle, Vector3 target, float panicDistance)
    {
        // Checks if the vehicle is a safe distance away.
        if (Vector3.Distance(vehicle.transform.position, target) < panicDistance)
        {
            // Calculates the desired force to the target.
            Vector3 fleeFrom = vehicle.transform.position - target;
            Vector3 neededForce = fleeFrom.normalized * vehicle.maxSpeed;
            return neededForce - vehicle.velocity;
        }
        else
        {
            return Vector3.zero;
        }
    }

    // Arrive Behaviour that calculated the speed to a given target and slows down near it.
    public Vector3 Arrive(SteeringVehicle vehicle, Vector3 target)
    {
        // Calculates the distance to vector.
        Vector3 toTarget = target - vehicle.transform.position;
        float distance = toTarget.magnitude;

        // Avoids the distance becoming zero, in order to avoid NaN errors.
        if (distance < Mathf.Epsilon)
        {
            return Vector3.zero;
        }
        else
        {
            // Calculates the speed necessary.
            float speed = Mathf.Min(vehicle.maxSpeed, distance / vehicle.decelaration);

            // Calculates the needed force.
            Vector3 neededForce = (toTarget / distance) * speed;
            return neededForce - vehicle.velocity;
        }
    }
}
