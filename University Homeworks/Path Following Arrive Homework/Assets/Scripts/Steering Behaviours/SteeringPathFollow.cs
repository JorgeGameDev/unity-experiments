﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Makes the vehicle follow a specific set of points.
public class SteeringPathFollow : SteeringBehaviour {

    [Header("Path")]
    public FollowingType followingType;
    public List<Transform> pathPoints;

    [Header("Following Properties")]
    public float arriveRadius;

    // Internal Variables
    private int _index;
    private bool _backTracking;

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        if(pathPoints.Count != 0)
        {
            // Checks the current index.
            Transform wayPoint = pathPoints[_index];

            // Checks if the vehicle is already close enough to the current path point.
            if (Vector3.Distance(vehicle.transform.position, wayPoint.transform.position) < arriveRadius)
            {
                // Stops on Arrival, when the vehicle reaches the end of the path.
                if (followingType == FollowingType.Stop)
                {
                    if (_index + 1 < pathPoints.Count)
                    {
                        _index++;
                    }
                }
                // Restarts the path when the vehicle reaches the final point.
                else if (followingType == FollowingType.LoopBack)
                {
                    if (_index + 1 < pathPoints.Count)
                    {
                        _index++;
                    }
                    else
                    {
                        _index = 0;
                    }
                }
                // Reverses the path when the vehicle reaches the final point.
                else if (followingType == FollowingType.Backtrack)
                {
                    // Are we backtracking?
                    if (!_backTracking)
                    {
                        if (_index + 1 < pathPoints.Count)
                        {
                            _index++;
                        }
                        else
                        {
                            _backTracking = true;
                            _index--;
                        }
                    }
                    else
                    {
                        if (_index > 0)
                        {
                            _index--;
                        }
                        else
                        {
                            _backTracking = false;
                            _index++;
                        }
                    }
                }
                // Destroys the waypoint when the vehicle reaches it.
                else if (followingType == FollowingType.DestroyWaypoint)
                {
                    pathPoints.Remove(wayPoint);
                    Destroy(wayPoint.gameObject);

                    // For Arrive Homework Only.
                    ArriveGameManager.arriveGameManager.ArrivedWaypoint();
                }
            }

            // Arrives at the direction of the point.
            return helper.Arrive(vehicle, wayPoint.position);
        }

        return Vector3.zero;
    }

    // Allows a waypoint to be added to the list during execution.
    public void AddWayPoint(Transform waypoint)
    {
        pathPoints.Add(waypoint);
    }

    // Destroys all the waypoints in the list.
    public void RemoveAllWayPoints()
    {
        // Destroys the actual objects.
        foreach(Transform tranform in pathPoints)
        {
            Destroy(tranform.gameObject);
        }

        // Clears the list.
        pathPoints.Clear();
    }
}

// Defines an enumerator of potential path types.
public enum FollowingType
{
    Stop,
    LoopBack,
    Backtrack,
    DestroyWaypoint,
}
