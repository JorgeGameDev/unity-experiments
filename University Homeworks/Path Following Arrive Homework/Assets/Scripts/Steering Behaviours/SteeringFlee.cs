﻿using UnityEngine;
using System.Collections;

// Flees away from a given target.
public class SteeringFlee : SteeringBehaviour
{
    [Header("Fleeing Properties")]
    public Transform fleePoint;
    public float panicDistance;

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        return helper.Flee(vehicle, fleePoint.transform.position, panicDistance);
    }
}