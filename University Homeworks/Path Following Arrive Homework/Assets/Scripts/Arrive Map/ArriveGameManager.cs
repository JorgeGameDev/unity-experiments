﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Defines the Game Manager of the Arrive Homework, where the player can place waypoints to define a Mail Man's navigation.
public class ArriveGameManager : MonoBehaviour {

    public static ArriveGameManager arriveGameManager;

    [Header("Required GameObjects")]
    public GameObject waypointGO;
    public Text trackerText;

    [Header("Raycast Definitions")]
    public LayerMask layerMask;

    [Header("Map Limits")]
    public Vector2 minLimits;
    public Vector2 maxLimits;

    // Camera used for raycasting waypoints.
    private Camera _camera;

    // Player Related Objects and Scripts.
    private GameObject _mailMan;
    private SteeringPathFollow _steeringPath;
    private Vector3 _mailManStart;

    // Tracker of Waypoints arrived compared to the ones that have been already placed.
    private int _wayPointsPlaced;
    private int _wayPointsArrived;

	// Use this for initialization
	void Start ()
    {
        if(arriveGameManager == null)
        {
            arriveGameManager = this;
        }
        else
        {
            Destroy(gameObject);
        }

        // Gets necessary components.
        _camera = Camera.main;
        _mailMan = GameObject.FindGameObjectWithTag("Player");
        _steeringPath = _mailMan.GetComponent<SteeringPathFollow>();
        _mailManStart = _mailMan.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Prepares a ray from the Camera based on the Mouse Position.
        Ray cameraRay = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit outHit;

        // Does a raycast to check where to place waypoints at.
        if(Physics.Raycast(cameraRay, out outHit, Camera.main.farClipPlane, layerMask))
        {
            // Checks if the player wants to place a cursor here.
            if (Input.GetButtonDown("Fire1"))
            {
                PlaceWaypoint(outHit);
            }
        }

        // Checks if the player wants to remove all the waypoints and reset the mailman position.
        if (Input.GetButtonDown("Fire2"))
        {
            _steeringPath.RemoveAllWayPoints();
            _mailMan.transform.position = _mailManStart;
        }

        // Clamps the Mail Man Position to the map.
        Vector3 mailManPosition = _mailMan.transform.position;
        mailManPosition.x = Mathf.Clamp(mailManPosition.x, minLimits.x, maxLimits.x);
        mailManPosition.z = Mathf.Clamp(mailManPosition.z, minLimits.y, maxLimits.y);
        _mailMan.transform.position = mailManPosition;
    }

    // Places the actual waypoint at the position the player requested for.
    void PlaceWaypoint(RaycastHit outHit)
    {
        // Gets the position of the point and makes sure the isn't colliding with the plane.
        Vector3 hitPoint = outHit.point;
        hitPoint.y += 0.02f;

        // Creates the waypoint.
        GameObject newWayPoint = (GameObject)Instantiate(waypointGO, hitPoint, Quaternion.identity);
        _steeringPath.AddWayPoint(newWayPoint.transform);
        _wayPointsPlaced++;
        UpdateUIText();
    }

    // Receives Feedback from the Arrive Behaviour announcing that the player has arrived to the waypoint.
    public void ArrivedWaypoint()
    {
        _wayPointsArrived++;
        UpdateUIText();
    }

    // Updates the UI with the new values of waypoints.
    void UpdateUIText()
    {
        // Updates the UI Text.
        trackerText.text = _wayPointsArrived + " Out Of " + _wayPointsPlaced;
    }
}
