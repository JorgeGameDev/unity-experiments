﻿using UnityEngine;
using System.Collections;

// Follows the MailMan on the scene, and feeds it into the renderer texture.
public class CameraFollow : MonoBehaviour {

    // Internal
    private GameObject _mailMan;
    private Vector3 _posDiference;

	// Use this for initialization
	void Start () {
        _mailMan = GameObject.FindGameObjectWithTag("Player");
        _posDiference = transform.position - _mailMan.transform.position;
	}
	
	// Late Update is called at the end of a frame
	void LateUpdate () {
        // Gets the mailman position and updates the camera position acordingly.
        Vector3 position = _mailMan.transform.position;
        position += _posDiference;
        transform.position = position;
	}
}
