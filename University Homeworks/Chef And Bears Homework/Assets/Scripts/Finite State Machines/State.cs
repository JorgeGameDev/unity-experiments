﻿using UnityEngine;
using System.Collections;

// State that defines an action or state of a FSM.
public abstract class State {

    // Agent Object which owns a FSM with this state.
    public GameObject agent;
    public FiniteStateMachine parentFSM;

    // Defines the multiple functions that the FSM-State can use.

    // Events that happen when the State is loaded fort he first time.
    public virtual void OnLoad() { }

    // Events that happen when the State is running and being called on Update().
    public virtual void UpdateState() { }

    // Events that happen when the State is started.
    public virtual void OnEnterState() { }

    // Events that happen when the State is stopped.
    public virtual void OnExitState() { }
}
