﻿using UnityEngine;
using System.Collections;

public class SleepAgent : MonoBehaviour {

    // Internal for Artificial Inteligence Logic.
    private FiniteStateMachine _fsm;

	// Use this for initialization
	void Start () {
        // Gets the FSM and loads the first state on to it.
        _fsm = GetComponent<FiniteStateMachine>();
        _fsm.LoadState<AgentSleepState>();
        _fsm.ActivateState<AgentSleepState>();
	}
}
