﻿using UnityEngine;
using System.Collections;

// State where the Wandering Behaviour is applied to the Chef.
public class HuntingState : State {

    // Internal Variables.
    private SteeringVehicle _vehicle;
    private ChefBehaviour _chefBehaviour;

    // Events that happen when the State is started.
    public override void OnEnterState()
    {
        // Gets the vehicle component and adds the wandering behaviour to it.
        _chefBehaviour = agent.GetComponent<ChefBehaviour>();
        _vehicle = agent.GetComponent<SteeringVehicle>();
        SteeringWander wander = agent.gameObject.AddComponent<SteeringWander>();

        // Sets up the variables on the wander behaviour.
        wander.distance = 12f;
        wander.radius = 6f;
        wander.jitter = 0.4f;

        // Adds the behaviour to the list.
        _vehicle.steeringBehaviours.Insert(0, wander);
    }

    // Events that happen when the State is running and being called on Update().
    public override void UpdateState()
    {
        // Checks if there is any bears around.
        RaycastHit[] hit = Physics.SphereCastAll(agent.transform.position, 2f, Vector3.right, 2f, _chefBehaviour.objectMask.value);
        Debug.Log(hit.Length);

        // If there are, change state, set it as the seeker.
        if(hit.Length > 0)
        {
            Debug.Log(hit[0].collider.name);
            Debug.Log("Found Bear!");
            foreach(RaycastHit hitObject in hit)
            {
                if(hitObject.collider.gameObject.CompareTag("Bear"))
                {
                    _chefBehaviour.FoundBear(hitObject.collider.gameObject);
                    return;
                }
            }
        }
    }

    // Events that happen when the State is stopped.
    public override void OnExitState()
    {
        // Removes the component.
        _vehicle.steeringBehaviours.RemoveAt(0);
        GameObject.Destroy(agent.GetComponent<SteeringWander>());
    }
}
