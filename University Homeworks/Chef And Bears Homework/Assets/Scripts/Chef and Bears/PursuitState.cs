﻿using UnityEngine;
using System.Collections;

// State where the Wandering Behaviour is applied to the Chef.
public class PursuitState : State
{
    // Internal Variables.
    private SteeringVehicle _vehicle;
    private SteeringPursuit _pursuit;
    private ChefBehaviour _chefBehaviour;
    private GameObject _bearTargeted;

    // Events that happen when the State is started.
    public override void OnEnterState()
    {
        // Gets the vehicle component.
        _chefBehaviour = agent.GetComponent<ChefBehaviour>();
        _vehicle = agent.GetComponent<SteeringVehicle>();
        _bearTargeted = _chefBehaviour.bearTargeted;

        // Turns on the target object.
        _chefBehaviour.targetObject.SetActive(true);

        // Adds the pursuit behaviour.
        _pursuit = agent.AddComponent<SteeringPursuit>();
        _vehicle.steeringBehaviours.Insert(0, _pursuit);

        // Pursuits the bear.
        _pursuit.pursuitVehicle = _chefBehaviour.bearTargeted.GetComponent<SteeringVehicle>();
    }

    // Events that happen when the State is running and being called on Update().
    public override void UpdateState()
    {
        // Checks if the chef is near enough to hunt the bear.
        if (Vector3.Distance(_bearTargeted.transform.position, agent.transform.position) < _chefBehaviour.huntDistance)
        {
            // Confirms the bear as hunted.
            _chefBehaviour.BearHunted();
        }
        else if(Vector3.Distance(_bearTargeted.transform.position, agent.transform.position) > _bearTargeted.GetComponent<BearBehaviour>().panicDistance * 2)
        {
            // Start Wandering Again
            _chefBehaviour.MissedBear();
        }

        // Sets the position to the bear being hunted.
        Vector3 bearPosition = _bearTargeted.transform.position;
        bearPosition.y += 0.1f;
        _chefBehaviour.targetObject.transform.position = bearPosition;
    }

    // Events that happen when the State is stopped.
    public override void OnExitState()
    {
        // Removes the component.
        _chefBehaviour.targetObject.SetActive(false);
        _vehicle.steeringBehaviours.RemoveAt(0);
        GameObject.Destroy(agent.GetComponent<SteeringPursuit>());
    }
}
