﻿using UnityEngine;
using System.Collections;

// Starting State, which simply depletes the meat counter that the chef has.
public class CookingState : State {

    // Internal Variables.
    private ChefBehaviour _chefBehaviour;

    // Events that happen when the State is started.
    public override void OnEnterState()
    {
        // Gets the chef behaviour.
        _chefBehaviour = agent.GetComponent<ChefBehaviour>();
    }

    // Events that happen when the State is running and being called on Update().
    public override void UpdateState()
    {
        // Asks for meat to be depleted.
        _chefBehaviour.CheckMeat();
    }
}
