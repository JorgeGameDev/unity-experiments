﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Manages the current game properties, such as the meat left or the bears hunted.
public class ChefGameManager : MonoBehaviour {

    // Singleton Reference
    public static ChefGameManager chefGameManager;

    [Header("Game Elements")]
    public GameObject chef;
    public float timePerMeat;
    public int meatRemaining;
    public int bearsHunted;

    [Header("Map Limits")]
    public Vector2 limitX;
    public Vector2 limitZ;

    [Header("Interface Elements")]
    public Text meatRemainingText;
    public Text bearsHuntedText;

    [Header("Bear Spawning")]
    public LayerMask placementMask;
    public GameObject bearObject;

    // Use this for early initialization
    void Awake()
    {
        // Makes this Game Manager instance the singleton.
        if(chefGameManager == null)
        {
            chefGameManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        // Sets up UI!
        meatRemainingText.text = meatRemaining.ToString();
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Checks if the user wants to spawn a new Bear.
	    if(Input.GetButtonDown("Fire1"))
        {
            // Does a raycast to see if a bear can be spawned here.
            RaycastHit hit;
            Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(cameraRay, out hit, Camera.main.farClipPlane, placementMask))
            {
                // Generates a Random Rotation for spawning bears to be more pleasing to the eye. Yes, that's a thing.
                float randomRot = Random.Range(0, 360);
                Vector3 rotationVector = new Vector3(0, randomRot, 0);
                Quaternion rotation = Quaternion.Euler(rotationVector);

                // Instantiates the bear.
                Instantiate(bearObject, hit.point, rotation);
            }
        }
	}

    // Depletes a meat piece.
    public void DepleatMeat()
    {
        // Removes meat from the counter and updates the user interface.
        meatRemaining--;
        meatRemainingText.text = meatRemaining.ToString();
    }

    // Increases one bear hunted.
    public void BearHunted()
    {
        // Adds a bear to the hunted count and updates the user interface.
        bearsHunted++;
        bearsHuntedText.text = bearsHunted.ToString();
        meatRemaining += 12;
        meatRemainingText.text = meatRemaining.ToString();
    }
}
