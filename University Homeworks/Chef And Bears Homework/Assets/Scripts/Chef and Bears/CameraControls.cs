﻿using UnityEngine;
using System.Collections;

// Manages the Camera Controls, allowing the player to freely move the camera to their discretion.
public class CameraControls : MonoBehaviour {

    [Header("Camera Properties")]
    public float cameraSpeed;

    [Header("Camera Limits")]
    public Vector2 limitX;
    public Vector2 limitZ;

	// Update is called once per frame
	void Update ()
    {
        // Gets the axis of the camera.
        float horAxis = Input.GetAxis("Horizontal");
        float verAxis = Input.GetAxis("Vertical");

        // Moves the camera in the axis the player inputed.
        Vector3 translation = new Vector3(-verAxis, 0, horAxis) * cameraSpeed;
        transform.Translate(translation * Time.deltaTime, Space.World);

        // Clamps the camera to a limited movement zone.
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, limitX.x, limitX.y);
        pos.z = Mathf.Clamp(pos.z, limitZ.x, limitZ.y);
        transform.position = pos;
	}
}
