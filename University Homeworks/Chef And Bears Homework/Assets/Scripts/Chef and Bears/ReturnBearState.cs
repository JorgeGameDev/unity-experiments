﻿using UnityEngine;
using System.Collections;

// Used for the chef to return to its original position.
public class ReturnBearState : State {

    // Internal Variables.
    private SteeringVehicle _vehicle;
    private BearBehaviour _bearBehaviour;

    // Events that happen when the State is started.
    public override void OnEnterState()
    {
        // Gets the vehicle component.
        _bearBehaviour = agent.GetComponent<BearBehaviour>();
        _vehicle = agent.GetComponent<SteeringVehicle>();

        // Adds the pursuit behaviour.
        SteeringSeek seek = agent.AddComponent<SteeringSeek>();
        _vehicle.steeringBehaviours.Insert(0, seek);

        // Seeks back to the origin position.
        seek.target = _bearBehaviour.startPos.transform;
    }

    // Events that happen when the State is running and being called on Update().
    public override void UpdateState()
    {
        // Checks if the chef is near enough to hunt the bear.
        if (Vector3.Distance(_bearBehaviour.startPos.transform.position, agent.transform.position) < 1f)
        {
            // Confirms that the bear has returned.
            _bearBehaviour.ReturnedToGrass();
        }
    }

    // Events that happen when the State is stopped.
    public override void OnExitState()
    {
        // Removes the component.
        _vehicle.steeringBehaviours.RemoveAt(0);
        GameObject.Destroy(agent.GetComponent<SteeringSeek>());
    }
}
