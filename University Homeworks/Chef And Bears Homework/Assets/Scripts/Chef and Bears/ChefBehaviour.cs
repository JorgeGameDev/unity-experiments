﻿using UnityEngine;
using System.Collections;

// Manages the swapping out and behaviour conditions of the Chef.
public class ChefBehaviour : MonoBehaviour {

    [Header("Masking Properties")]
    public LayerMask objectMask;

    [Header("Behaviour References")]
    [ReadOnly]
    public GameObject startPos;
    public float huntDistance;
    public GameObject bearTargeted;

    [Header("Polish Objects")]
    public ParticleSystem grillParticles;
    public GameObject targetObject;
    public GameObject meat;

    // Internal References
    private FiniteStateMachine fsm;
    private bool _isDepleating;

    // Use this for initialization
    void Start ()
    {
        // Stores the starting position.
        startPos = new GameObject();
        startPos.name = "Chef Start";
        startPos.transform.position = transform.position;

        // Gets the FSM.
        fsm = GetComponent<FiniteStateMachine>();

        // Loads all the states that are necessary for the chef.
        fsm.LoadState<CookingState>();
        fsm.LoadState<HuntingState>();
        fsm.LoadState<PursuitState>();
        fsm.LoadState<ReturnChefState>();

        // Activates the first, cooking state.
        fsm.ActivateState<CookingState>();
	}

    // Update is called every frame
    void Update()
    {
        // Clamps the movement to the game zone.
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, ChefGameManager.chefGameManager.limitX.x, ChefGameManager.chefGameManager.limitX.y);
        pos.z = Mathf.Clamp(pos.z, ChefGameManager.chefGameManager.limitZ.x, ChefGameManager.chefGameManager.limitZ.y);
        transform.position = pos;
        transform.rotation = Quaternion.LookRotation(GetComponent<SteeringVehicle>().direction);

        // Checks if a Reset was requested.
        if(Input.GetButtonDown("Restart"))
        {
            transform.position = startPos.transform.position;
        }
    }

    // Used for depleting meet over time.
    public void CheckMeat()
    {
        if(ChefGameManager.chefGameManager.meatRemaining > 0)
        {
            if(!_isDepleating)
            {
                StartCoroutine(DepleatMeat());
            }
        }
        else
        {
            fsm.ActivateState<HuntingState>();
            grillParticles.Stop();
            meat.SetActive(false);
        }
    }

    // Coroutine used for taking time for depleting meat.
    IEnumerator DepleatMeat()
    {
        _isDepleating = true;
        yield return new WaitForSeconds(ChefGameManager.chefGameManager.timePerMeat);
        ChefGameManager.chefGameManager.DepleatMeat();
        _isDepleating = false;
    }
    
    // Called when a Bear was found during the Hunting State.
    public void FoundBear(GameObject bear)
    {
        // Stores the reference to the bear that is being targeted.
        bearTargeted = bear;

        // Activates the pursuit behaviour.
        fsm.ActivateState<PursuitState>();
    }

    // Called when the chef misses the bear.
    public void MissedBear()
    {
        fsm.ActivateState<HuntingState>();
    }

    // Called when a Bear is hunted.
    public void BearHunted()
    {
        // Destroys the bear and announces the bear has been hunted.
        ChefGameManager.chefGameManager.BearHunted();
        fsm.ActivateState<ReturnChefState>();

        // Announces to the bear that it's been hunted. Code makes things looks much funnier.
        bearTargeted.GetComponent<BearBehaviour>().BearHunted();
        bearTargeted = null;
    }
    
    // Called when the chef returns to its origin point.
    public void ReturnedToGrill()
    {
        // Restarts particles and restarts cooking state.
        grillParticles.Play();
        meat.SetActive(true);
        fsm.ActivateState<CookingState>();
    }
}
