﻿using UnityEngine;
using System.Collections;

// Used for the chef to return to its original position.
public class ReturnChefState : State {

    // Internal Variables.
    private SteeringVehicle _vehicle;
    private ChefBehaviour _chefBehaviour;

    // Events that happen when the State is started.
    public override void OnEnterState()
    {
        // Gets the vehicle component.
        _chefBehaviour = agent.GetComponent<ChefBehaviour>();
        _vehicle = agent.GetComponent<SteeringVehicle>();

        // Adds the pursuit behaviour.
        SteeringSeek seek = agent.AddComponent<SteeringSeek>();
        _vehicle.steeringBehaviours.Insert(0, seek);

        // Seeks back to the origin position.
        seek.target = _chefBehaviour.startPos.transform;
    }

    // Events that happen when the State is running and being called on Update().
    public override void UpdateState()
    {
        // Checks if the chef is near enough to hunt the bear.
        if (Vector3.Distance(_chefBehaviour.startPos.transform.position, agent.transform.position) < 1f)
        {
            // Confirms that the chef has returned.
            _chefBehaviour.ReturnedToGrill();
        }
    }

    // Events that happen when the State is stopped.
    public override void OnExitState()
    {
        // Removes the component.
        agent.GetComponent<SteeringVehicle>().steeringBehaviours.RemoveAt(0);
        GameObject.Destroy(agent.GetComponent<SteeringSeek>());
    }
}