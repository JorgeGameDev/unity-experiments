﻿using UnityEngine;
using System.Collections;

// State called when the chef is close to the bear.
public class EvadeState : State {

    // Internal Variables.
    private SteeringVehicle _vehicle;
    private BearBehaviour _bearBehaviour;

    // Events that happen when the State is started.
    public override void OnEnterState()
    {
        // Gets the necessary components.
        _bearBehaviour = agent.GetComponent<BearBehaviour>();
        _vehicle = agent.GetComponent<SteeringVehicle>();

        // Adds Wander Behaviour for randomness.
        SteeringEvade evade = agent.gameObject.AddComponent<SteeringEvade>();

        // Sets up the variables on the evade behaviour.
        evade.evadeVehicle = ChefGameManager.chefGameManager.chef.GetComponent<SteeringVehicle>();
        evade.panicDistance = _bearBehaviour.panicDistance;

        // Adds the behaviour to the list.
        _vehicle.steeringBehaviours.Insert(0, evade);
    }

    // Events that happen when the State is running and being called on Update().
    public override void UpdateState()
    {
        // Checks if the bear has outrun the chef.
        if (Vector3.Distance(ChefGameManager.chefGameManager.chef.transform.position, agent.transform.position) > _bearBehaviour.panicDistance)
        {
            _bearBehaviour.Return();
        }
    }

    // Events that happen when the State is stopped.
    public override void OnExitState()
    {
        // Removes the component.
        _vehicle.steeringBehaviours.RemoveAt(0);
        GameObject.Destroy(agent.GetComponent<SteeringEvade>());
    }
}
