﻿using UnityEngine;
using System.Collections;

// Manages the swapping out and behaviour conditions of the Bear.
public class BearBehaviour : MonoBehaviour {

    [Header("Behaviour Properties")]
    [ReadOnly]
    public GameObject startPos;
    public float panicDistance;

    [Header("Masking Properties")]
    public LayerMask objectMask;

    // Internal References
    private FiniteStateMachine fsm;
    private Animator _animator;

    // Use this for initialization
    void Start ()
    {
        // Stores the starting position.
        startPos = new GameObject();
        startPos.name = "Bear Start";
        startPos.transform.position = transform.position;
        _animator = GetComponent<Animator>();

        // Gets the FSM.
        fsm = GetComponent<FiniteStateMachine>();

        // Loads all the states that are necessary for the bear.
        fsm.LoadState<GrassState>();
        fsm.LoadState<EvadeState>();
        fsm.LoadState<ReturnBearState>();

        // Activates the first, grass-eating state.
        fsm.ActivateState<GrassState>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Clamps the movement to the game zone.
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, ChefGameManager.chefGameManager.limitX.x, ChefGameManager.chefGameManager.limitX.y);
        pos.z = Mathf.Clamp(pos.z, ChefGameManager.chefGameManager.limitZ.x, ChefGameManager.chefGameManager.limitZ.y);
        transform.position = pos;
        transform.rotation = Quaternion.LookRotation(GetComponent<SteeringVehicle>().direction);
    }

    // Called when the chef gets near to this specific bear.
    public void ChefNear()
    {
        // Activates the evade state.
        fsm.ActivateState<EvadeState>();
    }
    
    // Called when the bear is now safe and can return to it's original point.
    public void Return()
    {
        fsm.ActivateState<ReturnBearState>();
    }

    // Called when the bear returns to its origin point.
    public void ReturnedToGrass()
    {
        // Restarts the Grass State.
        fsm.ActivateState<GrassState>();
    }

    // Called when the bear is hunted.
    public void BearHunted()
    {
        _animator.SetTrigger("Hunted");
        GetComponent<SteeringVehicle>().RemoveBehaviours();
    }

    // Called from the animator to destroy the bear object.
    public void DestroyBear()
    {
        fsm = null;
        Destroy(gameObject);
    }

    // Called when the bear is destroyed.
    void OnDestroy()
    {
        Destroy(startPos);
    }
}
