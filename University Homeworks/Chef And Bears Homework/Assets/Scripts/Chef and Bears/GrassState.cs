﻿using UnityEngine;
using System.Collections;

// State where the Bear waits for the chef to fetch them.
public class GrassState : State {

    // Internal Variables.
    private SteeringVehicle _vehicle;
    private BearBehaviour _bearBehaviour;

    // Events that happen when the State is started.
    public override void OnEnterState()
    {
        // Gets the necessary components.
        _bearBehaviour = agent.GetComponent<BearBehaviour>();
        _vehicle = agent.GetComponent<SteeringVehicle>();
    
        // Adds Wander Behaviour for randomness.
        SteeringWander wander = agent.gameObject.AddComponent<SteeringWander>();

        // Sets up the variables on the wander behaviour.
        wander.distance = 8f;
        wander.radius = 2f;
        wander.jitter = 0.3f;

        // Adds the behaviour to the list.
        _vehicle.steeringBehaviours.Insert(0, wander);
    }

    // Events that happen when the State is running and being called on Update().
    public override void UpdateState()
    {
        // Checks if the chef is near to the bear.
        if(Vector3.Distance(ChefGameManager.chefGameManager.chef.transform.position, agent.transform.position) < _bearBehaviour.panicDistance)
        {
            _bearBehaviour.ChefNear();
        }
    }

    // Events that happen when the State is stopped.
    public override void OnExitState()
    {
        // Removes the component.
        _vehicle.steeringBehaviours.RemoveAt(0);
        GameObject.Destroy(agent.GetComponent<SteeringWander>());
    }
}
