﻿using UnityEngine;
using System.Collections;

public class SteeringEvade : SteeringBehaviour
{
    [Header("Evade Properties")]
    public SteeringVehicle evadeVehicle;
    public float panicDistance;

    // Implements the base Calculate function.
    public override Vector3 Calculate(SteeringVehicle vehicle)
    {
        // Calculates the difference between the vehicles.
        Vector3 toEvade = evadeVehicle.transform.position - vehicle.transform.position;

        // Calculates the relative direction between the two vehicles, and checks if they are facing each other.
        float relativeDirection = Vector3.Dot(vehicle.direction, evadeVehicle.direction);
        if (relativeDirection < -0.95f)
        {
            return helper.Flee(vehicle, evadeVehicle.transform.position, panicDistance);
        }
        else
        {
            // Predicts the destination of the other vehicle in order to pursuit it.
            float lookAhead = toEvade.magnitude / (evadeVehicle.velocity.magnitude + vehicle.maxSpeed);
            Vector3 target = evadeVehicle.transform.position + (evadeVehicle.direction * lookAhead);
            return helper.Flee(vehicle, target, panicDistance);
        }
    }
}
