﻿using UnityEngine;
using System.Collections;

public class HueShift : MonoBehaviour {

    [Header("Color Variables")]
    [Range(0, 1)]
    public float shiftFrame;
    [Range(0, 1)]
    public float saturation;
    [Range(0, 1)]
    public float value;

    // Private Internal Components
    private float _hueColor;
    private Color _finalColor;
    private MeshRenderer _meshRenderer;

	// Use this for initialization
	void Start () {
        // Gets the necessary external components.
        _meshRenderer = GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        // Assings the values to the hue variable.
        _hueColor += shiftFrame;

        // Loops back depending on the value.
        if(_hueColor >= 1)
        {
            _hueColor = 0;
        }

        // Converts the vector into a new color.
        _finalColor = Color.HSVToRGB(_hueColor, saturation, value);
		_meshRenderer.material.color = _finalColor;
		_meshRenderer.material.SetColor("_EmissionColor", _finalColor * 1f);
    }
}
