﻿using UnityEngine;
using System.Collections;

// Manages Player and Camera Movement.
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {

    [Header("Player Properties")]
    public float speed;
    public float rotSpeed;
    public Transform groundPoint;
    public Transform lookPoint;

    // Internal Components
    private bool _isJumping;
    private Camera _mainCamera;
    private Rigidbody _rigidbody;
    private Vector3 _cameraDiference;

	// Use this for initialization
	void Start () {
        // Gets the main camera.
        _mainCamera = Camera.main;
        _rigidbody = GetComponent<Rigidbody>();
        _cameraDiference = _mainCamera.transform.position - transform.position;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        PlayerMovement();
    }

    // Used to update the player position.
    void PlayerMovement()
    {
        // Updates the player velocity and camera's position depending on the player's input.
        float horInput = Input.GetAxis("Horizontal");
        float verInput = Input.GetAxis("Vertical");

        // Rotates the player with the new velocity.
        Vector3 newMovement = Vector3.zero;
        bool isGrounded = Physics.CheckSphere(groundPoint.transform.position, 0.001f);

        // Checks if the player has pressed the jump key.
        if (isGrounded)
        {
            _isJumping = false;
            if (Input.GetButtonDown("Jump") && !_isJumping)
            {
                _isJumping = true;
                _rigidbody.AddForce(Vector3.up * 200);
            }
        }
        else if(!isGrounded)
        {
            _isJumping = true;
        }

        newMovement.z = verInput * speed * Time.deltaTime;

        // Adds the movement.
        transform.Rotate(new Vector3(0, horInput * rotSpeed, 0));
        transform.Translate(newMovement);

        // Updates the camera position.
        _cameraDiference = Quaternion.AngleAxis(horInput * rotSpeed, Vector3.up) * _cameraDiference;
        UpdateCameraPosition();
    }

    // Used to update camera position.
    void UpdateCameraPosition()
    {
        Vector3 newCameraPos = transform.position + _cameraDiference;
        _mainCamera.transform.position = Vector3.MoveTowards(_mainCamera.transform.position, newCameraPos, speed * 2 * Time.deltaTime);

        // And Rotation!
        _mainCamera.transform.LookAt(lookPoint);
    }
}
