﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Manages Player and Camera Movement.
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {

    [Header("Player Properties")]
    public float speed;
    public float rotSpeed;
    public Transform groundPoint;
    public Transform lookPoint;

    [Header("Camera Control")]
    public float cameraSensetivity;
    public Vector2 limitCameraX;

    [Header("PickUps Interaction")]
    public Text interactionText;
    private GameObject _pickedObject;

    // Internal Components
    private bool _isJumping;
    private Rigidbody _rigidbody;
    private Camera _camera;

	// Use this for initialization
	void Start () {
        // Gets the necessary components.
        _rigidbody = GetComponent<Rigidbody>();
        _camera = Camera.main;
        Cursor.lockState = CursorLockMode.Locked;
    }
	
	// Update is called once every physics second.
	void FixedUpdate ()
    {
        // Checks if the mouse is locked, if not, locks it.
        if(Cursor.lockState == CursorLockMode.None)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        PlayerMovement();
        MouseMovement();
        Interact();
    }

    // Used to update the player position.
    void PlayerMovement()
    {
        // Updates the player velocity and camera's position depending on the player's input.
        float verInput = Input.GetAxis("Vertical");
        float horInput = Input.GetAxis("Horizontal");

        // Rotates the player with the new velocity.
        Vector3 newMovement = Vector3.zero;
        bool isGrounded = Physics.CheckSphere(groundPoint.transform.position, 0.001f);

        // Checks if the player has pressed the jump key.
        if (isGrounded)
        {
            _isJumping = false;
            if (Input.GetButtonDown("Jump") && !_isJumping)
            {
                _isJumping = true;
                _rigidbody.AddForce(Vector3.up * 200);
            }
        }
        else if (!isGrounded)
        {
            _isJumping = true;
        }

        // Calculates the movement change in the player.
        newMovement.z = verInput * speed * Time.deltaTime;
        newMovement.x = horInput * speed * Time.deltaTime;
        transform.Translate(newMovement);
    }

    // Used to change the camera position based on the mouse position.
    void MouseMovement()
    {
        // Gets the mouse variables.
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        // Updates the camera rotation based on the axis movement.
        Vector3 rotation = _camera.transform.rotation.eulerAngles;

        // Adds to the rotation.
        rotation.x += mouseY;
        rotation.y += mouseX * cameraSensetivity;

        // Clamps the rotation in the X axis and applies it to the camera.
        _camera.transform.localRotation = Quaternion.AngleAxis(rotation.x, Vector3.right);

        // Updates the player rotation acording to the camera.
        Vector3 playerRotation = new Vector3(0, rotation.y, 0);
        transform.rotation = Quaternion.Euler(playerRotation);
    }

    // Used for interactions.
    void Interact()
    {
        // Does a raycast from the camera.
        if(_pickedObject == null)
        {
            PickObject();
        }
        else
        {
            HoldObject();
        }
    }

    // Used for picking objects when there's no object.
    void PickObject()
    {
        Ray cameraRay = new Ray(_camera.transform.position, _camera.transform.forward);
        RaycastHit raycastHit;
        if (Physics.Raycast(cameraRay, out raycastHit, 2.5f))
        {
            // Checks if the object is a pickup.
            if (raycastHit.collider.CompareTag("Interactible/Pickup"))
            {
                interactionText.text = "Pick Me!";

                // Checks if the player wants to pick up the object.
                if (Input.GetButtonDown("Interact"))
                {
                    _pickedObject = raycastHit.collider.gameObject;
                    _pickedObject.GetComponent<Rigidbody>().isKinematic = true;
                }
            }
        }
        else
        {
            interactionText.text = "";
        }
    }

    // Used for droping the held object whenever the player wants.
    void HoldObject()
    {
        // Makes the object be placed in front of the player.
        Vector3 offsetposition = transform.position;
        offsetposition.y = transform.position.y + 1.5f;
        _pickedObject.transform.position = offsetposition + _camera.transform.forward * 2.65f;
        interactionText.text = "Drop me!";

        // Drops the object if the player requests such.
        if (Input.GetButtonDown("Interact"))
        {
            _pickedObject.GetComponent<Rigidbody>().isKinematic = false;
            _pickedObject = null;
        }
    }
}
