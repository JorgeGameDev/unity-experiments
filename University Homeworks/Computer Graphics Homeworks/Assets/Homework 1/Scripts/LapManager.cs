﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Used for managing time laps and checking if the player has touched all flag-pole triggers.
/// </summary>
public class LapManager : MonoBehaviour
{
    // Static Reference
    public static LapManager lapManager;

    [Header("UI Elements")]
    public Camera UICamera;
    public GameObject UICubie;
    public Text lapTime;
    public Text lapBest;
    public Text cubieText;

    [Header("Audio Clips")]
    public AudioClip cubieClip;
    public AudioClip lapClip;

    // Internal, keeps tracks of the flag checked.
    private bool[] checkedFlags = new bool[3];
    private float checkedTime;
    private float currentTime;
    private float bestTime;
    private float cubiesCollected;
    private GameObject[] cubieObjects;
    private AudioSource _audioSource;
    private Animator _UICubieAnimator;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        // Sets this as a static reference to simplify things.
        lapManager = this;
    }

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    public void Start()
    {
        // Gets all the cubies and stores them to a list.
        cubieObjects = GameObject.FindGameObjectsWithTag("Cubie");

        // Gets the components from this lap manager.
        _audioSource = GetComponent<AudioSource>();
        _UICubieAnimator = UICubie.GetComponent<Animator>();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    private void Update()
    {
        // Calculates the time differences.
        currentTime = Time.time;
        float differenceTime = currentTime - checkedTime;

        // Calculates the difference in minutes, seconds and milliseconds.
        int seconds = (int)differenceTime % 60;
        int minutes = (int)differenceTime / 60;
        float millisecondsFraction = differenceTime * 1000;
        float milliseconds = millisecondsFraction % 1000;

        // Formats the string to show the current lap.
        string currentTimeString = String.Format("{0:00}:{1:00}.{2:00}", minutes, seconds, milliseconds);
        lapTime.text = currentTimeString;
    }

    /// <summary>
    /// Used for toggling a flag as being done.
    /// </summary>
    public void CheckFlag(int flagId)
    {
        checkedFlags[flagId] = true;
    }

    /// <summary>
    /// Adds a cubie to the count and updates the UI text.
    /// </summary>
    public void CollectCubie(GameObject cubieCollected)
    {
        // Collected Cubies.
        cubiesCollected++;
        cubieText.text = "Cubies\n" + cubiesCollected.ToString();
        _audioSource.PlayOneShot(cubieClip);

        // Converts objects between camera spaces.
        Matrix4x4 MMatrix = cubieCollected.transform.localToWorldMatrix;
        Matrix4x4 MVMatrix = Camera.main.worldToCameraMatrix * MMatrix;
        Matrix4x4 MVPMatrix = Camera.main.projectionMatrix * MVMatrix;

        // Converts the matrix to screen space.
        Vector3 screenSpace = MVPMatrix.MultiplyPoint(Vector3.zero);

        // Coverts objects back to the new camera space.
        Vector3 cameraPosition = UICamera.projectionMatrix.inverse.MultiplyPoint(screenSpace);
        Vector3 worldPosition = UICamera.worldToCameraMatrix.inverse.MultiplyPoint(screenSpace);

        // Sets the world position of the Cubie to the right Z and marks it as movable to the animation.
        worldPosition.z = 2;
        cubieCollected.transform.position = worldPosition;
        cubieCollected.GetComponent<Cubie>().isCollected = true;
    }

    /// <summary>
    /// Plays the Cubie animation in the UI;
    /// </summary>
    public void PlayCubieAnimation()
    {
        _UICubieAnimator.SetTrigger("CubieCollected");
    }

    /// <summary>
    /// Used for registering a lap as done.
    /// </summary>
    public void RegisterNewLap()
    {
        // Calculates the difference between lap time.
        float checkedBest = -(checkedTime - Time.time);

        if (checkedBest < bestTime || bestTime == 0)
        {
            // Activates the best lap counter.
            if (bestTime == 0)
            {
                lapBest.gameObject.SetActive(true);
            }

            // Calculates the best time to show up.
            int seconds = (int)checkedBest % 60;
            int minutes = (int)checkedBest / 60;
            float millisecondsFraction = checkedBest * 1000;
            float milliseconds = millisecondsFraction % 1000;

            // Formats the string to show the current lap.
            string bestTimeString = String.Format("Best Lap {0:00}:{1:00}.{2:00}", minutes, seconds, milliseconds);
            lapBest.text = bestTimeString;

            // Saves the best time.
            bestTime = checkedBest;
        }

        // Marks this time as being checked to restart the clock.
        checkedTime = Time.time;
        _audioSource.PlayOneShot(lapClip);

        // Turns all the checkpoint bools false.
        for (int b = 0; b < checkedFlags.Length; b++)
        {
            checkedFlags[b] = false;
        }

        // Turns all cubies on again.
        foreach(GameObject cubie in cubieObjects)
        {
            cubie.GetComponent<Cubie>().ResetCubie();
        }
    }

    /// <summary>
    /// Checks if a new lap was counted in the player.
    /// </summary>
    public void CheckForNewLap()
    {
        if(CheckCheckpoints())
        {
            RegisterNewLap();
        }
    }

    /// <summary>
    /// Checks if all the checkpoints have been triggered.
    /// </summary>
    private bool CheckCheckpoints()
    {
        // If any checkpoint is false, execution will be halted.
        foreach (bool checkpoint in checkedFlags)
        {
            if (!checkpoint)
            {
                return false;
            }
        }

        // All check points are true!
        return true;
    }
}
