﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines a checkpoint, that when the player touches the trigger, it will 
/// </summary>
public class FlagCheckpoints : MonoBehaviour {

    [Header("Flag Properties")]
    public int flagId;

    /// <summary>
    /// OnTriggerEnter is called when the another Collides enters the trigger.
    /// </summary>
    private void OnTriggerEnter(Collider collider)
    {
        // Checks if all the flags are turned on.
        if (collider.CompareTag("Player"))
        {
            LapManager.lapManager.CheckFlag(flagId);
        }
    }
}
