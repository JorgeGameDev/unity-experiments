﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used by the finishing line to check if a new lap can be counted.
/// </summary>
public class FinishingCheckpoints : MonoBehaviour {

    /// <summary>
    /// OnTriggerEnter is called when the another Collides enters the trigger.
    /// </summary>
    private void OnTriggerEnter(Collider collider)
    {
        // Checks if all the flags are turned on.
        if (collider.CompareTag("Player"))
        {
            LapManager.lapManager.CheckForNewLap();
        }
    }
}
