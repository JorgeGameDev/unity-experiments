﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used for transforming the player's and cameras position.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class TransformController : MonoBehaviour
{
    [Header("Movement Properties")]
    public float playerSpeed;
    public float cameraSpeed;

    [Header("Engine Audioclip")]
    public AudioClip engineIdle;
    public AudioClip engineMoving;

    // Internal Components
    private AudioSource _audioSource;
    private Rigidbody _rigidbody;
    private Camera _referenceCamera;
    private Vector3 _playerOffset;

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        // Gets the Rigidbody from the player.
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();

        // Gets the Main Camera to update it's position.
        _referenceCamera = Camera.main;

        // Calculates the offset that the camera should be from the player.
        _playerOffset = transform.position - _referenceCamera.transform.position;
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    private void FixedUpdate()
    {
        // Checks if the player has pressed any input.
        float horAxis = Input.GetAxis("Horizontal");
        float verAxis = Input.GetAxis("Vertical");
        float realHor = Input.GetAxisRaw("Horizontal");
        float realVer = Input.GetAxisRaw("Vertical");

        // Updates the player rigidbody speed based on the axis.
        _rigidbody.velocity = new Vector3(horAxis * playerSpeed, _rigidbody.velocity.y, verAxis * playerSpeed);

        // Rotates the model according to axis input.
        if (realHor != 0 || realVer != 0)
        {
            float angle = Mathf.Atan2(verAxis, -horAxis) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, angle, 0), Time.deltaTime * cameraSpeed);

            // Changes the engine sound.
            if(_audioSource.clip != engineMoving)
            {
                _audioSource.clip = engineMoving;
                _audioSource.Play();
            }
        }
        else
        {
            // Changes the engine sound.
            if (_audioSource.clip != engineIdle)
            {
                _audioSource.clip = engineIdle;
                _audioSource.Play();
            }
        }

        // Updates the camera position to lerp to the player's position.
        Vector3 finalPosition = transform.position - _playerOffset;
        _referenceCamera.transform.position = Vector3.Slerp(_referenceCamera.transform.position, finalPosition, Time.deltaTime * cameraSpeed);
    }
}
