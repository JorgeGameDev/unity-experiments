﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Repositions a cube by it's UI Slider.
/// </summary>
public class RepositionCube : MonoBehaviour {

    [Header("Position Slider")]
    public Slider positionSlider;

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        // Gets the value from the position slider.
        float xPosition = positionSlider.value;

        // Tweaks the local position of the cube by the given value.
        Vector3 cubePosition = transform.localPosition;
        cubePosition.x = xPosition;
        transform.localPosition = cubePosition;
    }
}
