﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Swaps the camera's focus between the Render Texture and Mesh Extrude Example.
/// </summary>
public class RenderMeshSwap : MonoBehaviour {

    [Header("Camera Animator")]
    public Animator cameraAnimator;

    [Header("Canvas Panels")]
    public GameObject renderPanel;
    public GameObject meshPanel;

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        // Gets the Animator from the Main Camera.
        cameraAnimator = Camera.main.GetComponent<Animator>();
    }

    /// <summary>
    /// Swaps the Camera View over to the Mesh View, disabling the Render Canvas.
    /// </summary>
    public void SwapToMesh()
    {
        renderPanel.SetActive(false);
        meshPanel.SetActive(true);
        cameraAnimator.SetBool("CameraRight", true);
    }

    /// <summary>
    /// Swaps the Camera View to the Render View, disabling the Mesh Canvas.
    /// </summary>
    public void SwapToRender()
    {
        renderPanel.SetActive(true);
        meshPanel.SetActive(false);
        cameraAnimator.SetBool("CameraRight", false);
    }
}
