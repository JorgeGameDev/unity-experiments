﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates a custom mesh and assigns them to the Mesh Filter in this object.
/// </summary>
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class ProceduralMesh : MonoBehaviour
{
    [Header("Mesh Properties")]
    public Vector2 quadSize;
    public Material renderMaterial;

    // Internal Variables
    private MeshFilter _meshFilter;
    private MeshRenderer _meshRenderer;

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    public void Start()
    {
        _meshFilter = GetComponent<MeshFilter>();
        _meshRenderer = GetComponent<MeshRenderer>();

        // Makes the mesh.
        MakeMesh();
    }

    /// <summary>
    /// Creates a custom mesh that will be used.
    /// </summary>
    void MakeMesh()
    {
        // Creates a new mesh.
        Mesh mesh = new Mesh();
        _meshFilter.mesh = mesh;

        // Creates a new set of points that will be used for creating the mesh.
        List<Vector3> vertices = new List<Vector3>();
        vertices.Add(new Vector3(-quadSize.x / 2, -quadSize.y / 2));
        vertices.Add(new Vector3(quadSize.x / 2, -quadSize.y / 2));
        vertices.Add(new Vector3(-quadSize.x / 2, quadSize.y / 2));
        vertices.Add(new Vector3(quadSize.x / 2, quadSize.y / 2));
        mesh.SetVertices(vertices);

        // Calculates the triangles from the given vertices's.
        int[] triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 2;
        triangles[2] = 1;
        triangles[3] = 1;
        triangles[4] = 2;
        triangles[5] = 3;
        mesh.SetTriangles(triangles, 0);

        // Creates the UV map of the texture.
        Vector2[] UVs = new Vector2[4];
        UVs[0] = new Vector2(0, 0);
        UVs[1] = new Vector2(1, 0);
        UVs[2] = new Vector2(0, 1);
        UVs[3] = new Vector2(1, 1);

        // Recalculates the mesh.
        mesh.uv = UVs;
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();

        // Assigns the Render Material to the generated quad.
        _meshRenderer.material = renderMaterial;
    }
}
