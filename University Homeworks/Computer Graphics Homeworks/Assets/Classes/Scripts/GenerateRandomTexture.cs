﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Generates a random Texture2D using perlin noise.
/// </summary>
public class GenerateRandomTexture : MonoBehaviour
{
    // Texture2D are technically just imported textures.
    [Header("Texture Parameters")]
    public Texture2D texture;
    public int texWidth, texHeight;

    [Header("Noise Properties")]
    public float scale;

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    public void Start()
    {
        // Initializes the Texture2D.
        texture = new Texture2D(texWidth, texHeight);

        // Loops through all the pixels of a texture to create image data on it.
        for (int w = 0; w < texWidth; w++)
        {
            for (int h = 0; h < texHeight; h++)
            {
                // Creates a value from the perlin noise.
                float x = (float)w / (float)texWidth * scale;
                float y = (float)h / (float)texHeight * scale;
                float noise = Mathf.PerlinNoise(x, y);

                // Creates a carolo from the noise.
                texture.SetPixel(w, h, new Color(noise, noise, noise));
            }
        }

        // Applies the texture, saving it to the GPU.
        texture.Apply();

        GetComponent<Renderer>().material.SetTexture("_NoiseTex", texture);
    }
}
