﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Extrudes a mesh by it's normals given a extrusion range.
/// </summary>
public class MeshExtrusion : MonoBehaviour
{
    [Header("Extrusion Slider")]
    public Slider extrusionRange;
    private float lastExtrusion;

    // Internal
    private MeshFilter _meshFilter;
    private Mesh _sharedMesh;
    private Vector3[] _vertices;

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    public void Start()
    {
        // Sets up extrusion and get necessary components.
        lastExtrusion = 0;
        _meshFilter = GetComponent<MeshFilter>();

        // Gets the model vertexes.
        _sharedMesh = _meshFilter.mesh;
        _vertices = _sharedMesh.vertices;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    public void Update()
    {
        // Checks if the values between the last extrusion and the slider are different, to avoid constantly repositioning vertices.
        if(lastExtrusion != extrusionRange.value)
        {
            Vector3[] newVertices = new Vector3[_vertices.Length];
            System.Array.Copy(_vertices, newVertices, _vertices.Length);

            // Extrudes each vertex by it's normal value.
            for (int i = 0; i < _vertices.Length; i++)
            {
                newVertices[i] = newVertices[i] + _sharedMesh.normals[i] * extrusionRange.value;
            }

            // Assigns the newly positioned vertices's to the mesh.
            _sharedMesh.vertices = newVertices;
            lastExtrusion = extrusionRange.value;
        }
    }
}
