﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Allows an object to be hold and moved across the screen using an transformation matrix.
/// </summary>
public class GrabAndHold : MonoBehaviour
{
    [Header("Reference Object")]
    public GameObject referenceObject;
    public Vector3 multiplyVector;

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    public void Update()
    {
        // Calculates the screen position.
        CalculateWorldPosition();
        CalculateScreenPosition();

        // *Whistles*
        FancyWorldScreen();
    }

    /// <summary>
    /// Calculates the screen coordinates of the object manually.
    /// </summary>
    private void CalculateScreenPosition()
    {
        // Gets the world matrix from the local space.
        Matrix4x4 MMatrix = referenceObject.transform.localToWorldMatrix;
        Matrix4x4 MVMatrix = Camera.main.worldToCameraMatrix * MMatrix;
        Matrix4x4 MVPMatrix = Camera.main.projectionMatrix * MVMatrix;

        // Converts the matrix to screen space.
        Vector3 screenSpace = MVPMatrix.MultiplyPoint(multiplyVector);

        // Gives the vector some space in order for the corner to be 0, 0 and not the middle point.
        screenSpace = (screenSpace + new Vector3(1, -1, 1)) * 0.5f;

        // Converts the vector over to pixel space.
        Vector2 pixelSize = new Vector2(screenSpace.x * Screen.width, screenSpace.y * Screen.height);
        Debug.Log(pixelSize);
    }

    /// <summary>
    /// Calculates the world coordinates of the mouse Position.
    /// </summary>
    private void CalculateWorldPosition()
    {
        // Gets the mouse position to convert.
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = 10;

        // Gets the projection values from the cursor manually.
        mousePosition.x /= Screen.width;
        mousePosition.y /= Screen.height;
        mousePosition -= new Vector3(0.5f, 0.5f, 0);
        mousePosition.Scale(new Vector3(2, 2, 1));

        // Calculates the Camera Position with the projection matrix.
        Vector3 cameraPosition = Camera.main.projectionMatrix.inverse.MultiplyPoint(mousePosition);
        Vector3 worldPosition = Camera.main.worldToCameraMatrix.inverse.MultiplyPoint(cameraPosition);

        // Assigns the object to the mouse position.
        referenceObject.transform.position = worldPosition;
    }

    /// <summary>
    /// Calculates the world position of the object manually.
    /// </summary>
    private void FancyWorldScreen()
    {
        // Does it the fancy, uncomplex way.
        Vector3 screenSpace = Camera.main.WorldToScreenPoint(referenceObject.transform.position);
        screenSpace.x -= Screen.width / 2;
        screenSpace.y -= Screen.height / 2;
    }
}
