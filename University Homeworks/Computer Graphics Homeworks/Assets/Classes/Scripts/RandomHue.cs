﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Procedually changes the hue of an object and it's emission property.
/// </summary>
public class RandomHue : MonoBehaviour
{
    [Header("Hue Properties")]
    public float hueAdd;
    private float currentHue;

    // Components.
    private Renderer render;
    private Material material; 

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    public void Start()
    {
        render = GetComponent<Renderer>();
        material = render.material;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    public void Update()
    {
        // Adds to the current hue.
        currentHue += hueAdd;

        // Clamps the current hue back to zero.
        if(currentHue > 360)
        {
            currentHue = 0;
        }

        // Updates the color and emission color of the material.
        Color newHueShift = Color.HSVToRGB(currentHue / 360, 1, 1);
        material.color = newHueShift;
        material.SetColor("_EmissionColor", newHueShift * 2f);

        // Updates the GI of the object.
        DynamicGI.SetEmissive(render, newHueShift * 2f);
        DynamicGI.UpdateEnvironment();
        RendererExtensions.UpdateGIMaterials(render);
    }
}
