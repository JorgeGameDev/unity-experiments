﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Uses low-level graphical functions to manipulate a mesh.
/// </summary>
public class GLDraw : MonoBehaviour
{
    [Header("Materials")]
    public Material material;

    [Header("Circle Properties")]
    public float radius;
    public int numberOfSides;

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    public void Start()
    {
        // Delegates the RengerGL function to the post renderer.
        Camera.onPostRender += RenderGL;
    }

    /// <summary>
    /// Does stuff with the GL library.
    /// </summary>
    void RenderGL(Camera camera)
    {
        // Prepares the material for the circle.
        material.SetPass(0);
        GL.PushMatrix();
        GL.MultMatrix(transform.localToWorldMatrix);
        GL.Begin(GL.TRIANGLE_STRIP);

        // For each side of the circle draw a vertex.
        for (float a = 0; a < 360; a += (360 / (numberOfSides - 1)))
        {
            float heading = a * Mathf.Deg2Rad;
            GL.Vertex(Vector3.zero);
            Vector3 vertice = new Vector3(Mathf.Cos(heading) * radius, Mathf.Sin(heading) * radius, 0);
            GL.Vertex(vertice);
        }

        // Draws the final vertice.
        float finalAngle = 360;
        float finalHeading = finalAngle * Mathf.Deg2Rad;
        Vector3 finalVertice = new Vector3(Mathf.Cos(finalHeading) * radius, Mathf.Sin(finalHeading) * radius, 0);
        GL.Vertex(finalVertice);

        // Closes the material.
        GL.End();
        GL.PopMatrix();
    }

    /// <summary>
    /// This function is called when the MonoBehaviour will be destroyed.
    /// </summary>
    public void OnDestroy()
    {
        Camera.onPostRender -= RenderGL;    
    }
}
