﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used for collecting cubies on the Game World, and moving them towards the UI camera.
/// </summary>
public class Cubie : MonoBehaviour {

    [HideInInspector]
    public bool isCollected;
    private Vector3 originalPosition;

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        originalPosition = transform.position;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    private void Update ()
    {
        // Checks if the cubie is collected to move it towards the UI.
		if(isCollected)
        {
            transform.position = Vector3.MoveTowards(transform.position, LapManager.lapManager.UICubie.transform.position, Time.deltaTime * 6f);

            // Checks if the cubie is near the UI enough.
            if(Vector3.Distance(transform.position, LapManager.lapManager.UICubie.transform.position) < 1f)
            {
                LapManager.lapManager.PlayCubieAnimation();
                gameObject.SetActive(false);
            }
        }
	}

    /// <summary>
    /// Resets a Cubie back to it's original position.
    /// </summary>
    public void ResetCubie()
    {
        // Marks the object as not being collected.
        isCollected = false;

        // Resets the scale and position.
        transform.localScale = Vector3.one;
        transform.position = originalPosition;
        gameObject.SetActive(true);
    }

    /// <summary>
    /// OnTriggerEnter is called when the another Collides enters the trigger.
    /// </summary>
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            // Collects the Cubie to the UI.
            LapManager.lapManager.CollectCubie(gameObject);
            transform.localScale = Vector3.one * 2;
        }
    }
}
