﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Shifts the material properties with the player's control, or over time with the sin function.
/// </summary>
public class MaterialShift : MonoBehaviour
{
    // References
    private Material material;
    private float offset;
    private float currentHue;
    public float changeRate;

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    public void Start()
    {
        material = GetComponent<MeshRenderer>().material;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    public void Update()
    {
        TextureOffset();
        UpdateEmission();
    }

    /// <summary>
    /// Offsets the texture horizontal forever.
    /// </summary>
    private void TextureOffset()
    {
        // Shifts the texture depending on the horizontal input.
        float horInput = Input.GetAxis("Horizontal") * Time.deltaTime;
        offset += horInput;

        // Clamps the offset to its limit.
        offset = Mathf.Clamp(offset, -1, 1);

        // Loops back the values.
        if (offset == -1)
            offset = 1;
        else if (offset == 1)
            offset = -1;

        // Sets the texture offsets.
        material.SetTextureOffset("_MainTex", new Vector2(offset, offset));
        material.SetTextureOffset("_EmissionMap", new Vector2(offset, offset));
    }

    /// <summary>
    /// Updates the emission of the material over time.
    /// </summary>
    private void UpdateEmission()
    {
        // Shifts the hue of the color and clamps it back.
        currentHue += changeRate;
        if (currentHue > 360)
            currentHue = 0;

        // Creates a new color from the shifted hue.
        Color color = Color.HSVToRGB(currentHue / 360, 1, 1);

        // Pulses the material over time using
        float emissionIntensity = Mathf.Abs(Mathf.Sin(Time.time)) * 2f;
        material.SetColor("_EmissionColor", color * emissionIntensity);

        // Updates the Dynamic GI.
        DynamicGI.SetEmissive(GetComponent<Renderer>(), color * emissionIntensity);
        DynamicGI.UpdateEnvironment();
    }
}
