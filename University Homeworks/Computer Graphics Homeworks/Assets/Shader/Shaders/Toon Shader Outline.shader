﻿Shader "Custom/Toon Shader Outline" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_CelSteps("Cellshading Steps", int) = 1

		_OutlineAmount("Outline Amount", float) = 0.2
		_OutlineColor("Outline Color", Color) = (0,0,0,0)

		_ExtrusionAmmount("Extrusion Ammount", Range(0,1)) = 0 
		_ExtrusionFactor("Extrusion Factor", Range(0,1)) = 0.2
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf CustomLambert vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		// Variables that are used in the shader. Should share the same name as the Properties.
		fixed4 _Color;
		float _OutlineAmount;
		float _ExtrusionAmmount;
		float _ExtrusionFactor;
		fixed4 _OutlineColor;
		int _CelSteps;

		// Customs a vertex 
		void vert(inout appdata_base v)
		{
			float3 localCamPos = mul(unity_WorldToObject, _WorldSpaceCameraPos);
			float3 viewDir = normalize(localCamPos - v.vertex);
			float viewDot = dot(viewDir, v.normal);

			if (viewDot <= _ExtrusionFactor)
			{
				v.vertex.xyz += v.normal * _ExtrusionAmmount;
			}
		}

		// If the material uses a custom standard model, LightingCustomLabert uses a custom lightning system.
		half4 LightingCustomLambert(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		{
			half viewDot = dot(viewDir, s.Normal);
			if (viewDot <= _OutlineAmount)
			{
				return _OutlineColor;
			}

			half lightDot = dot(lightDir, s.Normal);
			half4 cellDot = floor(lightDot * _CelSteps) / _CelSteps;
			half4 finalColor;

			finalColor.rgb = s.Albedo * _LightColor0 * (cellDot * atten);
			finalColor.a = s.Alpha;

			return finalColor;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
