﻿Shader "Custom/Wavy Shader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_NoiseTex ("Noise (2D)", 2D) = "black" {}
		_NoiseScale("Noise Scale", Range(0, 10)) = 2
		_NoiseSpeedX("Noise Speed X", Range(0, 10)) = 0
		_NoiseSpeedY("Noise Speed Y", Range(0, 10)) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NoiseTex;

		struct Input {
			float2 uv_MainTex;
			float displacement;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _NoiseScale;
		float _NoiseSpeedX;
		float _NoiseSpeedY;

		void vert(inout appdata_base v, out Input o)
		{
			float offsetX = _Time.z * _NoiseSpeedX;
			float offsetY = _Time.z * _NoiseSpeedY;
			float4 texPos = float4(v.texcoord.x * offsetX, v.texcoord.y * offsetY, 0, 0);
			float displacement = abs(tex2Dlod(_NoiseTex, texPos).r);
			v.vertex.xyz += v.normal * ((displacement - 0.5) * 2) * _NoiseScale;

			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.displacement = displacement;
		}

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb * IN.displacement;

			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
