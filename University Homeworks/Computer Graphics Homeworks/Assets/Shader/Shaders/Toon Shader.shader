﻿Shader "Custom/Toon Shader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_CelSteps("Cellshading Steps", int) = 1
		_OutlineAmount("Outline Amount", float) = 0.2
		_OutlineColor("Outline Color", Color) = (0,0,0,0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf CustomLambert fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		// Variables that are used in the shader. Should share the same name as the Properties.
		fixed4 _Color;
		float _OutlineAmount;
		fixed4 _OutlineColor;
		int _CelSteps;

		// If the material uses a custom standard model, LightingCustomLabert uses a custom lightning system.
		half4 LightingCustomLambert(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		{
			half viewDot = dot(viewDir, s.Normal);
			float finalOutline = lerp(0, _OutlineAmount, abs(_SinTime.z));
			if (viewDot <= finalOutline)
			{
				return finalOutline;
			}

			half lightDot = dot(lightDir, s.Normal);
			half4 cellDot = floor(lightDot * _CelSteps) / _CelSteps;
			half4 finalColor;

			finalColor.rgb = s.Albedo * _LightColor0 * (cellDot * atten);
			finalColor.a = s.Alpha;

			return finalColor;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
