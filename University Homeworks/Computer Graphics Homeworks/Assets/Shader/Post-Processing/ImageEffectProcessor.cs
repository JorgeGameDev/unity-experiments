﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Processes an image effect shader with a render texture. 
/// </summary>
[ExecuteInEditMode]
public class ImageEffectProcessor : MonoBehaviour
{
    [Header("Refernece Shader")]
    public string shader;

    [Header("Color Ramp")]
    public bool useRamp;
    public Texture2D colorRamp;

    // Reference Variables
    Material effectMaterial;

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    public void Start()
    {
        // Finds the shader to be used in the render material.
        effectMaterial = new Material(Shader.Find(shader));
    }

    /// <summary>
    /// OnRenderImage is called after all rendering is complete to render image.
    /// </summary>
    public void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        // Applies the material on the graphics processor.
        Graphics.Blit(source, destination, effectMaterial);

        // Shuold we use the color ramp to change the color of textures?
        if(useRamp)
        {
            effectMaterial.SetTexture("_ColorRamp", colorRamp);
        }
    }
}
