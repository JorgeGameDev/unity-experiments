﻿Shader "Hidden/DistrotionShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				// Moves the texture over time.
				float diffX = 0.5 - i.uv.x;
				float diffY = 0.5 - i.uv.y;

				// Distroces the position of the main texture.
				float texX = i.uv.x + diffX * _SinTime.z;
				float texY = i.uv.y + diffY * _SinTime.z;
				fixed4 col = tex2D(_MainTex, float2(texX, texY));
				return col;
			}
			ENDCG
		}
	}
}
