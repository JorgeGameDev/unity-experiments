﻿using UnityEngine;
using System.Collections;

// EXPERIMENT - SPLATOON MECHANICHS OF SPLATTER
public class PaintBobble : MonoBehaviour {

    // Variables relevant to this script.
    [Header("Splat Attributes")]
    private Material splatMaterial;
    public GameObject splatSplat;

    // Use this for initialization
    void Start () {
        // Sets up everything important.
        splatMaterial = GetComponent<MeshRenderer>().material;
        splatSplat.GetComponent<SpriteRenderer>().color = splatMaterial.color;
	}
	
	// Checks if the object has collided with anything.
	void OnCollisionEnter (Collision collide) {
        // Checks if the object isn't colliding with the player.
        if(collide.gameObject.tag != "Player")
        {
            // Gets the contact point of the collision.
            ContactPoint contactPoint = collide.contacts[0];
            // Spawns the splat at the contact point.
            Quaternion rotationPoint = Quaternion.LookRotation(contactPoint.normal);
            Vector3 positionPoint = contactPoint.point + contactPoint.normal * 0.01f;
            GameObject splatObj = (GameObject)Instantiate(splatSplat, positionPoint, rotationPoint);
            Destroy(gameObject);
        }
    }
}
