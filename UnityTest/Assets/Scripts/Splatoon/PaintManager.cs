﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// EXPERIMENT - SPLATOON MECHANICHS OF SPLATTER
// THIS SCRIPT OBTAINS THE COLORS AND TUNRS ON EXPERIMENT.
public class PaintManager : MonoBehaviour {

    // Important components.
    [Header("Player")]
    private GameObject _player;
    private bool _isActive = false;
    [Header("Color")]
    public Color splatColor;
    [Header("Sliders")]
    public Slider redSlider;
    public Slider greenSlider;
    public Slider blueSlider;
    [Header("World UI")]
    public Image previewImage;
    public Button startButton;

	// Use this for initialization
	void Start () {
        splatColor = new Color();
        _player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        UpdateColor();
	}

    // This function is used for the button to active the script.
    public void ManageColor ()
    {
        if(!_isActive)
        {
            _player.GetComponentInChildren<PaintSplatter>().isActive = true;
            startButton.GetComponentInChildren<Text>().text = "OFF";
            _isActive = true;
        }
        else
        {
                _player.GetComponentInChildren<PaintSplatter>().isActive = false;
                startButton.GetComponentInChildren<Text>().text = "ON";
                _isActive = false;
        }
    }

    // This function is used to update the UI and color.
    void UpdateColor ()
    {
        // Assings the color based on the sliders.
        splatColor.r = redSlider.value / 255;
        splatColor.g = greenSlider.value / 255;
        splatColor.b = blueSlider.value / 255;
        splatColor.a = 1;
        // Updates preview image and player splatcolor.
        previewImage.color = splatColor;
        _player.GetComponentInChildren<PaintSplatter>().splatColor = splatColor;
    }
}
