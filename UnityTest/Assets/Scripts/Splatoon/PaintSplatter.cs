﻿using UnityEngine;
using System.Collections;

// EXPERIMENT - SPLATOON MECHANICHS OF SPLATTER
public class PaintSplatter : MonoBehaviour {

    // Variables relevant to this script.
    [Header("Splatter Options")]
    public Color splatColor;
    public Material splatMaterial;
    public GameObject splatObject;
    public GameObject splatSplat;
    public bool isActive;
    [Header("Physics Options")]
    public float splatOffset;
    public float splatForce;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    // Checks if the player as requested the object to be fired.
        if(Input.GetButtonDown("Fire1") && isActive)
        {
            // Gives the color to the material.
            splatMaterial.color = splatColor;
            // Instanciates the blob.
            GameObject splatBob = (GameObject)Instantiate(splatObject, (transform.position + transform.forward * splatOffset), Quaternion.identity);
            splatBob.GetComponent<Rigidbody>().velocity = transform.forward * splatForce;
            // Gives the sprite that the blob is supposed to have.
            splatBob.GetComponent<PaintBobble>().splatSplat = splatSplat;
        }
	}
}
