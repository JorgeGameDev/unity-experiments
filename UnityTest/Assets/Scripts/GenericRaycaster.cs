﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

// EXPERIMENT - THIS RAYCAST IS MEANT TO BE ABLE TO DETECT BOTH PHYSICAL AND UI OBJECTS
// URGENT! CLEAN UP RAYCAST DETECTIONS
public class GenericRaycaster : MonoBehaviour {

    [Header("Physics Raycast")]
    private Ray _ray;
    private RaycastHit _rayHit;
    [Header("Graphics Raycast")]
    private EventSystem eventSys;

    // Use this for initalization.
    void Start ()
    {
        // Gets the event system and locks the cursor.
        eventSys = FindObjectOfType<EventSystem>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

	// Update is called once per frame
	void Update () {
        PhysicalRay();
        CanvasRay();
    }   

    // Does a raycast for physical elements.
    void PhysicalRay()
    {
        // Does a constant physics raycast.
        _ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        // Checks if the ray has collided with something.
        if (Physics.Raycast(_ray, out _rayHit, Camera.main.farClipPlane))
        {
            // Checks if player asked for input.
            if (Input.GetButtonDown("Submit"))
            {
                // Does a diferent interaction depending of the experiment.
                if (_rayHit.collider.gameObject.CompareTag("Waifu"))
                {
                    _rayHit.collider.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                }
            }
        }
    }

    // Does a raycast for canvas elements.
    void CanvasRay()
    {
        // Creates a new pointer point and gets the camera point from it.
        PointerEventData newPointer = new PointerEventData(EventSystem.current);
        newPointer.position = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f, 0));
        // Gets Raycast results.
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(newPointer, raycastResults);
        // Depending on the raycast result does a diferent operation.
        // If the object is an handle.
        if(raycastResults.Count > 0)
        {
            if (raycastResults[0].gameObject.name == "Handle")
            {
                // Sets the handle as selected.
                GameObject handleObj = raycastResults[0].gameObject.transform.parent.parent.gameObject;
                eventSys.SetSelectedGameObject(handleObj);
            }
            // Checks if the parent object is a button. Normally happens with text or backgrounds.
            else if (raycastResults[0].gameObject.transform.parent.gameObject.GetComponent<Button>())
            {
                // Sets the button as selected. 
                GameObject buttonObj = raycastResults[0].gameObject.transform.parent.gameObject;
                eventSys.SetSelectedGameObject(buttonObj);
            }
            else
            {
                eventSys.SetSelectedGameObject(null);
            }
        }
        // Resets the whole thing, similiar to events.
        raycastResults.Clear();
        
    }
}
