﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// EXPERIMENT - WAIFU GENERATOR
// REQUIRES FUNGUS SYSTEM
public class WaifuGenerator : MonoBehaviour {

    // Since this is a generator, different atributes, materials and so are stored in arrays.
    [Header("Generic Atributes")]
    public string[] names;
    public string[] likes;
    public string[] dislikes;
    [Header("Unity-Dependant Atribute")]
    public Material[] skinTones;
    public GameObject[] hatPrefabs;
    public Material[] hatMaterials;
    public GameObject[] clothPrefabs;
    public Material[] clothMaterials;
    [Header("Basic Generator")]
    public int waifusGen = 5;
    public GameObject waifuTransform;
    public GameObject basicWaifu;
    // Basic Waifu should include a WaifuInfo script.
	
	// Generate Waifus is a function that should be called by the button
	public void GenerateWaifus () {
        // Checks if there are already waifus generated.
        if(waifuTransform.transform.childCount != 0)
        {
           // If there are, deletes all the current waifus.
           foreach(Transform child in waifuTransform.transform)
            {
                Destroy(child.gameObject);
            }
        }
	    for(int i = 0; i < waifusGen; i++)
        {
            // Spawns the waifu and generates objects based on it.
            GameObject waifuObj = (GameObject)Instantiate(basicWaifu, new Vector3(-7 - i * 2, 0.5f, -5), Quaternion.identity);
            waifuObj.transform.SetParent(waifuTransform.transform);
            AtributeWaifu(waifuObj);
        }
	}

    // Gives atributes to the Waifu.
    void AtributeWaifu(GameObject waifuObj)
    {
        // Gives info to the the recently-created waifu.
        WaifuInfo waifuInfo = waifuObj.GetComponent<WaifuInfo>();
        waifuInfo.waifuName = names[Random.Range(0, names.Length)];
        waifuInfo.like = likes[Random.Range(0, likes.Length)];
        waifuInfo.dislike = dislikes[Random.Range(0, dislikes.Length)];
        waifuInfo.skinTone = skinTones[Random.Range(0, skinTones.Length)];
        waifuInfo.hatPrefab = hatPrefabs[Random.Range(0, hatPrefabs.Length)];
        waifuInfo.hatMaterial = hatMaterials[Random.Range(0, hatMaterials.Length)];
        waifuInfo.clothPrefab = clothPrefabs[Random.Range(0, clothPrefabs.Length)];
        waifuInfo.clothMaterial = clothMaterials[Random.Range(0, clothMaterials.Length)];
    }
}
