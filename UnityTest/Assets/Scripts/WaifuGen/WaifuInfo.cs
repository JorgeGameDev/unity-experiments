﻿using UnityEngine;
using Fungus;
using System.Collections;

// EXPERIMENT - WAIFU GENERATOR
public class WaifuInfo : MonoBehaviour {

    // Information receive that make this waifu unique.
    [Header("Generic Atributes")]
    public string waifuName;
    public string like;
    public string dislike;
    [Header("Unity-Dependant Atribute")]
    public Material skinTone;
    public GameObject hatPrefab;
    public Material hatMaterial;
    public GameObject clothPrefab;
    public Material clothMaterial;

    // Use this for initialization
    void Start () {
        // Loads Prefabs and Materials.
        GetComponentInChildren<MeshRenderer>().material = skinTone;
        // Instanciates Clothes and Hair/Hat
        GameObject hat = (GameObject)Instantiate(hatPrefab, transform.position, Quaternion.identity);
        hat.GetComponent<MeshRenderer>().material = hatMaterial;
        hat.transform.SetParent(transform);
        GameObject cloth = (GameObject)Instantiate(clothPrefab, transform.position, Quaternion.identity);
        cloth.GetComponent<MeshRenderer>().material = clothMaterial;
        cloth.transform.SetParent(transform);
        // Runs Fungus Setup
        FungusSetup();
    }
	
	// Function used to set up everything related to fungus.
	void FungusSetup () {
        // Sets the fungus info related to conversations.
        Flowchart fungusFlow = GetComponentInChildren<Flowchart>();
        fungusFlow.SetStringVariable("name", waifuName);
        fungusFlow.SetStringVariable("like", like);
        fungusFlow.SetStringVariable("dislike", dislike);
        // Disabled Courtines and Flowchart.
        fungusFlow.StopAllCoroutines();
        fungusFlow.StopAllBlocks();
        fungusFlow.gameObject.SetActive(false);
        // Sets up charachter;
        Character fungusChar = GetComponentInChildren<Character>();
        fungusChar.nameText = waifuName;
        fungusChar.nameColor = new Color(66, 65, 67, 255);
    }
}
