﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// EXPERIMENT - THIS CLASS MANGAGES THE MUSIC PLAYER
public class MusicManager : MonoBehaviour {

    // Relevant components.
    [Header("Audio Proprities")]
    public AudioClip audioClip;
    public Button referenceButton;
    private AudioSource _audioSource; 
    private bool _isPlaying = false;

    // Use this for initialization
    void Start () {
        // Gets components.
        _audioSource = GetComponent<AudioSource>();
	}
	
	// This function is called by the button to turn off or on the Audio Source.
	public void ManagePlayer () {
	    // Checks if the music is playing and acts acordingly.
        if(!_isPlaying)
        {
            _audioSource.PlayOneShot(audioClip);
            referenceButton.GetComponentInChildren<Text>().text = "STOP";
            _isPlaying = true;
        }
        else
        {
            _audioSource.Stop();
            referenceButton.GetComponentInChildren<Text>().text = "PLAY";
            _isPlaying = false;
        }
	}
}
