﻿using UnityEngine;
using System.Collections;

// EXPERIMENT - WORLD CANVAS & AUDIO SPECTRUM
public class AudioSpectrum : MonoBehaviour {

    // Variables relevant to this experiment.
    [Header("Audio Source")]
    public AudioSource soundSource;
    public int multiplyFactor;
    [Header("Spectrum Objects")]
    private GameObject[] spectrumObjects; // ArrayList using the Spectrum Objects.

	// Use this for initialization
	void Start () {
        // Gets the objects from the childs.
        spectrumObjects = new GameObject[transform.childCount];
        // Assigns all the objects based on 
        for(int i = 0; i < transform.childCount; i++)
        {
            spectrumObjects[i] = transform.GetChild(i).gameObject;
        }
	}
	
	// Update is called once per frame
	void Update () {
        // Gets the spectrum based on the audio source. 1024 is a power of 2.
        float[] spectrumSamples = new float[1024];
        soundSource.GetSpectrumData(spectrumSamples, 0, FFTWindow.Hamming);
        // For each of the bars, updates the spectum.
        for(int i = 0; i < spectrumObjects.Length; i++)
        {
            // Gets the current spectrum scale.
            RectTransform rectTransform = spectrumObjects[i].GetComponent<RectTransform>();
            Vector2 currentRect = new Vector2(rectTransform.rect.width, rectTransform.rect.height);
            Vector2 newRect = new Vector2(rectTransform.rect.width, spectrumSamples[i] * multiplyFactor);
            // Clamps the rect size.
            if (newRect.y > 146)
                newRect.y = 146;
            // Applies it!
            Vector2 finalValue = Vector2.Lerp(currentRect, newRect, Time.deltaTime * 15);
            spectrumObjects[i].GetComponent<RectTransform>().sizeDelta = new Vector2(finalValue.x, finalValue.y);
        }

	}
}
