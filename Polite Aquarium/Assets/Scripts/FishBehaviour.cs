﻿using UnityEngine;
using System.Collections;

public class FishBehaviour : MonoBehaviour {

    // Fish Behaviour defines the fish behaviour. And no, it's not the size of a fish brain.

    [Header("Fish Status")]
    public int fishType = 0;
    public int timesFeed = 0;
    public int currentLevel = 0;
    public byte speedOperator = 3;
    public GameObject[] currentLevelUnlock;

    [Header("Small Brain Inteligence")]
    private bool _isMoving;
    public Vector3 currentDistination;
	
    // Use this for initialization
    void Start ()
    {
        currentDistination = transform.position;
    }

	// Update is called once per frame
	void Update () {
        FishMovement();
	}

    // This function is used to take care of the fish movement, which is somewhat random, unless there are tea leaves.
    void FishMovement ()
    {
        if(transform.position == currentDistination)
        {
            if(!_isMoving)
            {
                StartCoroutine(CalculatePosition(2f));
            }
        }
        else
        {
            Debug.Log("Move!");
            transform.position = Vector3.MoveTowards(transform.position, currentDistination, speedOperator * Time.deltaTime);
        }
    }

    // This function is used to make timers for Calculating positions. It allows any boolean and time to be given to it.
    IEnumerator CalculatePosition(float time)
    {
        Debug.LogWarning("Courotine was called!");
        // Anounces the thing is moving.
        _isMoving = true;
        // Waits the time that has been passed to the function.
        yield return new WaitForSeconds(time);
        // Calculates the new position.
        currentDistination = new Vector3(Random.Range(GameManager.gameManager.aquariumStart.x, GameManager.gameManager.aquariumEnd.x),
                                         Random.Range(GameManager.gameManager.aquariumStart.y, GameManager.gameManager.aquariumEnd.y),
                                         0);
        // Anounces movement is done.
        _isMoving = false;
        Debug.LogWarning("Courotine as ended!");
    }
}
