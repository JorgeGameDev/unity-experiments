﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    // The Game Manager takes care of the Aquarium.
    [Header("Game Manager")]
    public static GameManager gameManager;
    [Header("Reference")]
    public Vector2 aquariumStart;
    public Vector2 aquariumEnd;
    public GameObject[] fishTypes;
    [Header("Fishes")]
    public byte fishCount;
    public List<GameObject> fishes;

	// Use this for initialization
	void Awake () {
        gameManager = this;
	}

    // Update is called every frame
    void Update ()
    {
        // TODO: Get a less shitty way to do this.
        GameObject[] fishesFound = GameObject.FindGameObjectsWithTag("Fish");
        if(fishesFound.Length != fishCount)
        {
            fishes.Clear();
            foreach(GameObject fish in fishesFound)
            {
                fishes.Add(fish);
            }
        }
    }

    // This function is used to load the aquarium WITH FISH.
    public void LoadFish(int numberOfFishes, int[] typeFish, int[] timesFeed, int[] currentLevel)
    {
        Debug.LogWarning(numberOfFishes + " fishes were loaded!");
        // Creates a new fish based on the number of fishes.
        for (int i = 0; i < numberOfFishes; i++)
        {
            
            // Creates a random position for the fish to spawn in.
            float randomX = Random.Range(aquariumStart.x, aquariumEnd.x);
            float randomY = Random.Range(aquariumStart.y, aquariumEnd.y);
            Vector2 randomPos = new Vector2(randomX, randomY);
            GameObject newFish = (GameObject)Instantiate(fishTypes[typeFish[i]], randomPos, Quaternion.identity);
            // Gives parameters to the newly created fish.
            FishBehaviour fishBehaviour = newFish.GetComponent<FishBehaviour>();
            fishBehaviour.fishType = typeFish[i];
            fishBehaviour.timesFeed = timesFeed[i];
            fishBehaviour.currentLevel = currentLevel[i];
            // Adds it to the list.
            fishes.Add(newFish);
        }
    }
}
