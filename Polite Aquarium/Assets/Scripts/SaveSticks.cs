﻿using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public class SaveSticks : MonoBehaviour {

    // SaveSticks takes care of saving and loading fish.

	// Use this for early initialization
	void Start () {
        Load();
	}

    // Update is called every frame.
    void Update()
    {
        Save();
    }
	
	// This function takes care of saving by creating files.
	void Save () {
        // Creates the necessary connections to save files.
        BinaryFormatter binaryF = new BinaryFormatter();
        FileStream fileS = File.Open(Application.persistentDataPath + Path.DirectorySeparatorChar + "Aquarium.dat", FileMode.OpenOrCreate);
        Aquarium aquarium = new Aquarium();
        // Creates the arrays required to save the files based on the number of fishes.
        int numberOfFishes = GameManager.gameManager.fishes.Count;
        int[] typeFish = new int[numberOfFishes];
        int[] timesFeed = new int[numberOfFishes];
        int[] currentLevel = new int[numberOfFishes];
        // Files each array to be given to the save file.
        for(int i = 0; i < numberOfFishes; i++)
        {
            FishBehaviour fishBehavour = GameManager.gameManager.fishes[i].GetComponent<FishBehaviour>();
            typeFish[i] = fishBehavour.fishType;
            timesFeed[i] = fishBehavour.timesFeed;
            currentLevel[i] = fishBehavour.currentLevel;
        }
        // Now it finally saves!
        aquarium.numberOfFishes = numberOfFishes;
        aquarium.typeFish = typeFish;
        aquarium.timesFeed = timesFeed;
        aquarium.currentLevel = currentLevel;
        binaryF.Serialize(fileS, aquarium);
        fileS.Close();
    }

    // This function is used to load the game!
    void Load()
    {
        if(File.Exists(Application.persistentDataPath + Path.DirectorySeparatorChar + "Aquarium.dat"))
        {
            // Creates the necessary connections to load files.
            BinaryFormatter binaryF = new BinaryFormatter();
            FileStream fileS = File.Open(Application.persistentDataPath + Path.DirectorySeparatorChar + "Aquarium.dat", FileMode.Open);
            Aquarium aquarium = (Aquarium)binaryF.Deserialize(fileS);
            fileS.Close();
            // Creates arrays holding everything related to the fishes loaded.
            int numberOfFishes = aquarium.numberOfFishes;
            int[] typeFish = new int[numberOfFishes];
            int[] timesFeed = new int[numberOfFishes];
            int[] currentLevel = new int[numberOfFishes];
            typeFish = aquarium.typeFish;
            timesFeed = aquarium.timesFeed;
            currentLevel = aquarium.timesFeed;
            // Tells the Game Manager to create the fishes from the files loaded.
            GameManager.gameManager.LoadFish(numberOfFishes, typeFish, timesFeed, currentLevel);
        }
    }
}

// The aquarium is the save file, and it's saved to Appdata.
[Serializable]
public class Aquarium
{
    // This class saves the fishes, and gives the atributes to each fish when loaded.
    public int numberOfFishes;
    public int[] typeFish;
    public int[] timesFeed;
    public int[] currentLevel;
}
