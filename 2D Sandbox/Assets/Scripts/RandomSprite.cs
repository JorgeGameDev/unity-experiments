﻿using UnityEngine;
using System.Collections;

public class RandomSprite : MonoBehaviour {

    // This script picks a sprite from a random list of sprites and changes it when the object
    // is created.
    [Header("Sprites")]
    public Sprite[] sprites;

	// Use this for initialization
	void Start () {
        // Picks a random variable and gives the sprite renderer that sprite.
        int randomValue = Random.Range(0, sprites.Length);
        GetComponent<SpriteRenderer>().sprite = sprites[randomValue];
	}
}
