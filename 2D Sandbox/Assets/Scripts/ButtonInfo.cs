﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonInfo : MonoBehaviour {

    // This script is used to generate UI buttons that hold the selectable objects.
    [Header("Object Info")]
    public Sprite objectImage;
    public SpawnableObject objectSObject;

	// Use this for initialization
	void Start () {
        // Sets the image in the button to the generated object.
        transform.GetChild(0).GetComponent<Image>().sprite = objectImage;
	}
	
	// This function is called by the button to set the selected button to the picked button.
	public void PickObject () {
        GameController.gameController.selectedObject = objectSObject;
	}
}
