﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
[System.Serializable]
public class SpawnableObject : ScriptableObject {

    // This scriptable object contains information regarding spawned objects.
    // Basically, they contain the name and prefab of the selected object.
    [Header("Object Info")]
    public string objectName;
    public Sprite objectIcon;
    public GameObject objectFab;

}
