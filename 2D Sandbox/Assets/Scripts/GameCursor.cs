﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameCursor : MonoBehaviour {

    // This script takes care of all the functions and code related to the Game Cursor.
    // It manages spawning and selecting objects.
    [Header("General Info")]
    public Image uiCursor;
    private Vector2 _cameraPoint;
    private float _zRotation; 
    private float _rotationValue;

    [Header("Spawning Info")]
    public bool spawningMode;
    public GameObject spawningText;
    public GameObject selectingText;

    [Header("Selecting Info")]
    public GameObject selectedObject;
    private RaycastHit2D _objectRay;
	
	// Update is called once per frame
	void Update () {
        // Gets the camera point based on the cursor position.
        _cameraPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _objectRay = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        // Calls functions holding other functions.
        UpdateCursor();
        SpawnObjects();
        RayCastObjects();

    }

    // This function is used to update the Cursor and related content.
    void UpdateCursor()
    {
        // Updates the rotation based on the scroll wheel.
        float scrollWheelValue = Input.GetAxis("Mouse ScrollWheel");

        // Changes the rotation depending on the scrollwheel values.
        if(scrollWheelValue > 0)
        {
            _zRotation += 15;
        }
        else if(scrollWheelValue < 0)
        {
            _zRotation -= 15;
        }

        // Places the cursor in the cameraPoint.
        Cursor.visible = false;
        uiCursor.transform.position = Input.mousePosition;
        uiCursor.transform.rotation = Quaternion.Euler(0, 0, _zRotation);
    }

    // This function is used for mostly spawning and controling objects.
    void SpawnObjects()
    {
        // Checks if the spawning mode is turned on.
        if (spawningMode)
        {
            if (Input.GetButtonUp("Spawn"))
            {
                // Spawns an object in the screen point.
                GameObject createdObj = (GameObject)Instantiate(GameController.gameController.selectedObject.objectFab, _cameraPoint, Quaternion.Euler(0, 0, _zRotation));
                // Stores the object in a quick acessible list.
                GameController.gameController.spawnedObjectsList.Add(createdObj);

            }
        }
    }

    // This function is used mostly for raycasting objects and selecting them.
    void RayCastObjects()
    {
        // Checks if the raycast hit an object.
        if(_objectRay.collider != null && _objectRay.collider.gameObject.CompareTag("Object"))
        {
            if(Input.GetButtonUp("Interact"))
            {
                // Selects the raycasted object.
                spawningMode = false;
                spawningText.SetActive(false);
                selectingText.SetActive(true);
                selectedObject = _objectRay.collider.gameObject;
                selectedObject.GetComponent<SpriteRenderer>().color = Color.cyan;
            }
        }
        
        // Checks if there's an object selected. Moves the object to the player cursor meanwhile.
        if(selectedObject != null)
        {
            // Changes the object position and rotation.
            selectedObject.transform.position = _cameraPoint;
            selectedObject.transform.rotation = Quaternion.Euler(0, 0, _zRotation);
            // Stops the object completly in case the object has a Rigidbody.
            if (selectedObject.GetComponent<Rigidbody2D>())
            {
                selectedObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            }

            // Releases the object if the mouse button is selected.
            if (Input.GetButtonUp("Spawn"))
            {
                // Deselects the object.
                spawningMode = true;
                selectedObject.GetComponent<SpriteRenderer>().color = Color.white;
                selectedObject = null;
                spawningText.SetActive(true);
                selectingText.SetActive(false);
            }

            // Disables the rigidbody in case the sepcial key is pressed.
            if (Input.GetButtonUp("Special") && selectedObject.GetComponent<Rigidbody2D>())
            {
                // Creates a temporary rigidbody variables.
                Rigidbody2D toggleRigid = selectedObject.GetComponent<Rigidbody2D>();
                // Toggles the rigidbody of the object depending on it's current state.
                if (toggleRigid.isKinematic == true)
                    toggleRigid.isKinematic = false;
                else if (toggleRigid.isKinematic == false)
                    toggleRigid.isKinematic = true;
            }

            // Removes the Object in case the remove key is pressed.
            if (Input.GetButtonUp("Remove"))
            {
                // Destroys the object.
                Destroy(selectedObject);
                selectedObject = null;
                spawningMode = true;
                spawningText.SetActive(true);
                selectingText.SetActive(false);
            }
            // Duplicates the object if the duplicate key is pressed.
            if (Input.GetButtonUp("Duplicate"))
            {
                // Instanciates the current object again and again and again and again.
                GameObject newInstance = (GameObject)Instantiate(selectedObject, _cameraPoint, Quaternion.Euler(0, 0, _zRotation));
                // Sets the object to a defaut state so it's totally equal to the original one.
                newInstance.GetComponent<SpriteRenderer>().color = Color.white;
                if (newInstance.GetComponent<RandomSprite>())
                {
                    newInstance.GetComponent<RandomSprite>().enabled = false;
                }
                if (newInstance.GetComponent<RandomScale>())
                {
                    newInstance.GetComponent<RandomScale>().enabled = false;
                }
            }
        }
    }
}
