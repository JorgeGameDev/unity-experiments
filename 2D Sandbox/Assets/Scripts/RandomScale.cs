﻿using UnityEngine;
using System.Collections;

public class RandomScale : MonoBehaviour {

    // This script allows objects to spawn with random scales.
    [Header("Scales")]
    public float maxScale;
    public float minScale;

	// Use this for initialization
	void Start () {
        // Picks a scale and gives it to the object.
        float finalScale = Random.Range(minScale, maxScale);
        // Creates a new vector 3 holding the final scale.
        Vector3 finalVectorScale = new Vector3(finalScale, finalScale, finalScale);
        transform.localScale = finalVectorScale;
	}
}
