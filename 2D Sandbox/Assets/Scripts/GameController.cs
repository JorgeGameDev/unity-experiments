﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    // The Game Master takes care of managing the selected objects and updating them to the UI.
    [Header("Game Controller")]
    public static GameController gameController;

    [Header("Object Information")]
    public SpawnableObject selectedObject;
    public List<GameObject> spawnedObjectsList;
    public Text selectedText;

    [Header("Spawnable Objects")]
    public Transform spawnToolbar;
    public Button spawnButtonUI;
    public SpawnableObject[] spawnableObjectsList;

    // Use this for initialization
    void Start () {
        // Sets this object as the Game Controller and selects a default object.
        gameController = this;
        selectedObject = spawnableObjectsList[0];
        // Creates the UI buttons.
        createUI();
	}
	
	// Update is called once per frame
	void Update () {
        UpdateUI();
        AlphaChoose();
    }

    // This function takes care of creating the UI buttons based on the number of objects.
    void createUI()
    {
        // For each element in the list creates an object.
        foreach(SpawnableObject spawnObj in spawnableObjectsList)
        {
            // Sets the button up.
            Button spawnButton = (Button)Instantiate(spawnButtonUI, spawnToolbar.transform.position, Quaternion.identity);
            spawnButton.transform.SetParent(spawnToolbar);
            spawnButton.GetComponent<ButtonInfo>().objectImage = spawnObj.objectIcon;
            spawnButton.GetComponent<ButtonInfo>().objectSObject = spawnObj;
        }
    }

    // This function takes care of UI Updates.
    void UpdateUI()
    {
        // Updates the selected text based on the selected object.
        selectedText.text = selectedObject.objectName;
    }

    // This function allows items to be selected with Alpha numbers (until there are more than 9 items)
    void AlphaChoose()
    {
        // The alpha keys are the top numberic keys.
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            selectedObject = spawnableObjectsList[0];
        }
        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            selectedObject = spawnableObjectsList[1];
        }
        if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            selectedObject = spawnableObjectsList[2];
        }
        if (Input.GetKeyUp(KeyCode.Alpha4))
        {
            selectedObject = spawnableObjectsList[3];
        }
        if (Input.GetKeyUp(KeyCode.Alpha5))
        {
            selectedObject = spawnableObjectsList[4];
        }
    }

    // This function is used to clean the whole scene by deleating all the objects in the list.
    public void ClearScene()
    {
        foreach(GameObject go in spawnedObjectsList)
        {
            // Destroys the objects.
            Destroy(go);
        }
    }
}
