﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Transform))]
public class TransformExtend : Editor {

	// Use this for initialization
	public override void OnInspectorGUI()
    {
        // Gets the target transform.
        Transform gameObject = (Transform)target;

        // Position recreation of the normal transform editor.
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Position", GUILayout.Width(70)))
        {
            gameObject.position = Vector3.zero;
        }

        gameObject.position = EditorGUILayout.Vector3Field("", gameObject.position);
        EditorGUILayout.EndHorizontal();

        // Rotation recreation of the normal transform editor.
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Rotation", GUILayout.Width(70)))
        {
            gameObject.rotation = Quaternion.Euler(Vector3.zero);
        }
        gameObject.rotation = Quaternion.Euler(EditorGUILayout.Vector3Field("", gameObject.rotation.eulerAngles));
        EditorGUILayout.EndVertical();

        // Scale recreation of the normal transform editor.
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Scale", GUILayout.Width(70)))
        {
            gameObject.localScale = Vector3.one;
        }
        gameObject.localScale = EditorGUILayout.Vector3Field("", gameObject.localScale);
        EditorGUILayout.EndHorizontal();

        // Aditional functionality to round editor values (rounding odd numbers).
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Round Values"))
        {
            gameObject.position = new Vector3(
                Mathf.Round(gameObject.position.x), 
                Mathf.Round(gameObject.position.y), 
                Mathf.Round(gameObject.position.z));
            gameObject.rotation = Quaternion.Euler(new Vector3(
                Mathf.Round(gameObject.rotation.eulerAngles.x), 
                Mathf.Round(gameObject.rotation.eulerAngles.y), 
                Mathf.Round(gameObject.rotation.eulerAngles.z)));
            gameObject.localScale = new Vector3(
                Mathf.Round(gameObject.localScale.x), 
                Mathf.Round(gameObject.localScale.y), 
                Mathf.Round(gameObject.localScale.z));
        }
        if (GUILayout.Button("1 Decimal"))
        {
            gameObject.position = new Vector3(
                Mathf.Round(gameObject.position.x * 10f) / 10f, 
                Mathf.Round(gameObject.position.y * 10f) / 10f, 
                Mathf.Round(gameObject.position.z * 10f) / 10f);
            gameObject.rotation = Quaternion.Euler(new Vector3(
                Mathf.Round(gameObject.rotation.eulerAngles.x * 10f) / 10f,
                Mathf.Round(gameObject.rotation.eulerAngles.y * 10f) / 10f,
                Mathf.Round(gameObject.rotation.eulerAngles.z * 10f) / 10f));
            gameObject.localScale = new Vector3(
                Mathf.Round(gameObject.localScale.x * 10f) / 10f, 
                Mathf.Round(gameObject.localScale.y * 10f) / 10f, 
                Mathf.Round(gameObject.localScale.z * 10f) / 10f);
        }
        if (GUILayout.Button("2 Decimal"))
        {
            gameObject.position = new Vector3(
                Mathf.Round(gameObject.position.x * 100f) / 100f, 
                Mathf.Round(gameObject.position.y * 100f) / 100f, 
                Mathf.Round(gameObject.position.z * 100f) / 100f);
            gameObject.rotation = Quaternion.Euler(new Vector3(
                Mathf.Round(gameObject.rotation.eulerAngles.x * 100f) / 100f,
                Mathf.Round(gameObject.rotation.eulerAngles.y * 100f) / 100f,
                Mathf.Round(gameObject.rotation.eulerAngles.z * 100f) / 100f));
            gameObject.localScale = new Vector3(
                Mathf.Round(gameObject.localScale.x * 100f) / 100f, 
                Mathf.Round(gameObject.localScale.y * 100f) / 100f, 
                Mathf.Round(gameObject.localScale.z * 100f) / 100f);
        }
        EditorGUILayout.EndHorizontal();

        this.Repaint();
    }
}
