﻿using UnityEngine;
using System.Collections;

public class PelletForce : MonoBehaviour {

    // This script is used to add forces to the pellets and check collisions of course.

    [Header("Pellet Force")]
    [HideInInspector] public float pelletForce;
    public string pelletString;
    private Rigidbody _rigidBody;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, 4f);
        _rigidBody = GetComponent<Rigidbody>();
        _rigidBody.velocity = new Vector3(-pelletForce, 0, 0);
    }
}
