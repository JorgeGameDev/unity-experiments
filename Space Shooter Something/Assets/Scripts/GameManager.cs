﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {

    // This script is used to manage thing such as the UI.
    [Header("Self-Reference")]
    public static GameManager gameManager;

	[Header("Game State")]
	[HideInInspector]public bool gameEnded;

    [Header("Score Points")]
    public int scorePoints;
    public Text scoreText;

	[Header("Game Over Panel")]
	public GameObject gameOverPanel;
	public Text finalScore;

	// Use this for initialization
	void Start () {
        gameManager = this;
	}
	
	// Update is called once per frame
	void Update () {
        scoreText.text = scorePoints.ToString();
	}

	// This function is used when the player loses the game.
	public void ShowScore () {
		Debug.LogWarning ("Player Ship Destroyed!");
		gameEnded = true;
		gameOverPanel.SetActive (true);
		finalScore.text = scoreText.text;
	}

	// This function is called o restart the game.
	public void RestartGame () {
		// Reloads Scene.
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
