﻿using UnityEngine;
using System.Collections;

public class FireScript : MonoBehaviour {

    // This script is used to take care of the ship firing the bullets.
    [Header("Global Settings")]
    public bool isPlayer = false;
    private bool _isCooldown = false;

    [Header("CoolDown")]
    private GameObject _playerObject;
    public float coolDown;

    [Header("Pellets")]
    private AudioSource _audioSource;
    public GameObject firePellet;
    public Transform firePoint;
    public float pelletForce;

    // Use this for initialization
    void Start()
    {
        // Gets the playerObject;
        _playerObject = GameObject.FindGameObjectWithTag("Player");
        _audioSource = GetComponent<AudioSource>();
        if (!isPlayer)
        {
            _isCooldown = true;
            StartCoroutine(WaitTime());
        }
    }

    // Update is called once per frame
    void Update () {
	    // Checks if the player is 
        if(!_isCooldown)
        {
            // Does a different tree depending on the player. 
            if(isPlayer)
            {
                // Compiles this code if the target platform is the editor.
#if UNITY_STANDALONE || UNITY_EDITOR
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    SpawnPellet(pelletForce, "Player");
                }
#endif
                // Compiles this code if the target platform is Android.
#if UNITY_ANDROID
                // Gets the touches made by the user.
                Touch[] touchesMade = Input.touches;

                if(touchesMade.Length > 0)
                {
                    SpawnPellet(pelletForce, "Player");
                }
#endif
            }
            else
            {
                if(_playerObject != null)
                {
                    SpawnPellet(pelletForce, "Enemy");
                }
            }
        }
    }

    // This function is used to create the pellet, since it's called multiple times.
    void SpawnPellet (float pelletForce, string pelletString)
    {
        // Instanciates the bullet.
        _audioSource.Play();
        GameObject newPellet = (GameObject)Instantiate(firePellet, firePoint.position, Quaternion.identity);
        newPellet.GetComponent<PelletForce>().pelletForce = pelletForce;
        newPellet.GetComponent<PelletForce>().pelletString = pelletString;
        StartCoroutine(WaitTime());
    }

    // This function is used to wait between cooldowns.
    IEnumerator WaitTime()
    {
        // Confirms it's in cooldown, and removes it at the end of the script.
        _isCooldown = true;
        // Waits for seconds before firing the bullets.
        yield return new WaitForSeconds(coolDown);
        _isCooldown = false;
    }
}
