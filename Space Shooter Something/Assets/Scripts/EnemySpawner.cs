﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

    // This script is used to create enemies to make the game fun!
    [Header("Enemy Spawning")]
    public GameObject enemyObj;
    public float spawnX;
    public Vector2 lineSpawn;
    public float coolDown;
    private bool _isCooldown;

    // Use this for initialization
    void Start()
    {
#if UNITY_STANDALONE
        lineSpawn.x = -10;
        lineSpawn.y = 10;
#endif
#if UNITY_ANDROID
        lineSpawn.x = -5;
        lineSpawn.y = 5;
#endif
    }

    // Update is called once per frame
    void Update () {
	    if(!_isCooldown)
        {
            // Creates a random position.
            float newZ = Random.Range(lineSpawn.x, lineSpawn.y);
            Vector3 spawnPosition = new Vector3(spawnX, 0, newZ);
            GameObject enemySpawned = (GameObject)Instantiate(enemyObj, spawnPosition, Quaternion.identity);
            enemySpawned.GetComponent<EnemyMovement>().speed = Random.Range(2, 6);
            StartCoroutine(WaitTime());
        }
	}

    // This function is used to wait between cooldowns.
    IEnumerator WaitTime()
    {
        // Confirms it's in cooldown, and removes it at the end of the script.
        _isCooldown = true;
        // Waits for seconds before firing the bullets.
        yield return new WaitForSeconds(coolDown);
        _isCooldown = false;
    }
}
