﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

    // This script is used to move the enemy spaceships on their own.
    [Header("Movement Info")]
    private GameObject _playerObject;
    public float speed;


	// Use this for initialization
	void Start () {
        // Gets the playerObject;
        _playerObject = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if(_playerObject != null)
        {
            // Updates the translate position based on player position and Y movement.
            float playerPositionZ = _playerObject.transform.position.z;
            float translationZ = transform.position.z - playerPositionZ;
            Vector3 newTranslation = new Vector3(speed, 0, -translationZ);
            // Applies the transform.
            transform.Translate(newTranslation * Time.deltaTime);
        }
    }

    // This script is used to check collisions with other objects.
    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Collision Triggered");
        // Checks who the bullet collided with.
        if (collider.gameObject.CompareTag("Pellet") && collider.gameObject.GetComponent<PelletForce>().pelletString == "Player")
        {
            GameManager.gameManager.scorePoints++;
            Destroy(collider.gameObject);
            Destroy(gameObject);
        }
    }
}
