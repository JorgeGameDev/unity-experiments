﻿using UnityEngine;
using System.Collections;

public class SpaceShipMove : MonoBehaviour {

    // This script takes care of moving the spaceship.
    [Header("Movement")]
    public float speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // Gets the input axis from the controler.
#if UNITY_STANDALONE
        float horInput = Input.GetAxis("Horizontal");

        if(horInput != 0)
        {
            // Creates a new vector 3 for the translation.
            Vector3 moveInput = new Vector3(0, 0, horInput * speed);
            transform.Translate(moveInput * Time.deltaTime);
            // TODO: More efficient way to do this. Clamps the position of the ship.
			Vector3 getVector = transform.position;
			if(getVector.z > 10)
			{
				getVector.z = 10;
				transform.position = getVector;
			}
			else if(getVector.z < -10)
			{
				getVector.z = -10;
				transform.position = getVector;
			}
        }

#endif
#if UNITY_ANDROID
        Vector3 deviceAcceleration = Input.acceleration;

        if(deviceAcceleration.x != 0)
        {
            // Creates a new vector 3 for the translation.
            Vector3 moveInput = new Vector3(0, 0, deviceAcceleration.x * speed);
            transform.Translate(moveInput * Time.deltaTime);
		// TODO: More efficient way to do this. Clamps the position of the ship.
		Vector3 getVector = transform.position;
			if(getVector.z > 5)
			{
				getVector.z = 5;
				transform.position = getVector;
			}
			else if(getVector.z < -5)
			{
				getVector.z = -5;
				transform.position = getVector;
			}
        }
#endif
    }

    // This function is used to check collisions with other objects.
    void OnTriggerEnter(Collider collider)
    {
        // Checks who the bullet collided with.
        if (collider.gameObject.CompareTag("Pellet") && collider.gameObject.GetComponent<PelletForce>().pelletString == "Enemy")
        {
            Destroy(collider.gameObject);
            Destroy(gameObject);
        }
    }
		
	// This function is called in the last frame the object is destroyed.
	void OnDisable () {
		// Calls the Game Manager to show final score.
		GameManager.gameManager.ShowScore();
	}
}
