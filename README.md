# UNITY EXPERIMENTS #

These are several, quickly prototyped experiments made with Unity. These are highly unfinished as they were always meant to be prototypes only in order to learn something new or experiment with various mechanics.

As such the code in the various projects here may not be in their ideal form as some of them were quickly made as a proof of concept, rather than targeted as an actual game.