// Includes Extenral Libraries, including Socket.IO
var io = require("socket.io")(process.env.PORT || 27030);
var shortId = require("shortid");

// Announces the server has started.
console.log("[SERVER] Server Started!");

// Server Variables, available to all players.
var netPlayers = [];
var playerSpeed = 3.5;

// Runs when the player connects to the sever.
io.on("connection", function (socket) {

    // Creates new client ID for the newly connected client.
    var thisPlayerId = shortId.generate();
    var choosenSkin = Math.round(Math.random() * 4) + 0;
    console.log("[SPAWNING] Client", thisPlayerId, "Connected with Skin", choosenSkin, "!");

    // Creates a player object that will be stored on the server.
    var player = {
        id: thisPlayerId, 
        skin: choosenSkin,
        destination: {
            x: 0, 
            y: 0, 
            z: 0
        },
        lastPosition: {
            x: 0,
            y: 0,
            z: 0
        },

        lastMoveTime: 0
    }
    
    // Adds the new client to the player list.
    netPlayers[thisPlayerId] = player;
    lastMoveTime = Date.now();

    // Spawns the new client.
    io.emit("spawn", {
        id: thisPlayerId,
        skin: choosenSkin
    });

    // Requests the position of the already connected clients.
    socket.broadcast.emit("requestPosition");

    // Sends all the existing players the newly  connected client.
    for (var playerId in netPlayers) {
        // Excluding the client it self.
        if (playerId == thisPlayerId) {
            continue;
        }
        
        socket.emit("spawn", {
            id: netPlayers[playerId].id,
            skin: netPlayers[playerId].skin
        });

        console.log("[SPAWNING] Spawning New", playerId, "Player!");
    };

    // Checks if the player has moved.
    socket.on('move', function (data) {
        // Transforms the received data into JSON.
        data.id = thisPlayerId;

        // Assigns the position to the player object.
        player.destination = data.destination;

        // Calculates the distance between the current position and the player's destination.
        console.log("[AUTHORITY] Distance between player's position and destination:", distance(data.current, data.destination));

        // Calculates the elapsed time between move actions. 
        var elapsedTime = Date.now() - player.lastMoveTime;
        elapsedTime /= 1000;
        var travelDistanceLimit = elapsedTime * playerSpeed;
        var requestedDistance = distance(player.lastPosition, data.current)
        console.log("[AUTHORITY] The player can travel", travelDistanceLimit, "and traveled", requestedDistance);

        if (requestedDistance > travelDistanceLimit)
        {
            console.log("[AUTHORITY] [WARN] Invalid user travel request! Distance too high!")
            return;
        }

        player.lastMoveTime = Date.now();

        // Removes the current position of the broadcasting player since other clients don't need to know it.
        player.lastPosition = data.current;
        delete data.current;
        data.x = data.destination.x;
        data.y = data.destination.y;
        data.z = data.destination.z;
        console.log("[MOVEMENT] A Client has", thisPlayerId, "Moved! (", player.destination.x, ",", player.destination.y, ",", player.destination.z, ")");

        // Sends the move action over to the other clients.
        socket.broadcast.emit("move", data);
    });

    // Tells this client to update the position of existing players.
    socket.on('updatePosition', function (data) {
        console.log("[MOVEMENT] Updating Position of Existing Players for New Client!");
        data.id = thisPlayerId;
        socket.broadcast.emit("updatePosition", data);
    });
    
    // Tells this client has started following another clients.
    socket.on('follow', function (data) {
        console.log("[ACTION] Player", thisPlayerId, "started following player!", data);
        data.id = thisPlayerId;
        socket.broadcast.emit("follow", data);
    });
    
    // Tells this client has attacked another clients.
    socket.on('attack', function (data) {
        console.log("[ACTION] Player", thisPlayerId, "has atacked another player!", data);
        data.id = thisPlayerId;
        io.emit("attack", data);
    });
    
    // Tells this client has sent a message.
    socket.on('sendChat', function (data) {
        // Recieves the message and sends it back to the other clients.
        console.log("[CHAT] Recived message ", data);
        data.id = thisPlayerId;
        socket.broadcast.emit("receiveChat", data);
    });
    
    // Disconnects the client.
    socket.on('disconnect', function (data) {
        console.log("[DISCONNECT] Client", thisPlayerId, "Disconnected!");

        // Removes the disconnected client from the player list.
        delete netPlayers[thisPlayerId];

        // Tells the client that this client has disconnected.
        socket.broadcast.emit("disconnected", {
            id: thisPlayerId
        });

        // Forces socket to disconnect.
        socket.disconnect(0);
    });
});

// Calculates the distance between two vectors using good old pythagoras.
function distance(vector1, vector2)
{
    // Variables that will hold the distance of the two vector points.
    var deltaX;
    var deltaY;

    // Calculates the correspondening values.
    deltaX = vector2.x - vector1.x;
    deltaY = vector2.y - vector1.y;
    deltaX *= deltaX;
    deltaY *= deltaY;

    // Returns the square root.
    return Math.sqrt(deltaX + deltaY);
}