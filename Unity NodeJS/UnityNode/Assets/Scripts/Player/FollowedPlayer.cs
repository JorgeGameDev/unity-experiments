﻿using UnityEngine;
using System.Collections;

public class FollowedPlayer : MonoBehaviour, IClickable {

    [Header("Player Following")]
    [ReadOnly]
    public GameObject playerFollowing;
    [ReadOnly]
    private NetworkIdentity _networkIdentity;
    private TargetingPlayer _targetingPlayer;

    // Use this for initialization.
    void Start()
    {
        _networkIdentity = GetComponent<NetworkIdentity>();
        _targetingPlayer = playerFollowing.GetComponent<TargetingPlayer>();
    }

    // Used to follow another player.
    public void OnClick(RaycastHit clickedHit, int mouseButton)
    {
        if(mouseButton == 1)
        {
            Debug.Log("<b>[ACTION]</b> Following " + clickedHit.collider.name);
            Network.Follow(_networkIdentity.id);
            _targetingPlayer.target = transform;
        }
    }
}
