﻿using UnityEngine;
using System.Collections;

public class MouseControl : MonoBehaviour {

    // Variables important to the player action.
    private Ray _clickRay;
    private RaycastHit _clickRayHit;

	// Update is called once per frame
	void Update () {
	    if(Input.GetButtonUp("Fire1"))
        {
            PlayerClick(1);
        }
        else if (Input.GetButtonUp("Fire2"))
        {
            // Authority Test
            Network.Move(new Vector3(500, 0, 500), new Vector3(200, 0, 200));
        }
    }

    // Function used to check if the player has clicked something.
    void PlayerClick(int mouseButton)
    {
        // Stores the raycast and checks it.
        _clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Checks if the raycast has hit anything.
        if(Physics.Raycast(_clickRay, out _clickRayHit, Camera.main.farClipPlane))
        {
            IClickable clickableObject = _clickRayHit.collider.gameObject.GetComponent<IClickable>();
            if(clickableObject != null)
            { 
                clickableObject.OnClick(_clickRayHit, mouseButton);
            }
        }
    }
}
