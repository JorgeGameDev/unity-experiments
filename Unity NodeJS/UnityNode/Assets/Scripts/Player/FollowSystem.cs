﻿using UnityEngine;
using System.Collections;
using System;

public class FollowSystem : MonoBehaviour {

    [Header("Follow Variables")]
    private TargetingPlayer _targetingPlayer;
    private NavMeshAgent _navAgent;

    [Header("Following Properties")]
    public float followFrequency = 0.5f;
    private WaitForSeconds _waitFrequency;
    public float stopFollowDistance = 4;
    private bool _hasFollowed = false;

	// Use this for initialization
	void Start () {
        _navAgent = GetComponent<NavMeshAgent>();
        _targetingPlayer = GetComponent<TargetingPlayer>();
        _waitFrequency = new WaitForSeconds(followFrequency);
    }
	
	// Update is called once per frame
	void Update ()
    { 
        // Checks if the target is dead.
        if(isFollowCheck())
        {
            // Checks if the target is dead.
            if (_targetingPlayer.target.GetComponent<Hittable>().IsDead)
            {
                _targetingPlayer.StopTarget();
                return;
            }

            // If not, lets continue!
            if (!_targetingPlayer.IsInRange(stopFollowDistance) && !_targetingPlayer.target.GetComponent<Hittable>().IsDead)
            {
                // Draws the target sprite.
                _targetingPlayer.DrawTargetSprite();

                // Checks for movement changes in the targeted player.
                Debug.Log("<b>[ACTION]</b> Searching if the following player position has changed!");
                _navAgent.SetDestination(_targetingPlayer.target.position);
                StartCoroutine(FollowChecKTime());
            }
        }
    }

    // Checks if the time it takes for the player to check if the new player has moved has passed.
    bool isFollowCheck()
    {
        return !_hasFollowed && _targetingPlayer.target != null;
    }

    // Timer that checks if the player has checked for a new follow target.
    IEnumerator FollowChecKTime ()
    {
        _hasFollowed = true;
        yield return _waitFrequency;
        _hasFollowed = false;
    }
}
