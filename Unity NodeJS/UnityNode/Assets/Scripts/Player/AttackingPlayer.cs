﻿using UnityEngine;
using System.Collections;

public class AttackingPlayer : MonoBehaviour {

    // Variables for targeting and attacking another player;
    [Header("Attack Conditions")]
    public float attackDistance = 2;
    public float attackTime = 2;
    private bool _hasAttacked = false;
    private WaitForSeconds _waitFrequency;
    private TargetingPlayer _targetingPlayer;

    // Use this for initialization
    void Start () {
        _targetingPlayer = GetComponent<TargetingPlayer>();
        _waitFrequency = new WaitForSeconds(attackTime);
    }
	
	// Update is called once per frame
	void Update () {
        if(!_hasAttacked)
        {
            if(_targetingPlayer.target != null && _targetingPlayer.IsInRange(attackDistance)
                && !_targetingPlayer.target.GetComponent<Hittable>().IsDead)
            {
                StartCoroutine(FollowChecKTime());
                string attackedId = _targetingPlayer.target.GetComponent<NetworkIdentity>().id;
                Network.Attack(attackedId);
            }
        }
    }

    // Timer that checks if the player is ready for another attack.
    IEnumerator FollowChecKTime()
    {
        _hasAttacked = true;
        yield return _waitFrequency;
        _hasAttacked = false;
    }
}
