﻿using UnityEngine;
using System.Collections;

public class Hittable : MonoBehaviour {

    // All classes of this class can have an health.
    [Header("Hittable Properties")]
    public float health = 100;
    public float respawnTime = 5;
    public Transform respawnPoint;
    private PlayerMovement _playerMovement;
    private WaitForSeconds _waitRespawn;
    private Animator _animator;

    // Use this for initialization
    void Start ()
    {
        _animator = GetComponent<Animator>();
        _playerMovement = GetComponent<PlayerMovement>();
        _waitRespawn = new WaitForSeconds(respawnTime);
        respawnPoint = GameObject.FindGameObjectWithTag("Spawn").transform;
	}

    // Applies damage to the hittable's health.
    public void TakeDamage(float damage)
    {
        // Applies damage.
        health -= damage;

        // Checks if health is under zero.
        if(IsDead)
        {
            _animator.SetTrigger("Death");
            _playerMovement.enabled = false;
            StartCoroutine(Respawn());
        }
    }

    // Used to respawn the player after a set time.
    IEnumerator Respawn()
    {
        // Starts a timer to wait for the player to respawn.
        yield return _waitRespawn;

        // Resets the player's propreties.
        transform.position = respawnPoint.position;
        Network.Move(respawnPoint.position, respawnPoint.position);

        // Respawns the player in animation, health and network info.
        _animator.SetTrigger("ReSpawn");
        _playerMovement.enabled = true;
        health = 100; 

        // Debugs
        Debug.LogWarning("[ACTION] Respawning Player " + GetComponent<NetworkIdentity>().id);

    }

    // Property that returns if the player is dead or not.
    public bool IsDead
    {
        get
        {
            return health <= 0;
        }
    }
}
