﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerMovement : MonoBehaviour {

    // Variables important to the player movement.
    private NavMeshAgent _navAgent;
    private TargetingPlayer _targetingPlayer;
    private Animator _animator;

	// Use this for initialization
	void Start () {
        // Gets the necessary components.
        _navAgent = GetComponent<NavMeshAgent>();
        _targetingPlayer = GetComponent<TargetingPlayer>();
        _animator = GetComponent<Animator>();
	}

    // Update is called once per frame
    void Update()
    {
        _animator.SetFloat("Distance", _navAgent.remainingDistance);
    }

    // Moves the player to the destinated destination.
    public void NavigateTo (Vector3 position) {
        _targetingPlayer.StopTarget();
        _navAgent.SetDestination(position);
        _animator.SetBool("Attack", false); // Bools also unset Triggers!
	}
}
