﻿using UnityEngine;
using System.Collections;

public class TargetingPlayer : MonoBehaviour {

    [Header("Target Player")]
    [ReadOnly]
    public Transform target;
    public Sprite followingCircle;

    // Checks if the player is in range of another player.
    public bool IsInRange(float stopFollowDistance)
    {
        return (Vector3.Distance(transform.position, target.transform.position)) < stopFollowDistance;
    }

    // Draws the circle showing this player is being followed.
    public void DrawTargetSprite()
    {
        SpriteRenderer playerCircle = target.GetComponentInChildren<SpriteRenderer>();
        if (gameObject.CompareTag("Player") && !playerCircle.enabled)
        {
            playerCircle.sprite = followingCircle;
            playerCircle.enabled = true;
        }
    }

    // Nullifies the targeted player and removes the circle.
    public void StopTarget()
    {
        // Executes only when the target player is set.
        if (target != null)
        {
            // Disables the circle of following the player.
            if (gameObject.CompareTag("Player"))
            {
                SpriteRenderer playerCircle = target.GetComponentInChildren<SpriteRenderer>();
                playerCircle.enabled = false;
            }

            // Sets target to null.
            target = null;
        }
    }
}
