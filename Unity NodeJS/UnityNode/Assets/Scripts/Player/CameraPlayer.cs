﻿using UnityEngine;
using System.Collections;

public class CameraPlayer : MonoBehaviour {

    // Defines the player that is necessary for the camera to follow the player.
    [Header("Camera")]
    private Camera _gameCamera;
    private GameObject _player;
    public Vector3 positionDiference;
    private Vector3 _currentPosition;

    // Update is called once per frame
    void Update ()
    {
        // Checks if the player has been assigned yet.
        if (_player == null)
        {
            _gameCamera = Camera.main;
            _player = GameObject.FindGameObjectWithTag("Player");
        }

        if (_player != null)
        {
            // Calculates the new camera position.
            _currentPosition = _player.transform.position;
            _currentPosition += positionDiference;
            _gameCamera.transform.position = _currentPosition;
        }
    }
}
