﻿    using UnityEngine;
using System.Collections;
using System;

public class MovableCollider : MonoBehaviour, IClickable {

    // Defines clickable areas, and variables used for that.
    private GameObject _player;
    private PlayerMovement _playerMovement;

	// Called when the player clicks on the floor, and moves the player to it.
    public void OnClick(RaycastHit clickedHit, int mouseButton)
    {
        if(mouseButton == 1)
        {
            // Checks if the player has been assigned yet.
            if (_player == null)
            {
                _player = GameObject.FindGameObjectWithTag("Player");
            }
        
            if(_player != null && !_player.GetComponent<Hittable>().IsDead)
            {
                // Gets the remaining components from the player.
                _playerMovement = _player.GetComponent<PlayerMovement>();

                // Moves the player to clicked position.
                _playerMovement.NavigateTo(clickedHit.point);
                Network.Move(_player.transform.position, clickedHit.point);
            }
        }
    }
}
