﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using SocketIO;
using System;

public class Network : MonoBehaviour {

    // Defines the Socket IO component that will be obtained as a parent.
    static SocketIOComponent socket;

    [Header("Auxiliary Objects")]
    public GameObject menuCanvas;
    public GameObject ingameCanvas;
    public Camera menuCamera;
    public Camera ingameCamera;

    [Header("Player Objects")]
    public NetworkSpawn networkSpawn;
    public NetworkCanvas networkCanvas;

    // Use this for initialization
    void Start()
    {
        socket = GetComponent<SocketIOComponent>();
    }

    // Use this to connect to socket.
    public void StartConnection()
    {
        // Checks if the network is connected.
        socket.Connect();

        // Enables and Disables the correct canvas and cameras.
        menuCanvas.SetActive(false);
        ingameCanvas.SetActive(true);
        menuCamera.gameObject.SetActive(false);
        ingameCamera.gameObject.SetActive(true);
        SetCallBacks();
    }

    // Sets callbacks for node functions.
    public void SetCallBacks()
    {
        // Basic Callbacks
        socket.On("open", OnConnected);
        socket.On("spawn", OnSpawned);
        socket.On("disconnected", OnDisconnect);
        // Movement Callbacks
        socket.On("move", OnMove);
        socket.On("follow", OnFollow);
        socket.On("requestPosition", OnRequestPosition);
        socket.On("updatePosition", OnUpdatePosition);
        // Action Callbacks
        socket.On("attack", OnAttack);
        // Chat Callbacks
        socket.On("receiveChat", OnReceiveChat);
    }

    // Function called when a player (client) connects. 
    void OnConnected(SocketIOEvent socketEvent)
    {
        Debug.LogWarning("<b>[SPAWNED]</b> Player has Connected!");
    }

    // Function called on spawn.
    void OnSpawned(SocketIOEvent socketEvent)
    {
        // Creates a new player from the prefab.
        int playerSkin = (int)socketEvent.data["skin"].n;
        string playerId = socketEvent.data["id"].str;
        GameObject spawnedPlayer = networkSpawn.SpawnPlayer(playerId, playerSkin);

        networkCanvas.ReceiveChat("", "<b><color=cyan>Player " + playerId + " has joined!</color></b>");
        if (socketEvent.data["x"])
        {
            Vector3 moveToPosition = JSONTools.JSONToVector(ref socketEvent);
            PlayerMovement playerMovement = spawnedPlayer.GetComponent<PlayerMovement>();
            playerMovement.NavigateTo(moveToPosition);
        }
    }

    // Function called when a player moves.
    void OnMove(SocketIOEvent socketEvent)
    {
        // Gets the ID of the player which just moved.
        string returnedId = socketEvent.data["id"].str;
        GameObject movedPlayer = networkSpawn.FindPlayer(returnedId);

        // Does the player movement.
        Vector3 playerDestination = JSONTools.JSONToVector(ref socketEvent);
        PlayerMovement playerMovement = movedPlayer.GetComponent<PlayerMovement>();
        playerMovement.NavigateTo(playerDestination);

        // Debugs.
        Debug.Log("<b>[MOVEMENT]</b> The player " + returnedId + " is moving to " + playerDestination);
    }

    // Function called when a player (client) requests the already connected clients position.
    void OnRequestPosition(SocketIOEvent socketEvent)
    {
        Debug.Log("<b>[REQUEST]</b> The server has requested the players positions.");
        socket.Emit("updatePosition", JSONTools.VectorToJSON(networkSpawn.LocalPosition()));
    }

    // Function called when the player gets back the positions to update.
    void OnUpdatePosition(SocketIOEvent socketEvent)
    {
        // Gets the ID of the player which just moved.
        string returnedId = socketEvent.data["id"].str;
        GameObject movedPlayer = networkSpawn.FindPlayer(returnedId);

        // Changes the player position to the returned position.
        Vector3 playerPosition = JSONTools.JSONToVector(ref socketEvent);
        movedPlayer.transform.position = playerPosition;
    }

    // Function called when a player follows another player.
    void OnFollow(SocketIOEvent socketEvent)
    {
        // Gets the ID of the player which requested the follow.
        string playerId = socketEvent.data["id"].str;
        GameObject movedPlayer = networkSpawn.FindPlayer(playerId);

        // Gets the ID of the player being followed.
        string targetId = socketEvent.data["targetId"].str;
        GameObject targetPlayer = networkSpawn.FindPlayer(targetId);

        // Assigns the player which is going to be followed to the player who is following.
        TargetingPlayer targetingPlayer = movedPlayer.GetComponent<TargetingPlayer>();
        targetingPlayer.target = targetPlayer.transform;

        Debug.Log("<b>[ACTION]</b> Player " + playerId + " is now following " + targetId + "!");
    }


    // Function called when an attack is made.
    void OnAttack(SocketIOEvent socketEvent)
    {
        // Sets the animation from the attacking player.
        string attackPlayerId = socketEvent.data["id"].str;
        GameObject attackPlayer = networkSpawn.FindPlayer(attackPlayerId);
        attackPlayer.GetComponent<Animator>().SetTrigger("Attack");

        // Gets the hittable class from the target.
        string targetPlayerId = socketEvent.data["targetId"].str;
        GameObject targetedPlayer = networkSpawn.FindPlayer(targetPlayerId);
        Hittable hittablePlayer = targetedPlayer.GetComponent<Hittable>();
        hittablePlayer.TakeDamage(10);

        // Debugs
        Debug.LogWarning("<b>[ACTION]</b> Player " + attackPlayerId +
            " has attacked " + targetPlayerId + "!");
    }

    // Function called when a message is received.
    void OnReceiveChat(SocketIOEvent socketEvent)
    {
        // Gets the message sent.
        string returnedMessage = socketEvent.data["chatMessage"].str;
        string sentUser = socketEvent.data["id"].str;
        Debug.Log("<b>[CHAT]</b> Received Message! " + returnedMessage);

        // Sends it over to the text canvas.
        networkCanvas.ReceiveChat(sentUser, returnedMessage);
    }

    // Function called when a player (client) disconnects.
    void OnDisconnect(SocketIOEvent socketEvent)
    {
        // Destroys the selected player.
        Debug.LogWarning("<b>[DISCONNECT]</b> Player " + socketEvent.data["id"] + " has disconnected!");
        string disconnectedId = socketEvent.data["id"].str;
        networkCanvas.ReceiveChat("", "<b><color=red>Player " + disconnectedId + " has disconnected!</color></b>");
        networkSpawn.RemovePlayer(disconnectedId);
    }

    // Send the following information over to the server.
    public static void Follow(string followingId)
    {
        Debug.Log("<b>[ACTION]</b> Player Started Following " + followingId);
        socket.Emit("follow", JSONTools.PlayerIdToJson(ref followingId));
    }

    // Send the movement information over to the server.
    public static void Move(Vector3 current, Vector3 destination)
    {
        // Stores both the current position and the destination as a single json object.
        Debug.Log("<b>[MOVEMENT]</b> A player has moved to Position " + destination + "!");
        JSONObject Json = new JSONObject(JSONObject.Type.OBJECT);
        Json.AddField("current", JSONTools.VectorToJSON(current));
        Json.AddField("destination", JSONTools.VectorToJSON(destination));
        socket.Emit("move", Json);
    }

    // Sends information to the server that a player was attacked.
    public static void Attack(string atackedId)
    {
        socket.Emit("attack", JSONTools.PlayerIdToJson(ref atackedId));
    }
}
