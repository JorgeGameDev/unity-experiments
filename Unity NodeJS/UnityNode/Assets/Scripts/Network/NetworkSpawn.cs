﻿using UnityEngine;
using System.Collections.Generic;

public class NetworkSpawn : MonoBehaviour {

    [Header("Prefabs")]
    public GameObject playerGO;
    [ReadOnly]public GameObject localPlayer;
    public Dictionary<string, GameObject> netPlayers = new Dictionary<string, GameObject>();

    [Header("Polishes")]
    public Material[] skins;

    // Takes care of spawning the player.
    public GameObject SpawnPlayer(string netId, int playerSkin) {

        // Spawns the player.
        GameObject spawnedPlayer = (GameObject)Instantiate(playerGO, transform.position, Quaternion.identity);
        spawnedPlayer.GetComponentInChildren<SkinnedMeshRenderer>().sharedMaterial = skins[playerSkin];
        spawnedPlayer.GetComponent<NetworkIdentity>().id = netId;

        // Adds the player to the list of networked players.
        netPlayers.Add(netId, spawnedPlayer);

        // Sets the first spawned as the player's client.
        if (localPlayer == null)
        {
            localPlayer = spawnedPlayer;
            localPlayer.GetComponentInChildren<SpriteRenderer>().enabled = true;
            localPlayer.AddComponent<AttackingPlayer>();
        }
        else
        {
            // Untags the player to avoid confusion.
            spawnedPlayer.tag = "Untagged";
            FollowedPlayer followedPlayer = spawnedPlayer.AddComponent<FollowedPlayer>();
            followedPlayer.playerFollowing = localPlayer;
        }

        // Debugs.
        Debug.LogWarning("<b>[SPAWNED]</b> Player " + netId + " has Spawned! | Player Count: " + netPlayers.Count);

        // Returns the player. 
        return spawnedPlayer;
    }

    // Finds an existing player by id.
    public GameObject FindPlayer(string netId)
    {
        return netPlayers[netId];
    }

    // Removes a player.
    public void RemovePlayer(string netId)
    {
        // Finds the player who is disconnecting.
        GameObject disconnectedPlayer = netPlayers[netId];
        Destroy(disconnectedPlayer);

        // Removes the player from the player list.
        netPlayers.Remove(netId);
    }

    // Returns the position of the local player.
    public Vector3 LocalPosition()
    {
        return localPlayer.transform.position;
    }
}
