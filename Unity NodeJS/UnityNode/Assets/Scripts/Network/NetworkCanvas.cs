﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using SocketIO;

public class NetworkCanvas : MonoBehaviour {

    [Header("Socket Component")]
    [ReadOnly]
    public SocketIOComponent socket;

    [Header("Canvas Elements")]
    [ReadOnly]
    public List<string> chatMessages = new List<string>();
    public int maxMessages;
    public Text textField;
    public InputField inputField;

    // Use this for initialization.
    void Start()
    {
        socket = GameObject.FindGameObjectWithTag("Networker").GetComponent<SocketIOComponent>();
    }

    // Update is called once per frame
    void Update () {
	}

    // Used to send messages.
    public void SendChat()
    {
        // Checks if the message is not empty.
        if(inputField.text != "")
        {
            // Stores the text sent in the chat.
            string newMessage = inputField.text;
            Debug.Log("<b>[CHAT]</b> Sent Message! " + newMessage);
            // Sends the message through socket.
            socket.Emit("sendChat", new JSONObject(ChatToJSON(newMessage)));
            // Stores the sent message on a list.
            ReceiveChat("You", newMessage);
            inputField.text = "";
            inputField.Select();
            inputField.ActivateInputField();
        }
    }

    // Used to add messages to the text box.
    public void ReceiveChat(string playerId, string message)
    {
        // Adds the message received.
        textField.text = "";
        // Adds the string to the list.
        if(playerId == "")
        {
            chatMessages.Add(message);
        }
        else
        {
            chatMessages.Add("[" + playerId + "] " + message);
        }
        // Checks if the messages went over the limit.
        if (chatMessages.Count > maxMessages)
        {
            chatMessages.RemoveAt(0);
        }
        // Displays all the existing strings.
        foreach(string chatMessage in chatMessages)
        {
            // Shows the message.
            textField.text += chatMessage + "\n";
        }
    }

    // Used to transform a chat message into JSON.
    public string ChatToJSON(string message)
    {
        return string.Format(@"{{""chatMessage"":""{0}""}}", message);
    }
}
