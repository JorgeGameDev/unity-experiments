﻿using UnityEngine;
using System.Collections;

public class NetworkIdentity : MonoBehaviour {

    // Holds the Server-Identification for this instance.
    [Header("Identification")]
    [ReadOnly]
    public string id;
}
