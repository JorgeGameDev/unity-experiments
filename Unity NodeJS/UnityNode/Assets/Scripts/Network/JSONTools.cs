﻿using UnityEngine;
using SocketIO;
using System.Collections;

public class JSONTools {

    // Converts Vector to JSON.
    public static JSONObject VectorToJSON(Vector3 vector)
    {
        // Converts the vector to JSON using a builder included in SocketIO.
        JSONObject Json = new JSONObject(JSONObject.Type.OBJECT);
        Json.AddField("x", vector.x);
        Json.AddField("y", vector.y);
        Json.AddField("z", vector.z);
        return Json;
    }

    // Converts a JSON back to Vector.
    public static Vector3 JSONToVector(ref SocketIOEvent socketEvent)
    {
        return new Vector3(socketEvent.data["x"].n, socketEvent.data["y"].n, socketEvent.data["z"].n);
    }

    // Returns the player ID as JSON.
    public static JSONObject PlayerIdToJson(ref string id)
    {
        JSONObject Json = new JSONObject(JSONObject.Type.OBJECT);
        Json.AddField("targetId", id);
        return Json;
    }
}
