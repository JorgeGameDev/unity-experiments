﻿using UnityEngine;
using System.Collections;

public interface IClickable {

    // Defines functions that objects using this interface require.
    void OnClick(RaycastHit clickedHit, int button);
}
